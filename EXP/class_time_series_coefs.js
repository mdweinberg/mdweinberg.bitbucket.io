var class_time_series_coefs =
[
    [ "TimeSeriesCoefs", "class_time_series_coefs.html#a1fe6aaa2ef06c23a6b5e8b0b5f63a624", null ],
    [ "coefs", "class_time_series_coefs.html#ab636fddbe2fe8509da9c559f20f361db", null ],
    [ "compute_df", "class_time_series_coefs.html#ab419b61d37d0ee09d605c9d91a547a48", null ],
    [ "delT", "class_time_series_coefs.html#a51de113e0e2b51a3e07dcccf121fb529", null ],
    [ "E", "class_time_series_coefs.html#a0ff4f4ca8b8b395542417bf2c07e85a4", null ],
    [ "lnL", "class_time_series_coefs.html#ac649a9199433ac1c4fdf4c1c62887862", null ],
    [ "M", "class_time_series_coefs.html#abd00b4447d8307081104b4be4353ef8b", null ],
    [ "outfile", "class_time_series_coefs.html#a396083675c18244ac6db0a8fbe332e4b", null ],
    [ "PHI", "class_time_series_coefs.html#ad70887665719629ae738ce41410b673b", null ],
    [ "R", "class_time_series_coefs.html#a09ca8c750cbac28add969872c19912c0", null ],
    [ "Rperi", "class_time_series_coefs.html#a9f1a0d554072a2a525695525fde2a1e5", null ],
    [ "T", "class_time_series_coefs.html#a312f32e33cd97019505979330128f449", null ],
    [ "Tmax", "class_time_series_coefs.html#aa4e5d89f1c4d674161277ff33f3382bb", null ]
];