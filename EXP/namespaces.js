var namespaces =
[
    [ "boost", "namespaceboost.html", "namespaceboost" ],
    [ "newargs", "namespacenewargs.html", null ],
    [ "NTC", "namespace_n_t_c.html", null ],
    [ "oxy", "namespaceoxy.html", null ],
    [ "plot_h", "namespaceplot__h.html", null ],
    [ "plot_he", "namespaceplot__he.html", null ],
    [ "plotRR", "namespaceplot_r_r.html", null ],
    [ "plotVY", "namespaceplot_v_y.html", null ],
    [ "plotVYlin", "namespaceplot_v_ylin.html", null ],
    [ "recomb", "namespacerecomb.html", null ],
    [ "std", "namespacestd.html", null ],
    [ "testmed", "namespacetestmed.html", null ],
    [ "tk", "namespacetk.html", null ]
];