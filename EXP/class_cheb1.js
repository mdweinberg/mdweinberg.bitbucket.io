var class_cheb1 =
[
    [ "Cheb1", "class_cheb1.html#a778123a0fa83fc0b10c818c7b17910c4", null ],
    [ "coef1", "class_cheb1.html#ad64c1e132734c4bddcd2ede0e8137100", null ],
    [ "coef2", "class_cheb1.html#a7aa6efb2eed20c6882389664329ae6e0", null ],
    [ "coef3", "class_cheb1.html#aa06dbd92891410d54a930c3d8ae57493", null ],
    [ "coef4", "class_cheb1.html#a20335fd6b9c528da6399cabacae65039", null ],
    [ "f0", "class_cheb1.html#ab4fdcde16dd68dc8e3bcafa28d35e571", null ],
    [ "f1", "class_cheb1.html#a7d5956b9e9bce1b0cfec3d3d07e01672", null ],
    [ "h", "class_cheb1.html#ab7dcbd54cb623cc67ba7cc366d73ca84", null ],
    [ "w", "class_cheb1.html#a20b0b48615e0d4e46d9c9671b794885f", null ]
];