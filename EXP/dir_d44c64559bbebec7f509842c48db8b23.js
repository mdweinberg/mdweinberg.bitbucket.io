var dir_d44c64559bbebec7f509842c48db8b23 =
[
    [ "AsciiHisto.H", "_ascii_histo_8_h.html", "_ascii_histo_8_h" ],
    [ "BarrierWrapper.H", "_barrier_wrapper_8_h.html", "_barrier_wrapper_8_h" ],
    [ "biorth.h", "biorth_8h.html", "biorth_8h" ],
    [ "biorth1d.h", "biorth1d_8h.html", "biorth1d_8h" ],
    [ "biorth2d.h", "biorth2d_8h.html", "biorth2d_8h" ],
    [ "biorth_wake.h", "biorth__wake_8h.html", "biorth__wake_8h" ],
    [ "BWHelper.H", "_b_w_helper_8_h.html", [
      [ "bw_struct", "classbw__struct.html", "classbw__struct" ]
    ] ],
    [ "CauchyPV.h", "_cauchy_p_v_8h.html", [
      [ "PVQuad", "class_p_v_quad.html", "class_p_v_quad" ]
    ] ],
    [ "ChebFit.h", "_cheb_fit_8h.html", [
      [ "ChebFit", "class_cheb_fit.html", "class_cheb_fit" ]
    ] ],
    [ "Circular.H", "_circular_8_h.html", [
      [ "CyclicIterator", "class_cyclic_iterator.html", "class_cyclic_iterator" ],
      [ "RingIterator", "class_ring_iterator.html", "class_ring_iterator" ]
    ] ],
    [ "clinalg.h", "clinalg_8h.html", "clinalg_8h" ],
    [ "Configuration.H", "_configuration_8_h.html", [
      [ "BoolTranslator", "struct_bool_translator.html", "struct_bool_translator" ],
      [ "Configuration", "class_configuration.html", "class_configuration" ],
      [ "translator_between< std::basic_string< Ch, Traits, Alloc >, bool >", "structboost_1_1property__tree_1_1translator__between_3_01std_1_1basic__string_3_01_ch_00_01_trai0d72484622a9a8e53eb6b5158b3b5cb9.html", "structboost_1_1property__tree_1_1translator__between_3_01std_1_1basic__string_3_01_ch_00_01_trai0d72484622a9a8e53eb6b5158b3b5cb9" ]
    ] ],
    [ "cpoly.h", "cpoly_8h.html", "cpoly_8h" ],
    [ "cubic.h", "cubic_8h.html", [
      [ "Cubic_Table", "class_cubic___table.html", "class_cubic___table" ]
    ] ],
    [ "CVector.h", "_c_vector_8h.html", "_c_vector_8h" ],
    [ "DiskWithHalo.h", "_disk_with_halo_8h.html", [
      [ "DiskWithHalo", "class_disk_with_halo.html", "class_disk_with_halo" ]
    ] ],
    [ "EXPException.H", "_e_x_p_exception_8_h.html", [
      [ "BadIndexException", "class_bad_index_exception.html", "class_bad_index_exception" ],
      [ "EXPException", "class_e_x_p_exception.html", "class_e_x_p_exception" ],
      [ "FileCreateError", "class_file_create_error.html", "class_file_create_error" ],
      [ "FileOpenError", "class_file_open_error.html", "class_file_open_error" ],
      [ "GenericError", "class_generic_error.html", "class_generic_error" ],
      [ "InternalError", "class_internal_error.html", "class_internal_error" ]
    ] ],
    [ "exponential.h", "exponential_8h.html", "exponential_8h" ],
    [ "FileUtils.H", "_file_utils_8_h.html", "_file_utils_8_h" ],
    [ "FindOrb.H", "_find_orb_8_h.html", [
      [ "FindOrb", "class_find_orb.html", "class_find_orb" ],
      [ "OrbValues", "struct_orb_values.html", "struct_orb_values" ]
    ] ],
    [ "fpetrap.h", "fpetrap_8h.html", "fpetrap_8h" ],
    [ "Func1d.H", "_func1d_8_h.html", [
      [ "Func1d", "class_func1d.html", "class_func1d" ]
    ] ],
    [ "gaussQ.h", "gauss_q_8h.html", [
      [ "GaussQuad", "class_gauss_quad.html", "class_gauss_quad" ],
      [ "HermQuad", "class_herm_quad.html", "class_herm_quad" ],
      [ "JacoQuad", "class_jaco_quad.html", "class_jaco_quad" ],
      [ "LaguQuad", "class_lagu_quad.html", "class_lagu_quad" ],
      [ "LegeQuad", "class_lege_quad.html", "class_lege_quad" ]
    ] ],
    [ "gknots.h", "gknots_8h.html", "gknots_8h" ],
    [ "hernquist.h", "include_2hernquist_8h.html", "include_2hernquist_8h" ],
    [ "hunter.h", "hunter_8h.html", "hunter_8h" ],
    [ "HunterX.h", "_hunter_x_8h.html", "_hunter_x_8h" ],
    [ "InitContainer.H", "_init_container_8_h.html", "_init_container_8_h" ],
    [ "interp.h", "interp_8h.html", "interp_8h" ],
    [ "isothermal.h", "isothermal_8h.html", "isothermal_8h" ],
    [ "kalnajs.h", "kalnajs_8h.html", "kalnajs_8h" ],
    [ "kevin_complex.h", "kevin__complex_8h.html", "kevin__complex_8h" ],
    [ "king.h", "king_8h.html", "king_8h" ],
    [ "linalg.h", "linalg_8h.html", "linalg_8h" ],
    [ "localmpi.h", "localmpi_8h.html", "localmpi_8h" ],
    [ "logic.h", "logic_8h.html", "logic_8h" ],
    [ "massmodel.h", "massmodel_8h.html", "massmodel_8h" ],
    [ "mconf.h", "mconf_8h.html", "mconf_8h" ],
    [ "mestel.h", "mestel_8h.html", "mestel_8h" ],
    [ "model2d.h", "model2d_8h.html", "model2d_8h" ],
    [ "model3d.h", "model3d_8h.html", "model3d_8h" ],
    [ "models.h", "models_8h.html", [
      [ "Hernquist", "class_hernquist.html", "class_hernquist" ],
      [ "Isothermal", "class_isothermal.html", "class_isothermal" ],
      [ "Logarithmic", "class_logarithmic.html", "class_logarithmic" ],
      [ "Miyamoto", "class_miyamoto.html", "class_miyamoto" ],
      [ "Miyamoto_Needle", "class_miyamoto___needle.html", "class_miyamoto___needle" ],
      [ "Modified_Hubble", "class_modified___hubble.html", "class_modified___hubble" ],
      [ "Needle", "class_needle.html", "class_needle" ],
      [ "Plummer", "class_plummer.html", "class_plummer" ],
      [ "Point_Quadrupole", "class_point___quadrupole.html", "class_point___quadrupole" ],
      [ "Quadrupole_Bar", "class_quadrupole___bar.html", "class_quadrupole___bar" ]
    ] ],
    [ "needle.h", "needle_8h.html", [
      [ "Miyamoto_Needle", "class_miyamoto___needle.html", "class_miyamoto___needle" ],
      [ "Needle", "class_needle.html", "class_needle" ]
    ] ],
    [ "norminv.H", "norminv_8_h.html", "norminv_8_h" ],
    [ "numerical.h", "numerical_8h.html", "numerical_8h" ],
    [ "orbit.h", "orbit_8h.html", "orbit_8h" ],
    [ "OrthoPoly.h", "_ortho_poly_8h.html", [
      [ "Cheb1", "class_cheb1.html", "class_cheb1" ],
      [ "Cheb2", "class_cheb2.html", "class_cheb2" ],
      [ "GenLagu", "class_gen_lagu.html", "class_gen_lagu" ],
      [ "Hermite", "class_hermite.html", "class_hermite" ],
      [ "Legendre", "class_legendre.html", "class_legendre" ],
      [ "OrthoPoly", "class_ortho_poly.html", "class_ortho_poly" ],
      [ "Ultra", "class_ultra.html", "class_ultra" ]
    ] ],
    [ "phase.h", "phase_8h.html", "phase_8h" ],
    [ "plummer.h", "plummer_8h.html", "plummer_8h" ],
    [ "poly.h", "poly_8h.html", "poly_8h" ],
    [ "port.h", "port_8h.html", "port_8h" ],
    [ "QPDistF.h", "_q_p_dist_f_8h.html", [
      [ "QPDistF", "class_q_p_dist_f.html", "class_q_p_dist_f" ]
    ] ],
    [ "rotcurv.h", "rotcurv_8h.html", "rotcurv_8h" ],
    [ "RunningTime.H", "_running_time_8_h.html", [
      [ "RunningTime", "class_running_time.html", "class_running_time" ]
    ] ],
    [ "simann2.h", "simann2_8h.html", "simann2_8h" ],
    [ "SLGridMP2.h", "_s_l_grid_m_p2_8h.html", [
      [ "SLGridCyl", "class_s_l_grid_cyl.html", "class_s_l_grid_cyl" ],
      [ "SLGridSlab", "class_s_l_grid_slab.html", "class_s_l_grid_slab" ],
      [ "SLGridSph", "class_s_l_grid_sph.html", "class_s_l_grid_sph" ]
    ] ],
    [ "sltableMP2.h", "include_2sltable_m_p2_8h.html", [
      [ "TableCyl", "class_table_cyl.html", "class_table_cyl" ],
      [ "TableSlab", "class_table_slab.html", "class_table_slab" ],
      [ "TableSph", "class_table_sph.html", "class_table_sph" ]
    ] ],
    [ "sphereSL.h", "include_2sphere_s_l_8h.html", [
      [ "SphereSL", "class_sphere_s_l.html", "class_sphere_s_l" ]
    ] ],
    [ "Spline.h", "include_2_spline_8h.html", "include_2_spline_8h" ],
    [ "staeckel.h", "staeckel_8h.html", [
      [ "Perfect", "class_perfect.html", "class_perfect" ]
    ] ],
    [ "StringTok.H", "_string_tok_8_h.html", [
      [ "StringTok", "class_string_tok.html", "class_string_tok" ]
    ] ],
    [ "Swap.h", "_swap_8h.html", [
      [ "Swap", "class_swap.html", "class_swap" ]
    ] ],
    [ "Timer.h", "_timer_8h.html", [
      [ "Timer", "class_timer.html", "class_timer" ]
    ] ],
    [ "toomre.h", "toomre_8h.html", "toomre_8h" ],
    [ "Vector.h", "_vector_8h.html", "_vector_8h" ],
    [ "vector3.h", "vector3_8h.html", [
      [ "Three_Vector", "class_three___vector.html", "class_three___vector" ]
    ] ],
    [ "VtkGrid.H", "_vtk_grid_8_h.html", "_vtk_grid_8_h" ],
    [ "ZBrent.H", "_z_brent_8_h.html", "_z_brent_8_h" ],
    [ "zip.H", "zip_8_h.html", "zip_8_h" ]
];