var struct_slab_1_1_slab_coef_header =
[
    [ "h", "struct_slab_1_1_slab_coef_header.html#aa24c95fca8051849928a325cbadf9387", null ],
    [ "jmax", "struct_slab_1_1_slab_coef_header.html#a586067cd027b02770acd3b2bbf8b8ad2", null ],
    [ "nmaxx", "struct_slab_1_1_slab_coef_header.html#a13cac38129c0b8d3bacb473d0e0beee2", null ],
    [ "nmaxy", "struct_slab_1_1_slab_coef_header.html#afe6ea8d9bda265859233131428d4a9c4", null ],
    [ "nmaxz", "struct_slab_1_1_slab_coef_header.html#aa5ad04b10398e80204eeec4986314ef3", null ],
    [ "time", "struct_slab_1_1_slab_coef_header.html#a65bb32982315b58f613002f2846d4a0e", null ],
    [ "type", "struct_slab_1_1_slab_coef_header.html#a2367e9baa1a2f0a0ec655fe37ca5c537", null ],
    [ "zmax", "struct_slab_1_1_slab_coef_header.html#a58b6675f861a07dbd045ad503ed2b9e5", null ]
];