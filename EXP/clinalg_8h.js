var clinalg_8h =
[
    [ "determinant", "clinalg_8h.html#a17a756c9c885707c99edb7ebe8d6fd82", null ],
    [ "embed_matrix", "clinalg_8h.html#ad4264010e2bb494b1aa0ce239f9bcbf8", null ],
    [ "embed_matrix", "clinalg_8h.html#aa9deed2c322da77b74687b14f113aab5", null ],
    [ "embed_matrix", "clinalg_8h.html#a652ae6830710f5c3163f2eddbb0b8f5b", null ],
    [ "inverse", "clinalg_8h.html#a44d4abbac411c9c59e78d3a142666fbd", null ],
    [ "linear_solve", "clinalg_8h.html#a001bc5c8fec5215f8e61dc6077879ed0", null ],
    [ "lu_backsub", "clinalg_8h.html#a29ec18bc9c7ce365b0d55b425831ac70", null ],
    [ "lu_decomp", "clinalg_8h.html#afd3ab9f18f5b48788213ab974aa3a298", null ],
    [ "lu_determinant", "clinalg_8h.html#a36fd4ee445b2000f33d2c3730c15e784", null ],
    [ "sub_matrix", "clinalg_8h.html#a2843ec829d28c5a97e96c48c2936e376", null ]
];