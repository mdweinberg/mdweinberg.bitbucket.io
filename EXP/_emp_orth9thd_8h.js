var _emp_orth9thd_8h =
[
    [ "EmpCylSL", "class_emp_cyl_s_l.html", "class_emp_cyl_s_l" ],
    [ "PCAbasis", "class_emp_cyl_s_l_1_1_p_c_abasis.html", "class_emp_cyl_s_l_1_1_p_c_abasis" ],
    [ "PCAelement", "class_emp_cyl_s_l_1_1_p_c_aelement.html", "class_emp_cyl_s_l_1_1_p_c_aelement" ],
    [ "dlegendre_R", "_emp_orth9thd_8h.html#a463f9e2704c84ce1b5e0f85e0bad1caf", null ],
    [ "legendre_R", "_emp_orth9thd_8h.html#a518d3674287a6be91af3669ab785edd9", null ],
    [ "sinecosine_R", "_emp_orth9thd_8h.html#a4fb8b0f30b5a1331fd3bc7e5b709224b", null ]
];