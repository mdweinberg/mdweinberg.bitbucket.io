var structgas__particle =
[
    [ "hsmooth", "structgas__particle.html#acc794ce36a1ce0a65a932f2a6cce64c5", null ],
    [ "mass", "structgas__particle.html#a832d4c193125b69735c91c1f8c9ceda6", null ],
    [ "metals", "structgas__particle.html#a1c9edd52d57e0ec00c310cba865bd48b", null ],
    [ "phi", "structgas__particle.html#ad8b7917fafa6a13a6c2e820e355e4fc1", null ],
    [ "pos", "structgas__particle.html#ae862ed85db09f8a8758224f3cd13f20a", null ],
    [ "rho", "structgas__particle.html#ac79cc13a1db1d179dea87528d49ac82b", null ],
    [ "temp", "structgas__particle.html#a99ec989e41d9ec439b65294f7f600def", null ],
    [ "vel", "structgas__particle.html#a192b9ed2e2f0346523c28e17ec5a7115", null ]
];