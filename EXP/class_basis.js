var class_basis =
[
    [ "Basis", "class_basis.html#af4c52f8df4bae59050463d2f8e251834", null ],
    [ "determine_acceleration_and_potential", "class_basis.html#abb4dbfbe9c234f44b1719a4d89dc3951", null ],
    [ "determine_fields_at_point_cyl", "class_basis.html#a3bcc5e3250f82d0ce476b096eabf342a", null ],
    [ "determine_fields_at_point_sph", "class_basis.html#a34f7d4340bcd0c5452f430cebf5c3665", null ],
    [ "dlegendre_R", "class_basis.html#a3e0034b2ddec5812374200458c328238", null ],
    [ "legendre_R", "class_basis.html#a34ad96f78ea0a4a10f2de56dfd276d89", null ],
    [ "sinecosine_R", "class_basis.html#a092a30a8e7aaaa852fc8748d3c69c02b", null ]
];