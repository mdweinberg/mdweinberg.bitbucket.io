var _z_brent_8_h =
[
    [ "ZBrent", "class_z_brent.html", "class_z_brent" ],
    [ "ZBrentReturn", "_z_brent_8_h.html#afed7781c1ff415e0185b4847de0fce93", [
      [ "Good", "_z_brent_8_h.html#afed7781c1ff415e0185b4847de0fce93aa9e10f610083e262e7aa4004c9b68fe9", null ],
      [ "Bracket", "_z_brent_8_h.html#afed7781c1ff415e0185b4847de0fce93a269c13c2affa9b7cff07a4aa1e9c5f2e", null ],
      [ "Iteration", "_z_brent_8_h.html#afed7781c1ff415e0185b4847de0fce93ad99cee9bc75fdda19c9973426672e410", null ]
    ] ]
];