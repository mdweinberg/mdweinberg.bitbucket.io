/*
@licstart  The following is the entire license notice for the
JavaScript code in this file.

Copyright (C) 1997-2019 by Dimitri van Heesch

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

@licend  The above is the entire license notice
for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "EXP", "index.html", [
    [ "Recent Additions and Changes", "news.html", null ],
    [ "Code overview", "over.html", null ],
    [ "Quick Start", "quick.html", [
      [ "Requirements", "quick.html#requirements", null ],
      [ "Compiling the code", "quick.html#compiling", null ],
      [ "Running a simulation", "quick.html#running", null ]
    ] ],
    [ "Configuration files", "config.html", null ],
    [ "Phase space file format", "bodies.html", null ],
    [ "Multiple time stepping", "multistep.html", null ],
    [ "Adaptive bases", "adaptive.html", null ],
    [ "Global variables", "global.html", null ],
    [ "Initial condition programs", "ics.html", null ],
    [ "User loadable modules", "modules.html", null ],
    [ "Exploration & Verification routines", "check.html", null ],
    [ "Phase-space file utilities", "utilities.html", null ],
    [ "Diagnostic output for various VERBOSE values", "verbose.html", null ],
    [ "Direct Simulation Monte Carlo", "dsmc.html", [
      [ "Atomic cooling code details", "dsmc.html#ion", null ],
      [ "Atomic cooling implementation", "dsmc.html#atomic", null ],
      [ "Radiative Recombination using the Milne relation", "dsmc.html#milne", null ],
      [ "DSMC trace-particle implementation", "dsmc.html#trace", null ]
    ] ],
    [ "CUDA implementation", "cuda.html", [
      [ "Overview", "cuda.html#overview", null ],
      [ "Strategy", "cuda.html#strategy", null ],
      [ "Implementation details", "cuda.html#impl", null ],
      [ "Usage notes", "cuda.html#use", null ]
    ] ],
    [ "FAQ", "faq.html", null ],
    [ "Namespaces", "namespaces.html", [
      [ "Namespace List", "namespaces.html", "namespaces_dup" ],
      [ "Namespace Members", "namespacemembers.html", [
        [ "All", "namespacemembers.html", "namespacemembers_dup" ],
        [ "Functions", "namespacemembers_func.html", null ],
        [ "Variables", "namespacemembers_vars.html", "namespacemembers_vars" ],
        [ "Typedefs", "namespacemembers_type.html", null ],
        [ "Enumerations", "namespacemembers_enum.html", null ]
      ] ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Hierarchy", "hierarchy.html", "hierarchy" ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Functions", "functions_func.html", "functions_func" ],
        [ "Variables", "functions_vars.html", "functions_vars" ],
        [ "Typedefs", "functions_type.html", null ],
        [ "Enumerations", "functions_enum.html", null ],
        [ "Enumerator", "functions_eval.html", null ],
        [ "Related Functions", "functions_rela.html", "functions_rela" ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ],
      [ "File Members", "globals.html", [
        [ "All", "globals.html", "globals_dup" ],
        [ "Functions", "globals_func.html", "globals_func" ],
        [ "Variables", "globals_vars.html", "globals_vars" ],
        [ "Typedefs", "globals_type.html", null ],
        [ "Enumerations", "globals_enum.html", null ],
        [ "Enumerator", "globals_eval.html", null ],
        [ "Macros", "globals_defs.html", null ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"_add_disk_8h.html",
"_out_frac_8_h.html",
"_vtk_p_c_a_8_h.html#a91ec4adb62732a5e935da8cd519eedca",
"class_axi_sym_model.html#a77b9bb693457557d379167ca0ca2caae",
"class_bessel_1_1_roots.html#a3655d15dfb45bd990c5d1b2145ef491a",
"class_c_matrix.html#aa24eddd46b894ca71316f3fd64c4e29d",
"class_collide.html#a09b184a3d79a4b16ecd4fb21be99d075",
"class_collide.html#ac7a1d2ebfb531d212b56b928e10f273b",
"class_collide_ion.html#a61763af2e6f5b107f1a21ad36546531b",
"class_collide_ion.html#adaf088c0637422c03925346c1f1bb7b1",
"class_collision_type_diag.html#ad2eb8330ee500cce8b8ccbacb93d433b",
"class_component_header.html#a072c894dff0c7f24f68af4f786347bc9",
"class_direct.html#a7f91e164b34edc569c071e3839f904e1",
"class_dump.html#a5a9dc9f12e51ad6fd76bb9670f33f623",
"class_emp_cyl_s_l.html#a7061f742fb013273fcc69b1078ada8f8",
"class_file_create_error.html",
"class_hunter_disk.html#a5d9dc6c1a508e857e3b262fc96cb9878",
"class_ion.html#af2185a6def948e0182e2c15b8649a410",
"class_m_ndisk.html#a4a9fb4f3d860c03e500f35a4979d6179",
"class_n_t_c_1_1_n_t_citem.html#a320b87c4e1edfea772d9d8f43476653f",
"class_orient.html#a32063926f65bbdf3fb80d0b3834fab66",
"class_output_container.html#a59b4e0d99e98a1eff3ce1e3179d92f49",
"class_particle_ferry.html#a2766c3d4d30e9f2da84dcf72a37afc58",
"class_pot_accel.html#aa6d9bde455e08dc9fead4f152357cb15",
"class_res_pot_orb.html#a34cf5b258a705b1e061d9fb91fededc6",
"class_s_l_grid_cyl.html#a3139afd5e3352543ba99b9d02c3a02c8",
"class_sat_fix_orb.html#ab858a674cd302b09a0df231327381543",
"class_sphere.html#abb1ec3fb8249f1baad4ac0a371140865",
"class_spherical_orbit.html#a4e4c91a9637446127df297eae76e0223",
"class_timer.html",
"class_two_center.html#a4697b2679e1325a3cfc78ab80b094784",
"class_user_diffuse.html#a96eec0f38f762b4712be630000b1481d",
"class_user_e_bar_p.html#ac553767c2a4e2f06f5368f77da0524e7",
"class_user_r_ptest.html#a54010a172f525604b432a438c969f5e8",
"class_user_res_pot_orb.html#aedbe34c994b541f7bce991e6219f2ee4",
"class_vector.html#a57a33816590480f25f900dbbdbb6913b",
"classgffint__data.html#a34fa34b14bbedce845f9733fbf4d822c",
"classp_h_o_t.html#aed3072178967f577490ffe3249b334fe",
"classzip__container.html#af171bf124870960a16d1a03120c5cb58",
"functions_func.html",
"globals_func_s.html",
"mconf_8h.html#aa1591a4f3a86360108de5b9ba34980ca",
"numerical_8h.html#ad1d9d913acda20ca68ffd102aa92339b",
"readem_8py_source.html",
"split_temps_8py.html#af1a8655e1710a0e56c9d60808ea1b957",
"structthrd__pass___collide.html#a53adf4632e7c905c17be8cae81cfae17"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';