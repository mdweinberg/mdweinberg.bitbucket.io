var src_2tipsydefs_8h =
[
    [ "dark_particle", "structdark__particle.html", "structdark__particle" ],
    [ "dump", "structdump.html", "structdump" ],
    [ "gas_particle", "structgas__particle.html", "structgas__particle" ],
    [ "star_particle", "structstar__particle.html", "structstar__particle" ],
    [ "forever", "src_2tipsydefs_8h.html#a8c25998f80abf59bbbc3fd3011d8bba6", null ],
    [ "MAXDIM", "src_2tipsydefs_8h.html#a674c1a85fb1c09ec56b0b8f6319e7b97", null ],
    [ "Real", "src_2tipsydefs_8h.html#ab685845059e8a2c92743427d9a698c70", null ],
    [ "dark_particles", "src_2tipsydefs_8h.html#a36fece5e2f4214b23ff9ec3f639e4d11", null ],
    [ "gas_particles", "src_2tipsydefs_8h.html#ad3e6da03befb06eb46e4445d729e974d", null ],
    [ "header", "src_2tipsydefs_8h.html#a6349b18097d9888f9071e823186391e9", null ],
    [ "star_particles", "src_2tipsydefs_8h.html#a05dce9e888e23bbabf50fe010f5e3c9e", null ]
];