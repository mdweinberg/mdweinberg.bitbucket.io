var hierarchy =
[
    [ "AddDisk", "class_add_disk.html", null ],
    [ "ANGLE_GRID", "struct_a_n_g_l_e___g_r_i_d.html", null ],
    [ "AsciiHisto< T >", "class_ascii_histo.html", null ],
    [ "EmpCylSL::AxiDisk", "class_emp_cyl_s_l_1_1_axi_disk.html", [
      [ "Exponential", "class_exponential.html", null ],
      [ "MNdisk", "class_m_ndisk.html", null ],
      [ "Truncated", "class_truncated.html", null ]
    ] ],
    [ "tk::band_matrix", "classtk_1_1band__matrix.html", null ],
    [ "BarrierWrapper", "class_barrier_wrapper.html", null ],
    [ "Biorth", "class_biorth.html", [
      [ "AxiSymBiorth", "class_axi_sym_biorth.html", [
        [ "BiorthGrid", "class_biorth_grid.html", null ],
        [ "BSDisk", "class_b_s_disk.html", null ],
        [ "BSSphere", "class_b_s_sphere.html", null ],
        [ "CBDisk", "class_c_b_disk.html", null ],
        [ "CBSphere", "class_c_b_sphere.html", null ],
        [ "HQSphere", "class_h_q_sphere.html", null ],
        [ "SphereSL", "class_sphere_s_l.html", null ]
      ] ],
      [ "OneDBiorth", "class_one_d_biorth.html", [
        [ "OneDTrig", "class_one_d_trig.html", null ]
      ] ]
    ] ],
    [ "BiorthWake", "class_biorth_wake.html", null ],
    [ "BoolTranslator", "struct_bool_translator.html", null ],
    [ "BoundSort", "class_bound_sort.html", null ],
    [ "bw_struct", "classbw__struct.html", null ],
    [ "BWData", "class_b_w_data.html", null ],
    [ "C", null, [
      [ "Icont< C, K, V, Args >", "class_icont.html", null ]
    ] ],
    [ "CellDiag", "class_cell_diag.html", null ],
    [ "chdata", "classchdata.html", null ],
    [ "ChebFit", "class_cheb_fit.html", null ],
    [ "CheckpointTimer", "class_checkpoint_timer.html", null ],
    [ "chianti_data", "classchianti__data.html", [
      [ "di_data", "classdi__data.html", null ],
      [ "elvlc_data", "classelvlc__data.html", null ],
      [ "fblvl_data", "classfblvl__data.html", null ],
      [ "gffint_data", "classgffint__data.html", null ],
      [ "klgfb_data", "classklgfb__data.html", null ],
      [ "pe_data", "classpe__data.html", null ],
      [ "splups_data", "classsplups__data.html", null ],
      [ "wgfa_data", "classwgfa__data.html", null ]
    ] ],
    [ "CMatrix", "class_c_matrix.html", null ],
    [ "cmplx", "structcmplx.html", null ],
    [ "SphericalCoefs::Coefs", "struct_spherical_coefs_1_1_coefs.html", null ],
    [ "CylindricalCoefs::Coefs", "struct_cylindrical_coefs_1_1_coefs.html", null ],
    [ "Collide", "class_collide.html", [
      [ "CollideIon", "class_collide_ion.html", null ],
      [ "CollideLTE", "class_collide_l_t_e.html", null ]
    ] ],
    [ "CollisionTypeDiag", "class_collision_type_diag.html", null ],
    [ "Component", "class_component.html", null ],
    [ "ComponentContainer", "class_component_container.html", null ],
    [ "ComponentHeader", "class_component_header.html", null ],
    [ "Configuration", "class_configuration.html", null ],
    [ "coulombSelect", "classcoulomb_select.html", null ],
    [ "Cspline< X, Y >", "class_cspline.html", null ],
    [ "Cubic_Table", "class_cubic___table.html", null ],
    [ "CVector", "class_c_vector.html", [
      [ "CPoly", "class_c_poly.html", null ]
    ] ],
    [ "CylCoefHeader", "struct_cyl_coef_header.html", null ],
    [ "CylindricalCoefs", "class_cylindrical_coefs.html", null ],
    [ "dark_particle", "structdark__particle.html", null ],
    [ "Ion::di_head", "class_ion_1_1di__head.html", null ],
    [ "DiskEval", "class_disk_eval.html", null ],
    [ "DiskHalo", "class_disk_halo.html", null ],
    [ "dump", "structdump.html", null ],
    [ "Dump", "class_dump.html", null ],
    [ "EL3", "class_e_l3.html", null ],
    [ "Elastic", "class_elastic.html", null ],
    [ "Cspline< X, Y >::Element", "class_cspline_1_1_element.html", null ],
    [ "EllipForce", "class_ellip_force.html", null ],
    [ "EllipsoidForce", "class_ellipsoid_force.html", null ],
    [ "EmpCylSL", "class_emp_cyl_s_l.html", null ],
    [ "Ensemble", "class_ensemble.html", null ],
    [ "Enum", null, [
      [ "psp_dist.FitType", "classpsp__dist_1_1_fit_type.html", null ],
      [ "psp_dist.PlotType", "classpsp__dist_1_1_plot_type.html", null ],
      [ "psp_distL.FitType", "classpsp__dist_l_1_1_fit_type.html", null ],
      [ "psp_distL.PlotType", "classpsp__dist_l_1_1_plot_type.html", null ]
    ] ],
    [ "EnumBitset< T >", "class_enum_bitset.html", null ],
    [ "EnumBitset< KE_Flags >", "class_enum_bitset.html", null ],
    [ "EnumTraits< T >", "struct_enum_traits.html", null ],
    [ "EnumTraits< KE_Flags >", "struct_enum_traits_3_01_k_e___flags_01_4.html", null ],
    [ "eqULL", "structeq_u_l_l.html", null ],
    [ "CollideIon::Pord::Es", "struct_collide_ion_1_1_pord_1_1_es.html", null ],
    [ "exception", null, [
      [ "EXPException", "class_e_x_p_exception.html", [
        [ "BadIndexException", "class_bad_index_exception.html", null ],
        [ "FileCreateError", "class_file_create_error.html", null ],
        [ "FileOpenError", "class_file_open_error.html", null ],
        [ "GenericError", "class_generic_error.html", null ],
        [ "InternalError", "class_internal_error.html", null ]
      ] ],
      [ "NTC::NTCitem::Error", "class_n_t_c_1_1_n_t_citem_1_1_error.html", null ],
      [ "TopBase::Error", "class_top_base_1_1_error.html", [
        [ "TopBase::FileOpen", "class_top_base_1_1_file_open.html", null ],
        [ "TopBase::NoIon", "class_top_base_1_1_no_ion.html", null ],
        [ "TopBase::NoLine", "class_top_base_1_1_no_line.html", null ],
        [ "TopBase::NoSLP", "class_top_base_1_1_no_s_l_p.html", null ]
      ] ]
    ] ],
    [ "ExternalCollection", "class_external_collection.html", null ],
    [ "FDIST", "class_f_d_i_s_t.html", null ],
    [ "foarray", "classfoarray.html", null ],
    [ "Frame_Rotation", "class_frame___rotation.html", null ],
    [ "CollideIon::Fspc", "class_collide_ion_1_1_fspc.html", null ],
    [ "Func1d", "class_func1d.html", [
      [ "FindOrb", "class_find_orb.html", null ]
    ] ],
    [ "gas_particle", "structgas__particle.html", null ],
    [ "GAUSS", "struct_g_a_u_s_s.html", null ],
    [ "GaussQuad", "class_gauss_quad.html", [
      [ "HermQuad", "class_herm_quad.html", null ],
      [ "JacoQuad", "class_jaco_quad.html", [
        [ "LegeQuad", "class_lege_quad.html", null ]
      ] ],
      [ "LaguQuad", "class_lagu_quad.html", null ]
    ] ],
    [ "Geometric", "class_geometric.html", null ],
    [ "hashULL", "structhash_u_l_l.html", null ],
    [ "HeatCool", "class_heat_cool.html", null ],
    [ "Hernquist", "class_hernquist.html", null ],
    [ "Info", "class_info.html", null ],
    [ "psp_io.Input", "classpsp__io_1_1_input.html", null ],
    [ "NTC::Interact< U >", "class_n_t_c_1_1_interact.html", null ],
    [ "CollideIon::InteractData", "struct_collide_ion_1_1_interact_data.html", null ],
    [ "Interaction", "class_interaction.html", null ],
    [ "InteractSelect", "class_interact_select.html", null ],
    [ "Interp1d", "class_interp1d.html", [
      [ "Cheby1d", "class_cheby1d.html", null ],
      [ "Linear1d", "class_linear1d.html", null ],
      [ "Spline1d", "class_spline1d.html", null ]
    ] ],
    [ "Ion", "class_ion.html", null ],
    [ "Isothermal", "class_isothermal.html", null ],
    [ "iterator", null, [
      [ "CyclicIterator< T, Iterator >", "class_cyclic_iterator.html", null ],
      [ "RingIterator< T, Container, Iterator >", "class_ring_iterator.html", null ]
    ] ],
    [ "KComplex", "class_k_complex.html", null ],
    [ "kdtree< coordinate_type, dimensions >", "classkdtree.html", null ],
    [ "CollideIon::KE_", "class_collide_ion_1_1_k_e__.html", null ],
    [ "KeyConvert", "class_key_convert.html", null ],
    [ "KLGFdata", "class_k_l_g_fdata.html", null ],
    [ "loadb_datum", "structloadb__datum.html", null ],
    [ "Logarithmic", "class_logarithmic.html", null ],
    [ "ltEL3", "structlt_e_l3.html", null ],
    [ "ltPAIR", "structlt_p_a_i_r.html", null ],
    [ "map", null, [
      [ "Cache< key_t, value_t >", "class_cache.html", null ],
      [ "Cache< double, kdtree::node * >", "class_cache.html", null ],
      [ "Collide::Labels", "class_collide_1_1_labels.html", null ],
      [ "EmpCylSL::PCAbasis", "class_emp_cyl_s_l_1_1_p_c_abasis.html", null ],
      [ "NTC::NTCitem::udMap", "class_n_t_c_1_1_n_t_citem_1_1ud_map.html", null ],
      [ "NTC::NTCitem::uqMap", "class_n_t_c_1_1_n_t_citem_1_1uq_map.html", null ]
    ] ],
    [ "MassModel", "class_mass_model.html", [
      [ "AxiSymModel", "class_axi_sym_model.html", [
        [ "CylDisk", "class_cyl_disk.html", null ],
        [ "DiskWithHalo", "class_disk_with_halo.html", null ],
        [ "EmbeddedDiskModel", "class_embedded_disk_model.html", null ],
        [ "ExponentialDisk", "class_exponential_disk.html", null ],
        [ "HernquistSphere", "class_hernquist_sphere.html", null ],
        [ "HunterDisk", "class_hunter_disk.html", null ],
        [ "HunterDiskX", "class_hunter_disk_x.html", null ],
        [ "IsothermalSphere", "class_isothermal_sphere.html", null ],
        [ "KalnajsDisk", "class_kalnajs_disk.html", null ],
        [ "LowSingIsothermalSphere", "class_low_sing_isothermal_sphere.html", null ],
        [ "MestelDisk", "class_mestel_disk.html", null ],
        [ "PlummerSphere", "class_plummer_sphere.html", null ],
        [ "SingIsothermalSphere", "class_sing_isothermal_sphere.html", null ],
        [ "SphericalModelMulti", "class_spherical_model_multi.html", null ],
        [ "SphericalModelTable", "class_spherical_model_table.html", [
          [ "KingSphere", "class_king_sphere.html", null ]
        ] ],
        [ "ToomreDisk", "class_toomre_disk.html", null ]
      ] ]
    ] ],
    [ "MasterHeader", "class_master_header.html", null ],
    [ "Matrix", "class_matrix.html", null ],
    [ "NTC::maxDouble", "class_n_t_c_1_1max_double.html", null ],
    [ "MixtureBasis", "class_mixture_basis.html", null ],
    [ "Miyamoto", "class_miyamoto.html", null ],
    [ "Miyamoto_Needle", "class_miyamoto___needle.html", null ],
    [ "Modified_Hubble", "class_modified___hubble.html", null ],
    [ "MVector< T >", "class_m_vector.html", null ],
    [ "PotAccel::NData", "struct_pot_accel_1_1_n_data.html", null ],
    [ "Needle", "class_needle.html", null ],
    [ "kdtree< coordinate_type, dimensions >::node", "structkdtree_1_1node.html", null ],
    [ "kdtree< coordinate_type, dimensions >::node_cmp", "structkdtree_1_1node__cmp.html", null ],
    [ "NTC::NTCdb", "class_n_t_c_1_1_n_t_cdb.html", null ],
    [ "NTC::NTCitem", "class_n_t_c_1_1_n_t_citem.html", null ],
    [ "nvTracer", "classnv_tracer.html", null ],
    [ "object", null, [
      [ "psp_io.particle_holder", "classpsp__io_1_1particle__holder.html", null ]
    ] ],
    [ "OrbitTable", "class_orbit_table.html", null ],
    [ "OrbResTable", "class_orb_res_table.html", null ],
    [ "OrbValues", "struct_orb_values.html", null ],
    [ "Orient", "class_orient.html", null ],
    [ "OrthoPoly", "class_ortho_poly.html", [
      [ "Cheb1", "class_cheb1.html", null ],
      [ "Cheb2", "class_cheb2.html", null ],
      [ "GenLagu", "class_gen_lagu.html", null ],
      [ "Hermite", "class_hermite.html", null ],
      [ "Legendre", "class_legendre.html", [
        [ "PVQuad", "class_p_v_quad.html", null ]
      ] ],
      [ "Ultra", "class_ultra.html", null ]
    ] ],
    [ "Output", "class_output.html", [
      [ "OrbTrace", "class_orb_trace.html", null ],
      [ "OutAscii", "class_out_ascii.html", null ],
      [ "OutCalbr", "class_out_calbr.html", null ],
      [ "OutCHKPT", "class_out_c_h_k_p_t.html", null ],
      [ "OutCHKPTQ", "class_out_c_h_k_p_t_q.html", null ],
      [ "OutCoef", "class_out_coef.html", null ],
      [ "OutDiag", "class_out_diag.html", null ],
      [ "OutFrac", "class_out_frac.html", null ],
      [ "OutLog", "class_out_log.html", null ],
      [ "OutMulti", "class_out_multi.html", null ],
      [ "OutPS", "class_out_p_s.html", null ],
      [ "OutPSN", "class_out_p_s_n.html", null ],
      [ "OutPSP", "class_out_p_s_p.html", null ],
      [ "OutPSQ", "class_out_p_s_q.html", null ],
      [ "OutRelaxation", "class_out_relaxation.html", null ]
    ] ],
    [ "OutputContainer", "class_output_container.html", null ],
    [ "pair", null, [
      [ "CollideIon::orderedPair", "class_collide_ion_1_1ordered_pair.html", null ]
    ] ],
    [ "Particle", "class_particle.html", null ],
    [ "ParticleFerry", "class_particle_ferry.html", null ],
    [ "EmpCylSL::PCAelement", "class_emp_cyl_s_l_1_1_p_c_aelement.html", null ],
    [ "Perfect", "class_perfect.html", null ],
    [ "PeriodicTable", "class_periodic_table.html", null ],
    [ "Perturbation", "class_perturbation.html", [
      [ "BarForcing", "class_bar_forcing.html", null ],
      [ "CircularOrbit", "class_circular_orbit.html", null ]
    ] ],
    [ "Phase", "class_phase.html", null ],
    [ "pHOT", "classp_h_o_t.html", null ],
    [ "pHOT_iterator", "classp_h_o_t__iterator.html", null ],
    [ "Plummer", "class_plummer.html", null ],
    [ "point< coordinate_type, dimensions >", "classpoint.html", null ],
    [ "Point_Quadrupole", "class_point___quadrupole.html", null ],
    [ "CollideIon::Pord", "class_collide_ion_1_1_pord.html", null ],
    [ "PotAccel", "class_pot_accel.html", [
      [ "Basis", "class_basis.html", [
        [ "AxisymmetricBasis", "class_axisymmetric_basis.html", [
          [ "CBrockDisk", "class_c_brock_disk.html", null ],
          [ "SphericalBasis", "class_spherical_basis.html", [
            [ "Bessel", "class_bessel.html", null ],
            [ "CBrock", "class_c_brock.html", null ],
            [ "Sphere", "class_sphere.html", null ]
          ] ]
        ] ],
        [ "Cylinder", "class_cylinder.html", null ]
      ] ],
      [ "Cube", "class_cube.html", null ],
      [ "Direct", "class_direct.html", null ],
      [ "ExternalForce", "class_external_force.html", [
        [ "externalShock", "classexternal_shock.html", null ],
        [ "generateRelaxation", "classgenerate_relaxation.html", null ],
        [ "HaloBulge", "class_halo_bulge.html", null ],
        [ "SatFix", "class_sat_fix.html", null ],
        [ "SatFixOrb", "class_sat_fix_orb.html", null ],
        [ "ScatterMFP", "class_scatter_m_f_p.html", null ],
        [ "tidalField", "classtidal_field.html", null ],
        [ "TreeDSMC", "class_tree_d_s_m_c.html", null ],
        [ "UserAddMass", "class_user_add_mass.html", null ],
        [ "UserAtmos", "class_user_atmos.html", null ],
        [ "UserBar", "class_user_bar.html", null ],
        [ "UserBarTrigger", "class_user_bar_trigger.html", null ],
        [ "UserDiffRot", "class_user_diff_rot.html", null ],
        [ "UserDiffuse", "class_user_diffuse.html", null ],
        [ "UserDisk", "class_user_disk.html", null ],
        [ "UserDSMC", "class_user_d_s_m_c.html", null ],
        [ "UserEBar", "class_user_e_bar.html", null ],
        [ "UserEBarN", "class_user_e_bar_n.html", null ],
        [ "UserEBarP", "class_user_e_bar_p.html", null ],
        [ "UserEBarS", "class_user_e_bar_s.html", null ],
        [ "UserHalo", "class_user_halo.html", null ],
        [ "UserLogPot", "class_user_log_pot.html", null ],
        [ "UserMassEvo", "class_user_mass_evo.html", null ],
        [ "UserMNdisk", "class_user_m_ndisk.html", null ],
        [ "UserPeriodic", "class_user_periodic.html", null ],
        [ "UserProfile", "class_user_profile.html", null ],
        [ "UserPST", "class_user_p_s_t.html", null ],
        [ "UserReflect", "class_user_reflect.html", null ],
        [ "UserResPot", "class_user_res_pot.html", null ],
        [ "UserResPotN", "class_user_res_pot_n.html", null ],
        [ "UserResPotOrb", "class_user_res_pot_orb.html", null ],
        [ "UserRPtest", "class_user_r_ptest.html", null ],
        [ "UserSatWake", "class_user_sat_wake.html", null ],
        [ "UserShear", "class_user_shear.html", null ],
        [ "UserSlabHalo", "class_user_slab_halo.html", null ],
        [ "UserSNheat", "class_user_s_nheat.html", null ],
        [ "UserTest", "class_user_test.html", null ],
        [ "UserTidalRad", "class_user_tidal_rad.html", null ],
        [ "UserWake", "class_user_wake.html", null ]
      ] ],
      [ "NoForce", "class_no_force.html", null ],
      [ "Shells", "class_shells.html", null ],
      [ "Slab", "class_slab.html", null ],
      [ "SlabSL", "class_slab_s_l.html", null ],
      [ "TwoCenter", "class_two_center.html", [
        [ "EJcom", "class_e_jcom.html", null ]
      ] ]
    ] ],
    [ "PParticle< real >", "class_p_particle.html", null ],
    [ "proxysatfix", "classproxysatfix.html", null ],
    [ "proxysatorb", "classproxysatorb.html", null ],
    [ "PSP", "class_p_s_p.html", [
      [ "PSPout", "class_p_s_pout.html", null ],
      [ "PSPspl", "class_p_s_pspl.html", null ]
    ] ],
    [ "psp_io.PSPDump", "classpsp__io_1_1_p_s_p_dump.html", null ],
    [ "PSPDump", "class_p_s_p_dump.html", null ],
    [ "PSPstanza", "class_p_s_pstanza.html", null ],
    [ "QPDistF", "class_q_p_dist_f.html", null ],
    [ "Quadrupole_Bar", "class_quadrupole___bar.html", null ],
    [ "testmed.Quantile", "classtestmed_1_1_quantile.html", null ],
    [ "NTC::Quantile", "class_n_t_c_1_1_quantile.html", null ],
    [ "NTC::QuantileBag", "class_n_t_c_1_1_quantile_bag.html", null ],
    [ "RadiusSort", "class_radius_sort.html", null ],
    [ "HeatCool::Rates", "struct_heat_cool_1_1_rates.html", null ],
    [ "CollideIon::RecombRatio", "class_collide_ion_1_1_recomb_ratio.html", null ],
    [ "RegularOrbit", "class_regular_orbit.html", [
      [ "SphericalOrbit", "class_spherical_orbit.html", null ]
    ] ],
    [ "RespMat", "class_resp_mat.html", null ],
    [ "ResPot", "class_res_pot.html", null ],
    [ "ResPotOrb", "class_res_pot_orb.html", null ],
    [ "Bessel::RGrid", "class_bessel_1_1_r_grid.html", null ],
    [ "Bessel::Roots", "class_bessel_1_1_roots.html", null ],
    [ "RUN", "class_r_u_n.html", null ],
    [ "RunningTime", "class_running_time.html", null ],
    [ "runtime_error", null, [
      [ "DiskHaloException", "class_disk_halo_exception.html", null ]
    ] ],
    [ "ResPotOrb::RW", "class_res_pot_orb_1_1_r_w.html", null ],
    [ "ResPot::RW", "class_res_pot_1_1_r_w.html", null ],
    [ "sCell", "classs_cell.html", [
      [ "pCell", "classp_cell.html", null ]
    ] ],
    [ "boost::serialization::Serialize< N >", "structboost_1_1serialization_1_1_serialize.html", null ],
    [ "boost::serialization::Serialize< 0 >", "structboost_1_1serialization_1_1_serialize_3_010_01_4.html", null ],
    [ "SimAnneal", "class_sim_anneal.html", null ],
    [ "sKeyCollTD", null, [
      [ "collDiag", "classcoll_diag.html", null ]
    ] ],
    [ "Slab::SlabCoefHeader", "struct_slab_1_1_slab_coef_header.html", null ],
    [ "SlabSL::SlabSLCoefHeader", "struct_slab_s_l_1_1_slab_s_l_coef_header.html", null ],
    [ "SLGridCyl", "class_s_l_grid_cyl.html", null ],
    [ "SLGridSlab", "class_s_l_grid_slab.html", null ],
    [ "SLGridSph", "class_s_l_grid_sph.html", null ],
    [ "SParticle", "class_s_particle.html", null ],
    [ "SphCoefHeader", "struct_sph_coef_header.html", null ],
    [ "SphericalCoefs", "class_spherical_coefs.html", null ],
    [ "SphericalSL", "class_spherical_s_l.html", null ],
    [ "tk::spline", "classtk_1_1spline.html", null ],
    [ "Spline2d", "class_spline2d.html", null ],
    [ "SPtype", "class_s_ptype.html", null ],
    [ "star_particle", "structstar__particle.html", null ],
    [ "StatsMPI", "class_stats_m_p_i.html", null ],
    [ "StringTok< T >", "class_string_tok.html", null ],
    [ "Swap< T >", "class_swap.html", null ],
    [ "TableCyl", "class_table_cyl.html", null ],
    [ "TableSlab", "class_table_slab.html", null ],
    [ "TableSph", "class_table_sph.html", null ],
    [ "TopBase::TBline", "struct_top_base_1_1_t_bline.html", null ],
    [ "thrd_pass_arguments", "structthrd__pass__arguments.html", null ],
    [ "thrd_pass_Collide", "structthrd__pass___collide.html", null ],
    [ "thrd_pass_posvel", "structthrd__pass__posvel.html", null ],
    [ "thrd_pass_PotAccel", "structthrd__pass___pot_accel.html", null ],
    [ "thrd_pass_tstep", "structthrd__pass__tstep.html", null ],
    [ "Three_Vector", "class_three___vector.html", null ],
    [ "Time", "class_time.html", null ],
    [ "Timer", "class_timer.html", null ],
    [ "TimeSeriesCoefs", "class_time_series_coefs.html", null ],
    [ "TopBase", "class_top_base.html", null ],
    [ "Trajectory", "class_trajectory.html", [
      [ "LinearOrbit", "class_linear_orbit.html", null ],
      [ "SatelliteOrbit", "class_satellite_orbit.html", null ],
      [ "UnboundOrbit", "class_unbound_orbit.html", null ]
    ] ],
    [ "boost::property_tree::translator_between< std::basic_string< Ch, Traits, Alloc >, bool >", "structboost_1_1property__tree_1_1translator__between_3_01std_1_1basic__string_3_01_ch_00_01_trai0d72484622a9a8e53eb6b5158b3b5cb9.html", null ],
    [ "tstep_pass_arguments", "structtstep__pass__arguments.html", null ],
    [ "tuple", null, [
      [ "AtomicElement", "class_atomic_element.html", null ]
    ] ],
    [ "TwoBodyDiffuse", "class_two_body_diffuse.html", null ],
    [ "vbkts", "classvbkts.html", null ],
    [ "Vector", "class_vector.html", [
      [ "Poly", "class_poly.html", null ]
    ] ],
    [ "vector", null, [
      [ "EmpCylSL::MatrixArray", "struct_emp_cyl_s_l_1_1_matrix_array.html", null ],
      [ "EmpCylSL::MstepArray", "struct_emp_cyl_s_l_1_1_mstep_array.html", null ]
    ] ],
    [ "VernerData", "class_verner_data.html", null ],
    [ "VernerData::VernerRec", "struct_verner_data_1_1_verner_rec.html", null ],
    [ "VtkGrid", "class_vtk_grid.html", null ],
    [ "VtkPCA", "class_vtk_p_c_a.html", null ],
    [ "AxiSymModel::WRgrid", "class_axi_sym_model_1_1_w_rgrid.html", null ],
    [ "CollideIon::XStup", "struct_collide_ion_1_1_x_stup.html", null ],
    [ "ZBrent< T >", "class_z_brent.html", null ],
    [ "zip_container< C1, C2 >", "classzip__container.html", null ]
];