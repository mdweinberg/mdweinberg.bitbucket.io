var classpsp__io_1_1particle__holder =
[
    [ "comp", "classpsp__io_1_1particle__holder.html#a63bc04cb678838f7e08984404b63c9b4", null ],
    [ "infile", "classpsp__io_1_1particle__holder.html#abbecf0029a842604f9bffb8c5d8093fb", null ],
    [ "mass", "classpsp__io_1_1particle__holder.html#a8da9e4d506d45ec97726921c15e600bb", null ],
    [ "nbodies", "classpsp__io_1_1particle__holder.html#a63fe545a1a6de13c556fc053ecc84f7b", null ],
    [ "pote", "classpsp__io_1_1particle__holder.html#a3ec1916b1793d0d57c40919633fe9bf9", null ],
    [ "time", "classpsp__io_1_1particle__holder.html#ad31daaaa196fa3e333349f0d52bffa41", null ],
    [ "xpos", "classpsp__io_1_1particle__holder.html#a0a7b70c304b2cb55268718e749c1b6e1", null ],
    [ "xvel", "classpsp__io_1_1particle__holder.html#ac074e477e6252b6498d96ec01425df84", null ],
    [ "ypos", "classpsp__io_1_1particle__holder.html#a246550ede708c4c6b689cd32d48fe206", null ],
    [ "yvel", "classpsp__io_1_1particle__holder.html#aba39a34b04e89b5de1d9192ca4506a77", null ],
    [ "zpos", "classpsp__io_1_1particle__holder.html#a0f4c42a69d057d1bb9afc19d5d6b791d", null ],
    [ "zvel", "classpsp__io_1_1particle__holder.html#a8155ccae505604fc242b821dcf6a00ba", null ]
];