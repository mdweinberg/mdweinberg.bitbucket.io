var class_k_l_g_fdata =
[
    [ "lQ", "class_k_l_g_fdata.html#a36b158b3c92c7963c7d0ab42262202c8", null ],
    [ "splMap", "class_k_l_g_fdata.html#a390a360bdb1b0e91a1ccb09c54fbca1c", null ],
    [ "tksplPtr", "class_k_l_g_fdata.html#a3a339c9f6d99bd1f9982eff9fa3a36e8", null ],
    [ "KLGFdata", "class_k_l_g_fdata.html#a6de56caa030c1d3fd417df9161465ac2", null ],
    [ "initialize", "class_k_l_g_fdata.html#a24dad9ed917f086cbd3f385659b03b89", null ],
    [ "operator()", "class_k_l_g_fdata.html#a47e4bd48f33151d43404c002e91b909a", null ],
    [ "ch", "class_k_l_g_fdata.html#a3b24c4460813d817f5ba11376660d20e", null ],
    [ "gfb", "class_k_l_g_fdata.html#a0332e50fc946bd989ef44811371e7cfe", null ],
    [ "ngfb", "class_k_l_g_fdata.html#a49c85b13ebc9b273219db31172ffcf29", null ],
    [ "nume", "class_k_l_g_fdata.html#a412b785c8d05fa44eaf780dc9660df7a", null ],
    [ "pe", "class_k_l_g_fdata.html#a9ee79faf2779a008304c97f2b9077119", null ],
    [ "spl", "class_k_l_g_fdata.html#a7de821fcf146cf14b79d27bae3f76e6a", null ]
];