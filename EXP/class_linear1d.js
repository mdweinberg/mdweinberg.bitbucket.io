var class_linear1d =
[
    [ "Linear1d", "class_linear1d.html#a8f4612cd47eb448ad86cd0ae7da8f993", null ],
    [ "Linear1d", "class_linear1d.html#aaf350702ae7adef50e7fceb61cb5a90b", null ],
    [ "Linear1d", "class_linear1d.html#a1899c71b4ab726f99f54c944de3fae7c", null ],
    [ "~Linear1d", "class_linear1d.html#a9e975abf68566a3f7d2e997523e83ca5", null ],
    [ "deriv", "class_linear1d.html#a7cca991f83a09a3183d721703a32ca7f", null ],
    [ "eval", "class_linear1d.html#a59080ed1d410b65d44fdba854be86daf", null ],
    [ "operator=", "class_linear1d.html#af417f5e4fa40fa9ee3d7329e632847c9", null ],
    [ "x", "class_linear1d.html#aa11c1be527e4e042a5b1b94ff9631587", null ],
    [ "y", "class_linear1d.html#a64ac07ea843969a8ec373a5519f96276", null ]
];