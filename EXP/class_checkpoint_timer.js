var class_checkpoint_timer =
[
    [ "CheckpointTimer", "class_checkpoint_timer.html#a0db0cad1676b37b951f2090ed81e1b98", null ],
    [ "done", "class_checkpoint_timer.html#ae499ed7b77e007cf5dafbcc4c38f4db0", null ],
    [ "exec", "class_checkpoint_timer.html#a379699e30dce1f0f00cbe7eaf5d32e7c", null ],
    [ "mark", "class_checkpoint_timer.html#a0ea7a8d2dae19263a12f84f2b4c63ec1", null ],
    [ "time_remaining", "class_checkpoint_timer.html#a437a8c78ffee49e869515880ec168d43", null ],
    [ "current", "class_checkpoint_timer.html#a3c4bd3a62951f7c9b67f3e053971ef21", null ],
    [ "delta", "class_checkpoint_timer.html#a8c84d425bdb5d8bc7335ac1837b2237a", null ],
    [ "diag", "class_checkpoint_timer.html#a1d024346c080894f6e9a67f574d0d9dd", null ],
    [ "final", "class_checkpoint_timer.html#afc444c3c26a6feca68eff5ab196d6e5f", null ],
    [ "firstime", "class_checkpoint_timer.html#ac2e1f6ff125f2518bd3009ff53f8c9f5", null ],
    [ "initial", "class_checkpoint_timer.html#a58ce29c92dde59ea4ae1e98b2317a942", null ],
    [ "last", "class_checkpoint_timer.html#a2a3cca057286c97f19422b479ada3e89", null ],
    [ "last_diag", "class_checkpoint_timer.html#aaa0538d7eef846b6f14272b6ae142dfd", null ],
    [ "mean", "class_checkpoint_timer.html#aa926ca99081c647d7c322a513ba7de84", null ],
    [ "nsteps", "class_checkpoint_timer.html#aa16af43e0a33230254c29ae85b684d05", null ],
    [ "var", "class_checkpoint_timer.html#ac71ff57adaf031073bc502b01829768e", null ]
];