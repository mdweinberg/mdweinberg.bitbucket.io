var class_perfect =
[
    [ "dFlambda", "class_perfect.html#a1644ee2706020559cd4e22cd486e1c0f", null ],
    [ "dFmu", "class_perfect.html#a02f633bdfe059be45e48af2a088ec3bb", null ],
    [ "Feff_lambda", "class_perfect.html#a6f894270e46a92541e458e06b6384aa0", null ],
    [ "Feff_mu", "class_perfect.html#af427445e6f2f2afb513eb096742fca22", null ],
    [ "Flambda", "class_perfect.html#a5d026dae2cd54736ad08e16ff288d980", null ],
    [ "Fmu", "class_perfect.html#ab36d5d92fdbc908799239e91eb6ce189", null ],
    [ "force", "class_perfect.html#ae09443d4e89b9ddce7ce787a2ba5fe52", null ],
    [ "Integrals", "class_perfect.html#a7404d9104dc628ab6a3f5e37a2edf578", null ],
    [ "meridional_force", "class_perfect.html#ac88aa214acebaac41589bd40ef03b3bd", null ],
    [ "potential", "class_perfect.html#a1179c1d11123c1bfac6763f192252e0b", null ],
    [ "second_integral", "class_perfect.html#af195119e1b577189d6ee84b007f558fa", null ],
    [ "set_Perfect", "class_perfect.html#ab23c19427f87549d48bc9b4b8eeac716", null ],
    [ "to_ellipsoidal", "class_perfect.html#a1081897f8093fd178a7dec27ac97b46c", null ],
    [ "a", "class_perfect.html#a6649f7890c55659d185fc593e6f87841", null ],
    [ "alpha", "class_perfect.html#addada398ddf928531d93ad88efdc3b03", null ],
    [ "b", "class_perfect.html#a352135ef809687ada5638565ac66d622", null ],
    [ "beta", "class_perfect.html#a0b498c98fcaad321059a6b4bb2e99c75", null ],
    [ "G", "class_perfect.html#ad8f19c3ab380ef6f3c5bf27cba5dd589", null ],
    [ "M", "class_perfect.html#a896e531af1ece39d761550299e9a4ae2", null ],
    [ "rho0", "class_perfect.html#a20e44215515e62dfc1143c754faeeba7", null ],
    [ "W0", "class_perfect.html#a133252f94881eff71e139a7b81e4c67c", null ]
];