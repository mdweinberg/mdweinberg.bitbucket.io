var structcuda_timer =
[
    [ "cudaTimer", "structcuda_timer.html#a33f36da682b0e5bda57020e044415d1f", null ],
    [ "~cudaTimer", "structcuda_timer.html#add455e3fd41a78a8827cc6f82202910c", null ],
    [ "elapsed", "structcuda_timer.html#abe133ebe37e90f45f393f07701bb5359", null ],
    [ "epsilon", "structcuda_timer.html#af8ee3cb40baeec57af48613868396056", null ],
    [ "restart", "structcuda_timer.html#a1d602f7d9cbfb0fca9e8fb61e5d1fae8", null ],
    [ "end", "structcuda_timer.html#acf099b000ebcc3cadb21d04b8e662571", null ],
    [ "start", "structcuda_timer.html#a6c295c69e951853a7c126bad2458e5c6", null ]
];