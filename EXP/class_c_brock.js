var class_c_brock =
[
    [ "CBrock", "class_c_brock.html#af31337a37124aa9b9c6914dfc381aac4", null ],
    [ "d_r_to_rh", "class_c_brock.html#a87787440d4245547937d9359c5508a08", null ],
    [ "get_dens", "class_c_brock.html#a6ff4da70d1aa662fae4ebf905198e655", null ],
    [ "get_dpotl", "class_c_brock.html#a951a60fd7a303ddd105f8d9d15f62c8d", null ],
    [ "get_potl", "class_c_brock.html#ab2c0acae0af4db2b0cf43a27f3c81538", null ],
    [ "get_potl_dens", "class_c_brock.html#ae66f74b349a63026c4978936e166422a", null ],
    [ "initialize", "class_c_brock.html#aae55f8c8c93c8f72fc62a2f997957126", null ],
    [ "knl", "class_c_brock.html#a866a5547abfcb0eaa63c0e98493058bf", null ],
    [ "norm", "class_c_brock.html#a4841bdef4ef80cb9a56bd40fc7ce5b7f", null ],
    [ "r_to_rh", "class_c_brock.html#a6ff0a84cee5c07e10b25b6508c8625c8", null ],
    [ "rh_to_r", "class_c_brock.html#a0c6566ef5da2be817308b0d7ad16432a", null ],
    [ "duu", "class_c_brock.html#a21680af2a4c6b3ae0bf3cfd8b467ab8f", null ],
    [ "uu", "class_c_brock.html#ab96410b9edce5fe69fec3d270f8cacdc", null ]
];