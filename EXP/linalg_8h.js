var linalg_8h =
[
    [ "determinant", "linalg_8h.html#ac17293293993fccb80e3dfd2ef0c32bb", null ],
    [ "embed_matrix", "linalg_8h.html#a652ae6830710f5c3163f2eddbb0b8f5b", null ],
    [ "improve", "linalg_8h.html#a6e8d3b674577e1418e882fe020eabccf", null ],
    [ "inverse", "linalg_8h.html#acad0c160b13faafd38b62b75b2ab9aa0", null ],
    [ "linear_solve", "linalg_8h.html#ae796129d40506496257489bed36171d8", null ],
    [ "lu_backsub", "linalg_8h.html#a7ec66624a4c9907906b29b40aad7ab6a", null ],
    [ "lu_decomp", "linalg_8h.html#a9f42039ad28f409201b60179e1c491cb", null ],
    [ "lu_determinant", "linalg_8h.html#aeb8252f66291a97adf7d9e1e60b6a053", null ],
    [ "sub_matrix", "linalg_8h.html#afc0ecf6a1bbc7074c4181ae33253e8c8", null ]
];