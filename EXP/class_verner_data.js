var class_verner_data =
[
    [ "VernerRec", "struct_verner_data_1_1_verner_rec.html", "struct_verner_data_1_1_verner_rec" ],
    [ "lQ", "class_verner_data.html#a25c4bf185021539d9a11125cf8af3568", null ],
    [ "vrPtr", "class_verner_data.html#ad7b519b7d4c0d8583263ce9e18157ef8", null ],
    [ "vrVec", "class_verner_data.html#a6798429d09b4e6a990e1896420de87a5", null ],
    [ "VernerData", "class_verner_data.html#a79abf99d193081e611f2a587d45061be", null ],
    [ "cross", "class_verner_data.html#a9efcc47d7d7badff924ce727bcddfe7e", null ],
    [ "crossPhotoIon", "class_verner_data.html#a76b404b9384873e817a6f068b84b677f", null ],
    [ "crossPhotoIon_VFKY", "class_verner_data.html#a22e144104449b644e1292e7d6ba3e7d7", null ],
    [ "initialize", "class_verner_data.html#afcecce545752efa5de8315d3613f8150", null ],
    [ "ch", "class_verner_data.html#ab4301b81e07573eb31e72f3486412ebb", null ],
    [ "data", "class_verner_data.html#ac352686eba865a3bf4603e542782578c", null ]
];