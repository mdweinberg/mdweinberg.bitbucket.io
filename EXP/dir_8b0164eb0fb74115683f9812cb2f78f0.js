var dir_8b0164eb0fb74115683f9812cb2f78f0 =
[
    [ "BarForcing.H", "_bar_forcing_8_h.html", [
      [ "BarForcing", "class_bar_forcing.html", "class_bar_forcing" ]
    ] ],
    [ "CircularOrbit.H", "_circular_orbit_8_h.html", [
      [ "CircularOrbit", "class_circular_orbit.html", "class_circular_orbit" ]
    ] ],
    [ "EllipForce.H", "_ellip_force_8_h.html", [
      [ "EllipForce", "class_ellip_force.html", "class_ellip_force" ]
    ] ],
    [ "LinearOrbit.H", "_linear_orbit_8_h.html", [
      [ "LinearOrbit", "class_linear_orbit.html", "class_linear_orbit" ]
    ] ],
    [ "MapWithDef.H", "_map_with_def_8_h.html", "_map_with_def_8_h" ],
    [ "MVector.H", "_m_vector_8_h.html", "_m_vector_8_h" ],
    [ "newargs.py", "newargs_8py.html", "newargs_8py" ],
    [ "Perturbation.H", "_perturbation_8_h.html", [
      [ "Perturbation", "class_perturbation.html", "class_perturbation" ]
    ] ],
    [ "plot_h.py", "plot__h_8py.html", "plot__h_8py" ],
    [ "plot_he.py", "plot__he_8py.html", "plot__he_8py" ],
    [ "plotRR.py", "plot_r_r_8py.html", "plot_r_r_8py" ],
    [ "plotVY.py", "plot_v_y_8py.html", "plot_v_y_8py" ],
    [ "plotVYlin.py", "plot_v_ylin_8py.html", "plot_v_ylin_8py" ],
    [ "RespMat3.h", "_resp_mat3_8h.html", "_resp_mat3_8h" ],
    [ "ResPot.H", "_res_pot_8_h.html", "_res_pot_8_h" ],
    [ "ResPotOrb.H", "_res_pot_orb_8_h.html", "_res_pot_orb_8_h" ],
    [ "SatelliteOrbit.h", "_satellite_orbit_8h.html", [
      [ "SatelliteOrbit", "class_satellite_orbit.html", "class_satellite_orbit" ]
    ] ],
    [ "SatFix.H", "_sat_fix_8_h.html", "_sat_fix_8_h" ],
    [ "SatFixOrb.H", "_sat_fix_orb_8_h.html", "_sat_fix_orb_8_h" ],
    [ "testmed.py", "testmed_8py.html", "testmed_8py" ],
    [ "TimeSeriesCoefs.H", "_time_series_coefs_8_h.html", [
      [ "TimeSeriesCoefs", "class_time_series_coefs.html", "class_time_series_coefs" ]
    ] ],
    [ "Trajectory.H", "_trajectory_8_h.html", [
      [ "Trajectory", "class_trajectory.html", "class_trajectory" ]
    ] ],
    [ "Tuple.H", "_tuple_8_h.html", "_tuple_8_h" ],
    [ "TwoBodyDiffuse.H", "_two_body_diffuse_8_h.html", [
      [ "TwoBodyDiffuse", "class_two_body_diffuse.html", "class_two_body_diffuse" ]
    ] ],
    [ "UnboundOrbit.H", "_unbound_orbit_8_h.html", [
      [ "UnboundOrbit", "class_unbound_orbit.html", "class_unbound_orbit" ]
    ] ],
    [ "UserAddMass.H", "_user_add_mass_8_h.html", [
      [ "UserAddMass", "class_user_add_mass.html", "class_user_add_mass" ]
    ] ],
    [ "UserAtmos.H", "_user_atmos_8_h.html", [
      [ "UserAtmos", "class_user_atmos.html", "class_user_atmos" ]
    ] ],
    [ "UserBar.H", "_user_bar_8_h.html", [
      [ "UserBar", "class_user_bar.html", "class_user_bar" ]
    ] ],
    [ "UserBarTrigger.H", "_user_bar_trigger_8_h.html", [
      [ "UserBarTrigger", "class_user_bar_trigger.html", "class_user_bar_trigger" ]
    ] ],
    [ "UserDiffRot.H", "_user_diff_rot_8_h.html", [
      [ "UserDiffRot", "class_user_diff_rot.html", "class_user_diff_rot" ]
    ] ],
    [ "UserDiffuse.H", "_user_diffuse_8_h.html", [
      [ "UserDiffuse", "class_user_diffuse.html", "class_user_diffuse" ]
    ] ],
    [ "UserDisk.H", "_user_disk_8_h.html", [
      [ "UserDisk", "class_user_disk.html", "class_user_disk" ]
    ] ],
    [ "UserDSMC.H", "_user_d_s_m_c_8_h.html", [
      [ "BoundSort", "class_bound_sort.html", "class_bound_sort" ],
      [ "RadiusSort", "class_radius_sort.html", "class_radius_sort" ],
      [ "UserDSMC", "class_user_d_s_m_c.html", "class_user_d_s_m_c" ]
    ] ],
    [ "UserEBar.H", "_user_e_bar_8_h.html", [
      [ "UserEBar", "class_user_e_bar.html", "class_user_e_bar" ]
    ] ],
    [ "UserEBarN.H", "_user_e_bar_n_8_h.html", [
      [ "UserEBarN", "class_user_e_bar_n.html", "class_user_e_bar_n" ]
    ] ],
    [ "UserEBarP.H", "_user_e_bar_p_8_h.html", [
      [ "UserEBarP", "class_user_e_bar_p.html", "class_user_e_bar_p" ]
    ] ],
    [ "UserEBarS.H", "_user_e_bar_s_8_h.html", [
      [ "UserEBarS", "class_user_e_bar_s.html", "class_user_e_bar_s" ]
    ] ],
    [ "UserHalo.H", "_user_halo_8_h.html", [
      [ "UserHalo", "class_user_halo.html", "class_user_halo" ]
    ] ],
    [ "UserLogPot.H", "_user_log_pot_8_h.html", [
      [ "UserLogPot", "class_user_log_pot.html", "class_user_log_pot" ]
    ] ],
    [ "UserMassEvo.H", "_user_mass_evo_8_h.html", [
      [ "UserMassEvo", "class_user_mass_evo.html", "class_user_mass_evo" ]
    ] ],
    [ "UserMNdisk.H", "_user_m_ndisk_8_h.html", [
      [ "UserMNdisk", "class_user_m_ndisk.html", "class_user_m_ndisk" ]
    ] ],
    [ "UserPeriodic.H", "_user_periodic_8_h.html", [
      [ "UserPeriodic", "class_user_periodic.html", "class_user_periodic" ]
    ] ],
    [ "UserProfile.H", "_user_profile_8_h.html", [
      [ "UserProfile", "class_user_profile.html", "class_user_profile" ]
    ] ],
    [ "UserPST.H", "_user_p_s_t_8_h.html", [
      [ "UserPST", "class_user_p_s_t.html", "class_user_p_s_t" ]
    ] ],
    [ "UserReflect.H", "_user_reflect_8_h.html", [
      [ "UserReflect", "class_user_reflect.html", "class_user_reflect" ]
    ] ],
    [ "UserResPot.H", "_user_res_pot_8_h.html", [
      [ "UserResPot", "class_user_res_pot.html", "class_user_res_pot" ]
    ] ],
    [ "UserResPotN.H", "_user_res_pot_n_8_h.html", [
      [ "UserResPotN", "class_user_res_pot_n.html", "class_user_res_pot_n" ]
    ] ],
    [ "UserResPotOrb.H", "_user_res_pot_orb_8_h.html", [
      [ "UserResPotOrb", "class_user_res_pot_orb.html", "class_user_res_pot_orb" ]
    ] ],
    [ "UserRPtest.H", "_user_r_ptest_8_h.html", [
      [ "UserRPtest", "class_user_r_ptest.html", "class_user_r_ptest" ]
    ] ],
    [ "UserSatWake.H", "_user_sat_wake_8_h.html", [
      [ "UserSatWake", "class_user_sat_wake.html", "class_user_sat_wake" ]
    ] ],
    [ "UserShear.H", "_user_shear_8_h.html", [
      [ "UserShear", "class_user_shear.html", "class_user_shear" ]
    ] ],
    [ "UserSlabHalo.H", "_user_slab_halo_8_h.html", [
      [ "UserSlabHalo", "class_user_slab_halo.html", "class_user_slab_halo" ]
    ] ],
    [ "UserSNheat.H", "_user_s_nheat_8_h.html", [
      [ "UserSNheat", "class_user_s_nheat.html", "class_user_s_nheat" ]
    ] ],
    [ "UserTest.H", "_user_test_8_h.html", [
      [ "UserTest", "class_user_test.html", "class_user_test" ]
    ] ],
    [ "UserTidalRad.H", "_user_tidal_rad_8_h.html", [
      [ "UserTidalRad", "class_user_tidal_rad.html", "class_user_tidal_rad" ]
    ] ],
    [ "UserWake.H", "_user_wake_8_h.html", "_user_wake_8_h" ]
];