var class_legendre =
[
    [ "Legendre", "class_legendre.html#a434e3f774067d26051087fe0c49f2620", null ],
    [ "coef1", "class_legendre.html#a0d1d4e87fc98c1863a3aa583f126f402", null ],
    [ "coef2", "class_legendre.html#a269daa87798014d15c424060036918cf", null ],
    [ "coef3", "class_legendre.html#af4ee4c6b608f71e0b294b606de7cf900", null ],
    [ "coef4", "class_legendre.html#a9ba54f9872f17576e0537f87df0ac718", null ],
    [ "f0", "class_legendre.html#a7f8a4f8230d4059d29fc06c106b8fb48", null ],
    [ "f1", "class_legendre.html#a6194ebf2e2c7f9ad707a68260d1d5418", null ],
    [ "h", "class_legendre.html#a3ff47311ca802bdf62979533b23bb947", null ],
    [ "w", "class_legendre.html#a0e7d5d53f303b11e78706f9368a690c8", null ]
];