var class_stats_m_p_i =
[
    [ "Return", "class_stats_m_p_i.html#a1eb9c91baf779feb62dac1daf389388d", null ],
    [ "SpStat", "class_stats_m_p_i.html#a8e180c017fc4704b7afa324d74e87a60", null ],
    [ "StatsMPI", "class_stats_m_p_i.html#ac248462c3fb6c44b47a9978b88169be5", null ],
    [ "add", "class_stats_m_p_i.html#a84be5c2015067b6c9fcbf6b263c161d9", null ],
    [ "clear", "class_stats_m_p_i.html#a1465e96175d7522bec590ffbc656a457", null ],
    [ "share", "class_stats_m_p_i.html#a60c418b8b0a9d9c32986b5069ca23245", null ],
    [ "stats", "class_stats_m_p_i.html#ac58165e1b155cb1ccbdbf8f2fc1d2e8f", null ],
    [ "ccnt", "class_stats_m_p_i.html#aaa1554f6527aa20efea41ed279f85251", null ],
    [ "cntT", "class_stats_m_p_i.html#aeabb97176fa75279b1370c7b977f0b68", null ],
    [ "curT", "class_stats_m_p_i.html#a7177fb05946071fc821a1b7ef699c13e", null ],
    [ "vmax", "class_stats_m_p_i.html#a947e5b4ceb06780a9eed236d97d9d95e", null ],
    [ "vmin", "class_stats_m_p_i.html#a24fd7cca8b971316c9ddd286f26ba6a5", null ],
    [ "vsum", "class_stats_m_p_i.html#abd6d00928a5fd060f644f564dc731d1e", null ]
];