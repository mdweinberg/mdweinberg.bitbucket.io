var class_user_sat_cross_time =
[
    [ "UserSatCrossTime", "class_user_sat_cross_time.html#af535bdc2e60de3a40b56e31e714becfb", null ],
    [ "~UserSatCrossTime", "class_user_sat_cross_time.html#a1bba7f2269ae2077240b1644f4cdb056", null ],
    [ "determine_acceleration_and_potential", "class_user_sat_cross_time.html#afe5f4a532269f75012e9bd5035c2ebbc", null ],
    [ "determine_acceleration_and_potential_thread", "class_user_sat_cross_time.html#aca136a4d218cb987b9932250b3fa03e2", null ],
    [ "initialize", "class_user_sat_cross_time.html#a1ef1b716f76f3b18636425b876b8f45f", null ],
    [ "userinfo", "class_user_sat_cross_time.html#af7cd28e2c8c9781debf564f8f06e6816", null ],
    [ "c0", "class_user_sat_cross_time.html#aeab9788f52a518ac4d69bc54ec39d6db", null ],
    [ "comp_name", "class_user_sat_cross_time.html#ae64acdf4144c2da2f6db0c466f5ae0ad", null ],
    [ "pos", "class_user_sat_cross_time.html#a7fd00ad2ea7c12f092376cf8536d59cb", null ],
    [ "ps2", "class_user_sat_cross_time.html#a7ca0c9c61880f1f936e396c72b0b0275", null ],
    [ "vel", "class_user_sat_cross_time.html#a29c591db26a946418791bbfbb7dba853", null ],
    [ "vl2", "class_user_sat_cross_time.html#a60c16a8f000465e99477faf7be86030c", null ]
];