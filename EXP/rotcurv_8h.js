var rotcurv_8h =
[
    [ "epicyclic_kappa", "rotcurv_8h.html#af915d907d2645c8177ca72add6eaf382", null ],
    [ "omega_circ", "rotcurv_8h.html#a269f47ae9257a21c0cb65849f95856ce", null ],
    [ "Oort_A", "rotcurv_8h.html#a0c9d268d5ed44daa11f4fb509da934d5", null ],
    [ "Oort_B", "rotcurv_8h.html#a3377e87f757fd0a9b2eedfa000b28db0", null ],
    [ "v_circ", "rotcurv_8h.html#aa7e32e3c67df0b9093a055eccd20062a", null ],
    [ "vertical_kappa", "rotcurv_8h.html#a7a47c6bd8133e3b423cca596337e31de", null ]
];