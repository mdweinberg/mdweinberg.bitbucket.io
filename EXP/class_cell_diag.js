var class_cell_diag =
[
    [ "CellDiag", "class_cell_diag.html#acf403c15e064767246bcc829fea9777d", null ],
    [ "bavg", "class_cell_diag.html#a957c08f72e694dcc21648985b7e3fdaf", null ],
    [ "bcel", "class_cell_diag.html#aff53b30d940c091008ac82718cd7148a", null ],
    [ "btot", "class_cell_diag.html#ab4ec6fe9a0c47cb0bd6dc64792801345", null ],
    [ "bvar", "class_cell_diag.html#a436185b96c0648befad7c863505c5f3b", null ],
    [ "maxd", "class_cell_diag.html#af455261f40d597e87f5fb4d77362096d", null ],
    [ "mind", "class_cell_diag.html#ad3a66658a82087eab0b1a82b920951cc", null ],
    [ "navg", "class_cell_diag.html#af9504b738b3a729d80aab4bf40edc08a", null ],
    [ "ntre", "class_cell_diag.html#a586294686160c2befe13fd52cf18eeea", null ],
    [ "nvar", "class_cell_diag.html#a2d0a76e03f0369a4054e4ac08a07c406", null ]
];