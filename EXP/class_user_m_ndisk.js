var class_user_m_ndisk =
[
    [ "UserMNdisk", "class_user_m_ndisk.html#aa4d9412500d874b0cf14193af8789ed2", null ],
    [ "~UserMNdisk", "class_user_m_ndisk.html#a9511ef460f70bf974b41d627f2b0e3a9", null ],
    [ "determine_acceleration_and_potential", "class_user_m_ndisk.html#aa9c8ae0899ca88f62abf4e370a337e54", null ],
    [ "determine_acceleration_and_potential_thread", "class_user_m_ndisk.html#aa07cad8935ad484141f41900bef31eaa", null ],
    [ "initialize", "class_user_m_ndisk.html#aa6c0f735394c00044c7ac62f4396287f", null ],
    [ "userinfo", "class_user_m_ndisk.html#a27dbe34527e153e0cd9fe195f908b20b", null ],
    [ "a", "class_user_m_ndisk.html#a7eb2fb4323db9a19741272226841ae1e", null ],
    [ "b", "class_user_m_ndisk.html#aadf21d1d89a9e694558f9396130917c5", null ],
    [ "c0", "class_user_m_ndisk.html#a602d572668a029bcd9d74b6cf3e5c0f1", null ],
    [ "ctr_name", "class_user_m_ndisk.html#ad7f01580697cf594007641f031784e57", null ],
    [ "DeltaT", "class_user_m_ndisk.html#ac5b96ba94e9ac2224e49755be6e23461", null ],
    [ "mass", "class_user_m_ndisk.html#a307c9f5511b762b3c9a1305b2839d6df", null ],
    [ "name", "class_user_m_ndisk.html#a3916fc83fdcbc93fa387c4e3596eecf8", null ],
    [ "Toff", "class_user_m_ndisk.html#affd510bd3161fc2d283929c77467ae7d", null ],
    [ "Ton", "class_user_m_ndisk.html#a6a668a036d4c647a248f4f918ddb3c51", null ]
];