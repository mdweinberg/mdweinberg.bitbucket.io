var struct_slab_s_l_1_1_slab_s_l_coef_header =
[
    [ "h", "struct_slab_s_l_1_1_slab_s_l_coef_header.html#a4ecff0f085fd829e6bfc9b2a169d70ae", null ],
    [ "jmax", "struct_slab_s_l_1_1_slab_s_l_coef_header.html#aee8294b3bd72f3d483949ca126cccb39", null ],
    [ "nmaxx", "struct_slab_s_l_1_1_slab_s_l_coef_header.html#a77c431f28ea2639f3b452357613b6299", null ],
    [ "nmaxy", "struct_slab_s_l_1_1_slab_s_l_coef_header.html#adf8f003dd53c86e15de7029e387e781e", null ],
    [ "nmaxz", "struct_slab_s_l_1_1_slab_s_l_coef_header.html#a3b37675920d474a0f1cd278755709746", null ],
    [ "time", "struct_slab_s_l_1_1_slab_s_l_coef_header.html#aec297657d667a4b59be1b0265b480dc6", null ],
    [ "type", "struct_slab_s_l_1_1_slab_s_l_coef_header.html#af840d148734f82c031337ccc4d575ac6", null ],
    [ "zmax", "struct_slab_s_l_1_1_slab_s_l_coef_header.html#a0fecba0903bd544a630c5ff8400175c9", null ]
];