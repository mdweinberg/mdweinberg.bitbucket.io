var class_ortho_poly =
[
    [ "coef1", "class_ortho_poly.html#a8691a2e79b7d678a2f33c295a230ce48", null ],
    [ "coef2", "class_ortho_poly.html#a074c75f7b1758471d5ca9e0c9556adc7", null ],
    [ "coef3", "class_ortho_poly.html#a5a1ad9977808f1a4503ccde47b260b57", null ],
    [ "coef4", "class_ortho_poly.html#aa86a42c4a207651a43822242d198725b", null ],
    [ "f", "class_ortho_poly.html#afe8111b9a2946e381e7283a8e06da3b8", null ],
    [ "f0", "class_ortho_poly.html#afaaab0fc6afe43bc01271a549adfc96f", null ],
    [ "f1", "class_ortho_poly.html#a9e16af920759d1d259e492fab6c79100", null ],
    [ "fv", "class_ortho_poly.html#a878a35ed39cf9bfadf7efac4238c91d6", null ],
    [ "get_a", "class_ortho_poly.html#a954a4941a43f355ddb7e17185eca5052", null ],
    [ "get_b", "class_ortho_poly.html#a5e664c78f30952123f31d15582e0bee2", null ],
    [ "h", "class_ortho_poly.html#a012dedf1c757a8ac3c999f2c6c2c6b05", null ],
    [ "w", "class_ortho_poly.html#a789ce34b2842e2ddf958e0af56cdc4e6", null ],
    [ "a", "class_ortho_poly.html#a327992a3cee582b2ddd35100a303461d", null ],
    [ "b", "class_ortho_poly.html#ad31e77ccb4c55b23bf2e0c8312c28119", null ],
    [ "coefs", "class_ortho_poly.html#a2477962944125247f6bb446ba361afaa", null ],
    [ "param", "class_ortho_poly.html#a3e70f7ac20305d4ae84c11b57713bbca", null ]
];