var class_cheb2 =
[
    [ "Cheb2", "class_cheb2.html#af6b0c70c48aa34c3f0498e0a58a6c48e", null ],
    [ "coef1", "class_cheb2.html#a5303f7db171db0ee3b35b0c0a7d2908f", null ],
    [ "coef2", "class_cheb2.html#a2c4c84551fd52fa733f5bc903481c30d", null ],
    [ "coef3", "class_cheb2.html#a2a81555193a44fd55dc0549917640c23", null ],
    [ "coef4", "class_cheb2.html#a6a34ad54975368f6060341cbcf12db04", null ],
    [ "f0", "class_cheb2.html#a96e82bc058b95ba0f9109ddd137d5091", null ],
    [ "f1", "class_cheb2.html#a7cbc8d1e1c0571b7a2e6bdd8d1df7fe2", null ],
    [ "h", "class_cheb2.html#a9ddb27326d84b1e34210b8c1713c7b2c", null ],
    [ "w", "class_cheb2.html#ac6cb9737949aaf8bbad129ee21609778", null ]
];