var class_collide_ion_1_1_recomb_ratio =
[
    [ "RecombRatio", "class_collide_ion_1_1_recomb_ratio.html#a178274e0c97fb531f508ec0612df3ad0", null ],
    [ "operator()", "class_collide_ion_1_1_recomb_ratio.html#a4e1fde50d906cbf781820afef43039ec", null ],
    [ "writeScript", "class_collide_ion_1_1_recomb_ratio.html#a1dfbeae74927c9a089cb551883109fec", null ],
    [ "cdata", "class_collide_ion_1_1_recomb_ratio.html#a956ae1241c6031efca9d774e4c168d73", null ],
    [ "dT", "class_collide_ion_1_1_recomb_ratio.html#a450498df6513d88899f1aad2ba788e62", null ],
    [ "Emax0", "class_collide_ion_1_1_recomb_ratio.html#ae7f5bb776998bd70644d13691cc70e8e", null ],
    [ "Emin0", "class_collide_ion_1_1_recomb_ratio.html#a0bd0112b45d0cbe5a176febbd2833c11", null ],
    [ "norder", "class_collide_ion_1_1_recomb_ratio.html#af2889fc8b4815e5bc70a0e0a3aac75a2", null ],
    [ "numE0", "class_collide_ion_1_1_recomb_ratio.html#a6abe7ff8a630987de54d560d6e2bf3f8", null ],
    [ "numT", "class_collide_ion_1_1_recomb_ratio.html#ad36848a9d3d9f4039635ae8e564db019", null ],
    [ "Tmn", "class_collide_ion_1_1_recomb_ratio.html#afdf169beb4419e68525c14058e546c91", null ],
    [ "Tmx", "class_collide_ion_1_1_recomb_ratio.html#a150adc834c7341d88ab7349897b53aff", null ],
    [ "Z", "class_collide_ion_1_1_recomb_ratio.html#af6c3e42aa56982f67a5ff15b0e39d75b", null ]
];