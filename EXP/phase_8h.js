var phase_8h =
[
    [ "Ensemble", "class_ensemble.html", "class_ensemble" ],
    [ "Frame_Rotation", "class_frame___rotation.html", "class_frame___rotation" ],
    [ "Phase", "class_phase.html", "class_phase" ],
    [ "force_func_ptr", "phase_8h.html#abeae78b397d089e7c430108ae7fda699", null ],
    [ "freezing_function", "phase_8h.html#a836f1515043416409e8e3c93004e4eee", null ],
    [ "pot_func_ptr", "phase_8h.html#a9993235f9900a9f9bc443241b18bc84c", null ],
    [ "epicyclic_kappa", "phase_8h.html#af915d907d2645c8177ca72add6eaf382", null ],
    [ "forward_interp", "phase_8h.html#a51cf9e969f481a99b12a195cb87c1201", null ],
    [ "inverse_interp_zero", "phase_8h.html#a456f1226446218a175a6762c82951359", null ],
    [ "Newton", "phase_8h.html#a9d0000be75b2f1f1faf9df0a59c91f95", null ],
    [ "Oort_A", "phase_8h.html#a0c9d268d5ed44daa11f4fb509da934d5", null ],
    [ "Oort_B", "phase_8h.html#a3377e87f757fd0a9b2eedfa000b28db0", null ],
    [ "v_circ", "phase_8h.html#aa7e32e3c67df0b9093a055eccd20062a", null ],
    [ "vertical_kappa", "phase_8h.html#a7a47c6bd8133e3b423cca596337e31de", null ],
    [ "z_Rotation", "phase_8h.html#a7e02f7f2a5f8cafafc8e8b6e099f6a8c", null ]
];