var coef__pa_8py =
[
    [ "cnt", "coef__pa_8py.html#a3fbd0b63e654309f716a47c318295380", null ],
    [ "curtm", "coef__pa_8py.html#a9e63f0448fb0b30049f279a5ebc18480", null ],
    [ "data", "coef__pa_8py.html#a0f053585bb2bdbd94f61681b07e941ca", null ],
    [ "dif", "coef__pa_8py.html#ac371ce36863b71b82dab081671ba3756", null ],
    [ "file", "coef__pa_8py.html#ae91f45ac5e19744181c4e68e5031b87f", null ],
    [ "hi", "coef__pa_8py.html#a9f14730b8fef0a886ef205d0730ea0e2", null ],
    [ "label", "coef__pa_8py.html#a87f3dc202845f17cdf5d1850282f72bb", null ],
    [ "lo", "coef__pa_8py.html#a51e7e169ddfb186ff41d56d66e79c5d4", null ],
    [ "m", "coef__pa_8py.html#acf771f75b8e9b23c35d01e4358390f16", null ],
    [ "size", "coef__pa_8py.html#aff09bb46ab1331bc4966bdef827d881c", null ],
    [ "speed", "coef__pa_8py.html#a8155b9d9066e496015a4f75f9adb094e", null ],
    [ "sum", "coef__pa_8py.html#a4f14d649e49d66e7f53f72de2f195f22", null ],
    [ "sums", "coef__pa_8py.html#a670c156b8f12e08adbbfe5169de01ed9", null ],
    [ "t", "coef__pa_8py.html#aae7dafa95dc399ee18808c40b95f92cf", null ],
    [ "time", "coef__pa_8py.html#a3c3e6b0dcfac7496f436f419ffedac12", null ],
    [ "title", "coef__pa_8py.html#a18d054ba58021846c8df005170240794", null ],
    [ "toks", "coef__pa_8py.html#a665598f05f3b17ba65fc52979e181a58", null ],
    [ "v", "coef__pa_8py.html#a285597955330725b9dd2f171ca4b98c6", null ]
];