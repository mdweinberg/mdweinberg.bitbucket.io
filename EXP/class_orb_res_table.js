var class_orb_res_table =
[
    [ "OrbResTable", "class_orb_res_table.html#a1ac1424fdf41cb975d099e2c9c95ab4a", null ],
    [ "~OrbResTable", "class_orb_res_table.html#a22171d08675fcd20b2728b9f8dab84f9", null ],
    [ "setup", "class_orb_res_table.html#ae061d5c3c6dc8f605084e3b680e387c3", null ],
    [ "setup_E_array", "class_orb_res_table.html#a2c7b6ecd11d9d8911595e7e820d3a3ce", null ],
    [ "derivative_step", "class_orb_res_table.html#a903ca0aa5a47c0dfa7e3de5290b846ef", null ],
    [ "dfqE", "class_orb_res_table.html#ac558c36f85d98711317a8706ba75f228", null ],
    [ "dfqL", "class_orb_res_table.html#ae8fba7a53fd88dd5815dfdee0b243f63", null ],
    [ "EInt", "class_orb_res_table.html#a6b9ad8f3a1a76ce5eafda22638dee240", null ],
    [ "EJac", "class_orb_res_table.html#af68f356631c0632f1790d87906f7fae3", null ],
    [ "ELoc", "class_orb_res_table.html#aaff9efdd7f3ec21eb5f73880596a6251", null ],
    [ "ERes", "class_orb_res_table.html#affdbacc075d2be12bbae07903d7dc538", null ],
    [ "norm", "class_orb_res_table.html#a9ef0c53c07e8f35f5781e996770f1692", null ],
    [ "number", "class_orb_res_table.html#afcd0ed0158449ad84b5bf20097fa01c1", null ],
    [ "orbits", "class_orb_res_table.html#a3a49ea5f829e34b25c73b94a561299ee", null ],
    [ "p", "class_orb_res_table.html#a1bd55636f87a74c48be0413d664c1917", null ],
    [ "PotTrans", "class_orb_res_table.html#a8364fbac13fbd82424c8c68bea466212", null ],
    [ "PotTrans2", "class_orb_res_table.html#aa48472e0fbd77c491b070ea606d1d417", null ],
    [ "timer", "class_orb_res_table.html#a67b022f04490ef8c4208e3ba93f4f3d0", null ],
    [ "torbit", "class_orb_res_table.html#a757362aaf0e1148c433a254f0130a6f7", null ]
];