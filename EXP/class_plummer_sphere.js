var class_plummer_sphere =
[
    [ "PlummerSphere", "class_plummer_sphere.html#a86bcc229834769dcc528c4e8a104ab41", null ],
    [ "d2fde2", "class_plummer_sphere.html#ab95198d90d07b124d3e42689bc861f43", null ],
    [ "dfde", "class_plummer_sphere.html#a5cce356c16602d5a7565829746eb4b3d", null ],
    [ "dfdl", "class_plummer_sphere.html#a803e60961f740a9cf5acf605de13ef98", null ],
    [ "distf", "class_plummer_sphere.html#a046542e5c6831717f9b5bcd1cb850906", null ],
    [ "get_density", "class_plummer_sphere.html#a1adbb81a75a56d8775991c72a06c7b8e", null ],
    [ "get_dpot", "class_plummer_sphere.html#a2284b215f5c25868c28e6dee159d9791", null ],
    [ "get_dpot2", "class_plummer_sphere.html#a603d8d644951e5868f76a0e4cdac8faf", null ],
    [ "get_mass", "class_plummer_sphere.html#ad7aede352bfadd171d221cf16062f16a", null ],
    [ "get_max_radius", "class_plummer_sphere.html#aab24fa7d3d850ff0916bf8bcc5723dd1", null ],
    [ "get_min_radius", "class_plummer_sphere.html#ade00f87203a81b6d3299638491ad470c", null ],
    [ "get_pot", "class_plummer_sphere.html#ab521401ab6440b28433f149867563de8", null ],
    [ "get_pot_dpot", "class_plummer_sphere.html#afb71240a34ad55b8d419a36ab5a881b8", null ],
    [ "dfac", "class_plummer_sphere.html#a9b12f5b54d3eb5cb0b043de2f9e50c59", null ],
    [ "ffac", "class_plummer_sphere.html#aeb86e917c5befaf750bfe1456abc5a06", null ],
    [ "mass", "class_plummer_sphere.html#a66c8824f973b22749ab2aee621206564", null ],
    [ "rmax", "class_plummer_sphere.html#ac439a9772d9462d5165aade1d5dc2876", null ],
    [ "rmin", "class_plummer_sphere.html#a2ed8bffa65a69ebf759c447bf2e1a890", null ],
    [ "rscl", "class_plummer_sphere.html#a05a2671858379c86776a2fc510b650bc", null ]
];