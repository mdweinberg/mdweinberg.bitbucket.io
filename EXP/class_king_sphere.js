var class_king_sphere =
[
    [ "KingSphere", "class_king_sphere.html#acfa2eff104aa3b78e3e41be4232f39f0", null ],
    [ "d2fde2", "class_king_sphere.html#ad89ab099c359f76e7e65b9202c714aa2", null ],
    [ "dfde", "class_king_sphere.html#af1471186410f1902c5840ba4035346a2", null ],
    [ "distf", "class_king_sphere.html#ae981250fa1763ce5fb8ca72651f509fd", null ],
    [ "setup_df", "class_king_sphere.html#a0b59261568d6888256e1cc8a18be578b", null ],
    [ "betaK", "class_king_sphere.html#a82b00f644f8a3b375b783cfd97c3f289", null ],
    [ "E0K", "class_king_sphere.html#a1f96351f8f8d3f3afdd464d27b4ecd43", null ],
    [ "kingK", "class_king_sphere.html#ae065ee37ee4cebb8001fe59cdc8aa0d2", null ]
];