var class_gauss_quad =
[
    [ "GaussQuad", "class_gauss_quad.html#a2d164fe57b165dcd45a3f9ad4b102903", null ],
    [ "bomb", "class_gauss_quad.html#aec3155c26b5792115c894a8288185fa5", null ],
    [ "get_alpha", "class_gauss_quad.html#ad0ff3faadab8afbd0ff35111a02938dc", null ],
    [ "get_beta", "class_gauss_quad.html#a647dda30c76a016d266d6ccc83ae79a7", null ],
    [ "get_n", "class_gauss_quad.html#a4522920cf5303e3d0040b626b6ada8a6", null ],
    [ "knot", "class_gauss_quad.html#a30ed9e0400fddda528ad20b71fb040aa", null ],
    [ "kV", "class_gauss_quad.html#a3e4612306c93a8c4a02976c66ca2e56d", null ],
    [ "weight", "class_gauss_quad.html#a5cac0f24ea4719573bf3913c7bef6490", null ],
    [ "wV", "class_gauss_quad.html#a0e4ae07dabd33d03706c3be657dfded2", null ],
    [ "alpha", "class_gauss_quad.html#a9c69bf4fa86abd41689d3dcffa4a5851", null ],
    [ "beta", "class_gauss_quad.html#a5e3f4ea3271cac850f964c12fd6d6eee", null ],
    [ "FunctionID", "class_gauss_quad.html#a11bf2d3ddf9e63ca3dcfa6376413d016", null ],
    [ "n", "class_gauss_quad.html#af6bcda50fcfae1b73a9e1e2dfa3c83cf", null ],
    [ "r", "class_gauss_quad.html#afa3891d83c115dac7123fcefb4b9a456", null ],
    [ "w", "class_gauss_quad.html#aaf3b7dd45ef878ea5427b0e7aaa5a6a7", null ]
];