var class_hernquist_sphere =
[
    [ "HernquistSphere", "class_hernquist_sphere.html#a984f3ad39bf053316522666c3318af89", null ],
    [ "d2fde2", "class_hernquist_sphere.html#afab9f5efb35adb4e61e5b3758e90a2ac", null ],
    [ "dfde", "class_hernquist_sphere.html#aa0d27f1e5dc455ddcf49e10202a853f1", null ],
    [ "dfdl", "class_hernquist_sphere.html#a6005707847dfe35ca2b6e423a7f40c97", null ],
    [ "distf", "class_hernquist_sphere.html#a8b0132401b94171ffaf309792ac7b989", null ],
    [ "get_density", "class_hernquist_sphere.html#a85bbadfba50fe5f4e81b00e5eb68b5fe", null ],
    [ "get_dpot", "class_hernquist_sphere.html#a926ec2665ba371cb35b5f43df81c1d90", null ],
    [ "get_dpot2", "class_hernquist_sphere.html#abb190a388e0df34ac058c739c8dde702", null ],
    [ "get_mass", "class_hernquist_sphere.html#ad71a1eef352f07e5da69e0ad33d3869a", null ],
    [ "get_max_radius", "class_hernquist_sphere.html#a97a291bb7c67e70f74abc79ae841ead8", null ],
    [ "get_min_radius", "class_hernquist_sphere.html#ae6d8d9df1927efa05224264452ca0f84", null ],
    [ "get_pot", "class_hernquist_sphere.html#a6aed89baa36f399e4ae381857032ee02", null ],
    [ "get_pot_dpot", "class_hernquist_sphere.html#a57fe33d175c333d8f7df0b9689d25234", null ],
    [ "rmax", "class_hernquist_sphere.html#a369aaa81a491be8984c2f41ca5a89ec8", null ],
    [ "rmin", "class_hernquist_sphere.html#a0687c820fdfe76c1490906ebfff13255", null ],
    [ "rot", "class_hernquist_sphere.html#aa094a060562a5b421cc451a674ff0296", null ],
    [ "rscl", "class_hernquist_sphere.html#a85e8ade11ce479674207e85e6af19b68", null ],
    [ "tolE", "class_hernquist_sphere.html#ad7dc5d0d7044fc41178167e23f803c67", null ]
];