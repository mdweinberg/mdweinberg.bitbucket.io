var class_user_halo =
[
    [ "UserHalo", "class_user_halo.html#a133d375decea3f58c485eee510540d2e", null ],
    [ "~UserHalo", "class_user_halo.html#a8b4d02b60b3ee3ebae82fd3906464e39", null ],
    [ "determine_acceleration_and_potential", "class_user_halo.html#a00cd6356aaf074d5d95833ef38c0b009", null ],
    [ "determine_acceleration_and_potential_thread", "class_user_halo.html#a58629b539d252522d5223597cc6e3b02", null ],
    [ "initialize", "class_user_halo.html#aeca44db704f6e0da1632ed20dabca7c0", null ],
    [ "userinfo", "class_user_halo.html#aa9aab4269f2589b8802e87e099fd18df", null ],
    [ "c0", "class_user_halo.html#a2c13446e9f8f8580d7dd34b9c2ae6b2e", null ],
    [ "comp_name", "class_user_halo.html#a2fb7f2cdd0c12bc712ebf3562eefa58e", null ],
    [ "diverge", "class_user_halo.html#a15a84545f1a27eb5d5d6cdfe81478378", null ],
    [ "diverge_rfac", "class_user_halo.html#a9d4c5308412662f1da49b8988552fbc2", null ],
    [ "model", "class_user_halo.html#ab608b1b0fee5ca1f04902e1cba69807a", null ],
    [ "model_file", "class_user_halo.html#a44129789938172e30a0eef33a01a1bac", null ],
    [ "q1", "class_user_halo.html#a06e03b5050461dfa50a1eaa42c6eddeb", null ],
    [ "q2", "class_user_halo.html#aeec803370a5e53de2ef410672b1864b6", null ],
    [ "q3", "class_user_halo.html#aba03f022faf848414746e1f733d0b060", null ]
];