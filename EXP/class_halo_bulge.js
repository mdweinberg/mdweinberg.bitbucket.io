var class_halo_bulge =
[
    [ "HaloBulge", "class_halo_bulge.html#adec71270f0f642021a0239121b175b8d", null ],
    [ "determine_acceleration_and_potential_thread", "class_halo_bulge.html#add6447de5d35a660300bb87749fa29b6", null ],
    [ "initialize", "class_halo_bulge.html#a72cf46c868ed8686458d7f8e05e99909", null ],
    [ "bmodel", "class_halo_bulge.html#ac7864f66118ed8fbad680b2a532f8c23", null ],
    [ "HMODEL", "class_halo_bulge.html#ada8942616fa582437aa460854278d5c6", null ],
    [ "INFILE", "class_halo_bulge.html#a3cbd78f4e2afef8acd0964d53ba38061", null ],
    [ "MBULGE", "class_halo_bulge.html#ab4c6d7045f053bda8cd90f7cc410ee35", null ],
    [ "MHALO", "class_halo_bulge.html#a72f9de577e906e8a75c0c62b8eeab794", null ],
    [ "model", "class_halo_bulge.html#a6c05e0daeaa27b3bc4021c256047d90f", null ],
    [ "RBCORE", "class_halo_bulge.html#a92778988c013f3ec28a86c454de3e7ad", null ],
    [ "RBMOD", "class_halo_bulge.html#a320ce7e685727d08b366532f5f8bc986", null ],
    [ "RBMODMIN", "class_halo_bulge.html#a27d88fd0efa04717f6d10acc06108cfa", null ],
    [ "RBULGE", "class_halo_bulge.html#a67fd8c4790fcd95502c75d77f503e976", null ],
    [ "RHALO", "class_halo_bulge.html#a3a716b8c8b3e9323bfb8ac4627d626d8", null ],
    [ "RMOD", "class_halo_bulge.html#acb608daa85cb5b53ffdab983b828b447", null ],
    [ "RMODMIN", "class_halo_bulge.html#a15ff4f713e16686b596b68c8785ac33a", null ]
];