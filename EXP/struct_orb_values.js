var struct_orb_values =
[
    [ "apo", "struct_orb_values.html#ab54a7920bc325de17007e2eb8743c53a", null ],
    [ "azimuthal_period", "struct_orb_values.html#af91ae259ab357194d34e68884aa1ec54", null ],
    [ "Boltzmann", "struct_orb_values.html#a94eeab4bc54de25013797eb55214d498", null ],
    [ "energy", "struct_orb_values.html#a656442b8b1ffc145c6cd36925911c813", null ],
    [ "kappa", "struct_orb_values.html#ad35fdefb2c797f913a0d12119a66ac5d", null ],
    [ "peri", "struct_orb_values.html#ab13a415423308e84457de91669ed6a2f", null ],
    [ "radial_period", "struct_orb_values.html#a97ef77f764e2a4be3c426fef21d2cd12", null ],
    [ "rate", "struct_orb_values.html#a38c62fb95b1e08ac4ecd5269dd671a96", null ],
    [ "t0", "struct_orb_values.html#ae9fed392d134914a5a7626c436c25e60", null ],
    [ "tf", "struct_orb_values.html#a4c88a123f34543727d4f4337210df631", null ],
    [ "value", "struct_orb_values.html#a56cf8a9907c37e247b75a04aec472f65", null ]
];