var class_add_disk =
[
    [ "AddDisk", "class_add_disk.html#ab62ed0e659c19c837b979ccda3b3a130", null ],
    [ "~AddDisk", "class_add_disk.html#ae5288cf32ccffde9fd9cd98ae0883f19", null ],
    [ "get_model", "class_add_disk.html#a9065e7ec2e6f81758bf5e9dd3cbd441b", null ],
    [ "d", "class_add_disk.html#a813e68624e27d3e0ec16ba94ded4a90d", null ],
    [ "dmass", "class_add_disk.html#a8d0125d455ac9132bc4e11222e4c2bc0", null ],
    [ "logarithmic", "class_add_disk.html#adbd7b21e141c6b0663f4b3051c84de7a", null ],
    [ "m", "class_add_disk.html#a53f7d39d403a859efa3554cc7eb312ee", null ],
    [ "mod", "class_add_disk.html#ad942368c20a1259fb3a705afc115f9e4", null ],
    [ "number", "class_add_disk.html#a9fa8b981e42fbe83c0f92a6f41b42e26", null ],
    [ "p", "class_add_disk.html#acc3378fde8c2dafcc018ac80eb2f465a", null ],
    [ "r", "class_add_disk.html#ae7cdda6408a64b6faeec75366da358d2", null ],
    [ "Rmin", "class_add_disk.html#a99facebc0b9382ac0e0341d9fd696844", null ],
    [ "use_mpi", "class_add_disk.html#abd2041a9deffc1d1d2971adb5e88fec0", null ]
];