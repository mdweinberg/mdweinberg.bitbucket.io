var class_user_slab_halo =
[
    [ "UserSlabHalo", "class_user_slab_halo.html#a56cf3a3af9c6b28517448f8a2056071b", null ],
    [ "~UserSlabHalo", "class_user_slab_halo.html#a665a3d5b85d6b40119761e5ff66c9d1d", null ],
    [ "determine_acceleration_and_potential", "class_user_slab_halo.html#a1abed0214d19471a3297fbde08f87939", null ],
    [ "determine_acceleration_and_potential_thread", "class_user_slab_halo.html#a2313e09fced931664dd12dd9bda6a539", null ],
    [ "initialize", "class_user_slab_halo.html#a9277b00ae118f8955e6a34104c8e67f3", null ],
    [ "userinfo", "class_user_slab_halo.html#a91c3672ca5b7b0e6dae483f41f3d9ea8", null ],
    [ "c0", "class_user_slab_halo.html#a43a495b118fda314c681c0c1c102630d", null ],
    [ "ctr_name", "class_user_slab_halo.html#a0929093f9a670f9666e2c2b13a7a9d43", null ],
    [ "h0", "class_user_slab_halo.html#aeede07ab9103b0ad429c99862d716fb1", null ],
    [ "rho0", "class_user_slab_halo.html#afea238b48586ca7bc6e42e5e18570acf", null ],
    [ "sig20", "class_user_slab_halo.html#ab1525673c28c12a9317d6c6181f3e0de", null ],
    [ "U0", "class_user_slab_halo.html#a5be85b230a3415793b44a987720e573d", null ],
    [ "v0", "class_user_slab_halo.html#ab4c4ea76a0cd039fedba57c002776c98", null ],
    [ "v20", "class_user_slab_halo.html#a826e2b9aa650cf88c7ec2dc676e4b0d5", null ],
    [ "z0", "class_user_slab_halo.html#a4b81a05356ba179a88dcf04c41982557", null ]
];