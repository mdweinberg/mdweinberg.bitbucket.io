var namespace_n_t_c =
[
    [ "Interact", "class_n_t_c_1_1_interact.html", "class_n_t_c_1_1_interact" ],
    [ "maxDouble", "class_n_t_c_1_1max_double.html", "class_n_t_c_1_1max_double" ],
    [ "NTCdb", "class_n_t_c_1_1_n_t_cdb.html", "class_n_t_c_1_1_n_t_cdb" ],
    [ "NTCitem", "class_n_t_c_1_1_n_t_citem.html", "class_n_t_c_1_1_n_t_citem" ],
    [ "Quantile", "class_n_t_c_1_1_quantile.html", "class_n_t_c_1_1_quantile" ],
    [ "QuantileBag", "class_n_t_c_1_1_quantile_bag.html", "class_n_t_c_1_1_quantile_bag" ]
];