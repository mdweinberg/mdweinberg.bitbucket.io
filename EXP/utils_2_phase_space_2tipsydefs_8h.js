var utils_2_phase_space_2tipsydefs_8h =
[
    [ "dark_particle", "structdark__particle.html", "structdark__particle" ],
    [ "dump", "structdump.html", "structdump" ],
    [ "gas_particle", "structgas__particle.html", "structgas__particle" ],
    [ "star_particle", "structstar__particle.html", "structstar__particle" ],
    [ "MAXDIM", "utils_2_phase_space_2tipsydefs_8h.html#a674c1a85fb1c09ec56b0b8f6319e7b97", null ],
    [ "Real", "utils_2_phase_space_2tipsydefs_8h.html#ab685845059e8a2c92743427d9a698c70", null ],
    [ "xdr_dark", "utils_2_phase_space_2tipsydefs_8h.html#a18bac09320b8f4587a454d45c16c4ac6", null ],
    [ "xdr_gas", "utils_2_phase_space_2tipsydefs_8h.html#a182ca7f6caa29af42d040cc2983cd730", null ],
    [ "xdr_init", "utils_2_phase_space_2tipsydefs_8h.html#ad5c01cb2c6a74faae19cf4a55e01b4cd", null ],
    [ "xdr_quit", "utils_2_phase_space_2tipsydefs_8h.html#a54920b3241ba7c64ec8762e4d992fe10", null ],
    [ "xdr_star", "utils_2_phase_space_2tipsydefs_8h.html#a3ca824c3364115b20f5df3d0dd7ef086", null ],
    [ "dark_particles", "utils_2_phase_space_2tipsydefs_8h.html#a36fece5e2f4214b23ff9ec3f639e4d11", null ],
    [ "gas_particles", "utils_2_phase_space_2tipsydefs_8h.html#ad3e6da03befb06eb46e4445d729e974d", null ],
    [ "header", "utils_2_phase_space_2tipsydefs_8h.html#a6349b18097d9888f9071e823186391e9", null ],
    [ "star_particles", "utils_2_phase_space_2tipsydefs_8h.html#a05dce9e888e23bbabf50fe010f5e3c9e", null ]
];