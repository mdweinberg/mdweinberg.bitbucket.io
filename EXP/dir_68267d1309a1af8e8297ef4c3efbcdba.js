var dir_68267d1309a1af8e8297ef4c3efbcdba =
[
    [ "DSMC", "dir_2ee2cc264e21d8f7dd3d8c50544928e7.html", "dir_2ee2cc264e21d8f7dd3d8c50544928e7" ],
    [ "user", "dir_8b0164eb0fb74115683f9812cb2f78f0.html", "dir_8b0164eb0fb74115683f9812cb2f78f0" ],
    [ "AxisymmetricBasis.H", "_axisymmetric_basis_8_h.html", [
      [ "AxisymmetricBasis", "class_axisymmetric_basis.html", "class_axisymmetric_basis" ]
    ] ],
    [ "Basis.H", "_basis_8_h.html", [
      [ "Basis", "class_basis.html", "class_basis" ]
    ] ],
    [ "Bessel.H", "_bessel_8_h.html", "_bessel_8_h" ],
    [ "BN.H", "_b_n_8_h.html", [
      [ "coulombSelect", "classcoulomb_select.html", "classcoulomb_select" ]
    ] ],
    [ "CBrock.H", "_c_brock_8_h.html", [
      [ "CBrock", "class_c_brock.html", "class_c_brock" ]
    ] ],
    [ "CBrockDisk.H", "_c_brock_disk_8_h.html", [
      [ "CBrockDisk", "class_c_brock_disk.html", "class_c_brock_disk" ]
    ] ],
    [ "chkTimer.H", "chk_timer_8_h.html", [
      [ "CheckpointTimer", "class_checkpoint_timer.html", "class_checkpoint_timer" ],
      [ "Time", "class_time.html", "class_time" ]
    ] ],
    [ "coef.H", "coef_8_h.html", [
      [ "CylCoefHeader", "struct_cyl_coef_header.html", "struct_cyl_coef_header" ],
      [ "SphCoefHeader", "struct_sph_coef_header.html", "struct_sph_coef_header" ]
    ] ],
    [ "Component.H", "_component_8_h.html", [
      [ "Component", "class_component.html", "class_component" ],
      [ "loadb_datum", "structloadb__datum.html", "structloadb__datum" ]
    ] ],
    [ "ComponentContainer.H", "_component_container_8_h.html", [
      [ "ComponentContainer", "class_component_container.html", "class_component_container" ],
      [ "Interaction", "class_interaction.html", "class_interaction" ]
    ] ],
    [ "Cspline.H", "_cspline_8_h.html", [
      [ "Cspline", "class_cspline.html", "class_cspline" ],
      [ "Element", "class_cspline_1_1_element.html", "class_cspline_1_1_element" ]
    ] ],
    [ "Cube.H", "_cube_8_h.html", [
      [ "Cube", "class_cube.html", "class_cube" ]
    ] ],
    [ "Cylinder.H", "_cylinder_8_h.html", [
      [ "Cylinder", "class_cylinder.html", "class_cylinder" ]
    ] ],
    [ "CylindricalCoefs.H", "_cylindrical_coefs_8_h.html", [
      [ "Coefs", "struct_cylindrical_coefs_1_1_coefs.html", "struct_cylindrical_coefs_1_1_coefs" ],
      [ "CylindricalCoefs", "class_cylindrical_coefs.html", "class_cylindrical_coefs" ]
    ] ],
    [ "Direct.H", "_direct_8_h.html", [
      [ "Direct", "class_direct.html", "class_direct" ]
    ] ],
    [ "EJcom.H", "_e_jcom_8_h.html", [
      [ "EJcom", "class_e_jcom.html", "class_e_jcom" ]
    ] ],
    [ "EmpCylSL.h", "_emp_cyl_s_l_8h.html", "_emp_cyl_s_l_8h" ],
    [ "EnumBitset.H", "_enum_bitset_8_h.html", [
      [ "EnumBitset", "class_enum_bitset.html", "class_enum_bitset" ],
      [ "EnumTraits", "struct_enum_traits.html", null ]
    ] ],
    [ "exp_thread.h", "src_2exp__thread_8h.html", "src_2exp__thread_8h" ],
    [ "expand.h", "expand_8h.html", "expand_8h" ],
    [ "ExternalCollection.H", "_external_collection_8_h.html", "_external_collection_8_h" ],
    [ "ExternalForce.H", "_external_force_8_h.html", "_external_force_8_h" ],
    [ "externalShock.H", "external_shock_8_h.html", [
      [ "externalShock", "classexternal_shock.html", "classexternal_shock" ]
    ] ],
    [ "generateRelaxation.H", "generate_relaxation_8_h.html", [
      [ "generateRelaxation", "classgenerate_relaxation.html", "classgenerate_relaxation" ]
    ] ],
    [ "global.H", "global_8_h.html", "global_8_h" ],
    [ "HaloBulge.H", "_halo_bulge_8_h.html", [
      [ "HaloBulge", "class_halo_bulge.html", "class_halo_bulge" ]
    ] ],
    [ "header.H", "src_2header_8_h.html", [
      [ "ComponentHeader", "class_component_header.html", "class_component_header" ],
      [ "MasterHeader", "class_master_header.html", "class_master_header" ]
    ] ],
    [ "Hernquist.H", "src_2hernquist_8h.html", [
      [ "Hernquist", "class_hernquist.html", "class_hernquist" ]
    ] ],
    [ "KarzasLatter.H", "_karzas_latter_8_h.html", [
      [ "KLGFdata", "class_k_l_g_fdata.html", "class_k_l_g_fdata" ]
    ] ],
    [ "MixtureBasis.H", "_mixture_basis_8_h.html", "_mixture_basis_8_h" ],
    [ "NoForce.H", "_no_force_8_h.html", [
      [ "NoForce", "class_no_force.html", "class_no_force" ]
    ] ],
    [ "NVTX.H", "_n_v_t_x_8_h.html", "_n_v_t_x_8_h" ],
    [ "OrbTrace.H", "_orb_trace_8_h.html", [
      [ "OrbTrace", "class_orb_trace.html", "class_orb_trace" ]
    ] ],
    [ "Orient.H", "_orient_8_h.html", [
      [ "EL3", "class_e_l3.html", "class_e_l3" ],
      [ "ltEL3", "structlt_e_l3.html", "structlt_e_l3" ],
      [ "Orient", "class_orient.html", "class_orient" ]
    ] ],
    [ "OutAscii.H", "_out_ascii_8_h.html", [
      [ "OutAscii", "class_out_ascii.html", "class_out_ascii" ]
    ] ],
    [ "OutCalbr.H", "_out_calbr_8_h.html", [
      [ "OutCalbr", "class_out_calbr.html", "class_out_calbr" ]
    ] ],
    [ "OutCHKPT.H", "_out_c_h_k_p_t_8_h.html", [
      [ "OutCHKPT", "class_out_c_h_k_p_t.html", "class_out_c_h_k_p_t" ]
    ] ],
    [ "OutCHKPTQ.H", "_out_c_h_k_p_t_q_8_h.html", [
      [ "OutCHKPTQ", "class_out_c_h_k_p_t_q.html", "class_out_c_h_k_p_t_q" ]
    ] ],
    [ "OutCoef.H", "_out_coef_8_h.html", [
      [ "OutCoef", "class_out_coef.html", "class_out_coef" ]
    ] ],
    [ "OutDiag.H", "_out_diag_8_h.html", [
      [ "OutDiag", "class_out_diag.html", "class_out_diag" ]
    ] ],
    [ "OutFrac.H", "_out_frac_8_h.html", [
      [ "OutFrac", "class_out_frac.html", "class_out_frac" ]
    ] ],
    [ "OutLog.H", "_out_log_8_h.html", [
      [ "OutLog", "class_out_log.html", "class_out_log" ]
    ] ],
    [ "OutMulti.H", "_out_multi_8_h.html", [
      [ "OutMulti", "class_out_multi.html", "class_out_multi" ]
    ] ],
    [ "OutPS.H", "_out_p_s_8_h.html", [
      [ "OutPS", "class_out_p_s.html", "class_out_p_s" ]
    ] ],
    [ "OutPSN.H", "_out_p_s_n_8_h.html", [
      [ "OutPSN", "class_out_p_s_n.html", "class_out_p_s_n" ]
    ] ],
    [ "OutPSP.H", "_out_p_s_p_8_h.html", [
      [ "OutPSP", "class_out_p_s_p.html", "class_out_p_s_p" ]
    ] ],
    [ "OutPSQ.H", "_out_p_s_q_8_h.html", [
      [ "OutPSQ", "class_out_p_s_q.html", "class_out_p_s_q" ]
    ] ],
    [ "Output.H", "_output_8_h.html", [
      [ "Output", "class_output.html", "class_output" ]
    ] ],
    [ "OutputContainer.H", "_output_container_8_h.html", [
      [ "OutputContainer", "class_output_container.html", "class_output_container" ]
    ] ],
    [ "OutRelaxation.H", "_out_relaxation_8_h.html", [
      [ "OutRelaxation", "class_out_relaxation.html", "class_out_relaxation" ]
    ] ],
    [ "Particle.H", "src_2_particle_8_h.html", "src_2_particle_8_h" ],
    [ "ParticleFerry.H", "_particle_ferry_8_h.html", "_particle_ferry_8_h" ],
    [ "pCell.H", "p_cell_8_h.html", "p_cell_8_h" ],
    [ "pHOT.H", "p_h_o_t_8_h.html", [
      [ "CellDiag", "class_cell_diag.html", "class_cell_diag" ],
      [ "pHOT", "classp_h_o_t.html", "classp_h_o_t" ],
      [ "pHOT_iterator", "classp_h_o_t__iterator.html", "classp_h_o_t__iterator" ]
    ] ],
    [ "pHOT_types.H", "p_h_o_t__types_8_h.html", "p_h_o_t__types_8_h" ],
    [ "PotAccel.H", "_pot_accel_8_h.html", "_pot_accel_8_h" ],
    [ "ScatterMFP.H", "_scatter_m_f_p_8_h.html", "_scatter_m_f_p_8_h" ],
    [ "Shells.H", "_shells_8_h.html", [
      [ "Shells", "class_shells.html", "class_shells" ]
    ] ],
    [ "Slab.H", "_slab_8_h.html", [
      [ "Slab", "class_slab.html", "class_slab" ],
      [ "SlabCoefHeader", "struct_slab_1_1_slab_coef_header.html", "struct_slab_1_1_slab_coef_header" ]
    ] ],
    [ "SlabSL.H", "_slab_s_l_8_h.html", [
      [ "SlabSL", "class_slab_s_l.html", "class_slab_s_l" ],
      [ "SlabSLCoefHeader", "struct_slab_s_l_1_1_slab_s_l_coef_header.html", "struct_slab_s_l_1_1_slab_s_l_coef_header" ]
    ] ],
    [ "sltableMP2.h", "src_2sltable_m_p2_8h.html", [
      [ "TableCyl", "class_table_cyl.html", "class_table_cyl" ],
      [ "TableSlab", "class_table_slab.html", "class_table_slab" ],
      [ "TableSph", "class_table_sph.html", "class_table_sph" ]
    ] ],
    [ "Species.H", "_species_8_h.html", "_species_8_h" ],
    [ "Sphere.H", "_sphere_8_h.html", "_sphere_8_h" ],
    [ "SphericalBasis.H", "_spherical_basis_8_h.html", [
      [ "SphericalBasis", "class_spherical_basis.html", "class_spherical_basis" ]
    ] ],
    [ "SphericalCoefs.H", "_spherical_coefs_8_h.html", [
      [ "Coefs", "struct_spherical_coefs_1_1_coefs.html", "struct_spherical_coefs_1_1_coefs" ],
      [ "SphericalCoefs", "class_spherical_coefs.html", "class_spherical_coefs" ]
    ] ],
    [ "StatsMPI.H", "_stats_m_p_i_8_h.html", [
      [ "StatsMPI", "class_stats_m_p_i.html", "class_stats_m_p_i" ]
    ] ],
    [ "tidalField.H", "tidal_field_8_h.html", [
      [ "tidalField", "classtidal_field.html", "classtidal_field" ]
    ] ],
    [ "tipsydefs.h", "src_2tipsydefs_8h.html", "src_2tipsydefs_8h" ],
    [ "TwoCenter.H", "_two_center_8_h.html", [
      [ "TwoCenter", "class_two_center.html", "class_two_center" ]
    ] ],
    [ "version.h", "version_8h.html", "version_8h" ],
    [ "VtkPCA.H", "_vtk_p_c_a_8_h.html", "_vtk_p_c_a_8_h" ]
];