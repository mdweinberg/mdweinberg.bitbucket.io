var class_collision_type_diag =
[
    [ "IDVD", "class_collision_type_diag.html#ae8da90b7738ebcf62508f1ee7e5b96df", null ],
    [ "IDVI", "class_collision_type_diag.html#ac04cfadb170c128a3479138572f59f50", null ],
    [ "IDVP", "class_collision_type_diag.html#a912441055b929cf062d97d90add61961", null ],
    [ "UDDA", "class_collision_type_diag.html#ae02760b6c4a1e01db109271e1576105b", null ],
    [ "CollisionTypeDiag", "class_collision_type_diag.html#a36c54b7008b9827699eff14e15c583bb", null ],
    [ "accum", "class_collision_type_diag.html#a6c69d7973a3d6097956889c63811e6c8", null ],
    [ "reset", "class_collision_type_diag.html#ad4329d22079472d1e45f4bec818b6e77", null ],
    [ "sumUp", "class_collision_type_diag.html#a38b1b9df25c5345b0806897199aef827", null ],
    [ "sync", "class_collision_type_diag.html#a13019bab4a21fa6ffd041367f936af5d", null ],
    [ "syncUDDA", "class_collision_type_diag.html#a2dacdbdae27b806b5e705b1389589368", null ],
    [ "collDiag", "class_collision_type_diag.html#ae093496ab366795581dcc14691a05e8c", null ],
    [ "CE", "class_collision_type_diag.html#ad44a3de4cdd8c87ac08f757032208474", null ],
    [ "CE_s", "class_collision_type_diag.html#a98c9033eb64bf4e3ed5ee739ea9dd260", null ],
    [ "CI", "class_collision_type_diag.html#afb12e76bacc5a8984d3adce53cbeccb6", null ],
    [ "CI_s", "class_collision_type_diag.html#aad7c4362b9f459ea786ac67bac2d51fd", null ],
    [ "dv", "class_collision_type_diag.html#a216e553fd2281a553b3d24da8169f5ef", null ],
    [ "dv_s", "class_collision_type_diag.html#a45860ba7fe1cd7788d1f97e2da172556", null ],
    [ "eV_10", "class_collision_type_diag.html#ae216e6be69545db8f3e72232cd1057bf", null ],
    [ "eV_10_s", "class_collision_type_diag.html#a3a95d804292bd655f70e5d20ced94046", null ],
    [ "eV_av", "class_collision_type_diag.html#a23ac04688f0c931d00615c4131e447ad", null ],
    [ "eV_av_s", "class_collision_type_diag.html#ab6c37602376d882b827f8d1a932e15d8", null ],
    [ "eV_max", "class_collision_type_diag.html#a75ab622d2d8876594e3b9c46f1de1f28", null ],
    [ "eV_max_s", "class_collision_type_diag.html#ae1b38f9ca370c21eb088dde162a1e701", null ],
    [ "eV_min", "class_collision_type_diag.html#ae91d02954faf86594a36b7a7e5e39132", null ],
    [ "eV_min_s", "class_collision_type_diag.html#abf09483899c1dd402e7fcdc31820af8d", null ],
    [ "eV_N", "class_collision_type_diag.html#a155cabd4e0783cb3e7f34dc2d8939d66", null ],
    [ "eV_N_s", "class_collision_type_diag.html#a859ba8c5ab47ae71f915c83ca6cf8d9b", null ],
    [ "ff", "class_collision_type_diag.html#aa5610787a38c4cb361cc29dde56d975a", null ],
    [ "ff_s", "class_collision_type_diag.html#aa2b42b32de82853c679357fcb2cbdc66", null ],
    [ "ie", "class_collision_type_diag.html#a416067d19a6d74235401534c84153700", null ],
    [ "ie_s", "class_collision_type_diag.html#a2288f8e9e049962b265503546c1f27db", null ],
    [ "ne", "class_collision_type_diag.html#adb5f13dc2947197ead1d4588b7ed02ff", null ],
    [ "ne_s", "class_collision_type_diag.html#a4e19c548271e778aa283458e0be12fac", null ],
    [ "nn", "class_collision_type_diag.html#a091b2b72b91c8cb50ea5cd571dc7b20e", null ],
    [ "nn_s", "class_collision_type_diag.html#a0b1465124b8bcfb14d2fb98428684717", null ],
    [ "np", "class_collision_type_diag.html#aca307642983fb1726032bbc3f218b8bc", null ],
    [ "np_s", "class_collision_type_diag.html#ade20275b4b28bd060fa1a5f877575b75", null ],
    [ "RR", "class_collision_type_diag.html#aa9be839bba40775062b7e4c5829bd216", null ],
    [ "RR_s", "class_collision_type_diag.html#ad2eb8330ee500cce8b8ccbacb93d433b", null ],
    [ "zero", "class_collision_type_diag.html#a4e96c4c2b7fd110c47d85a2a5f564c52", null ]
];