var psp__dist_8py =
[
    [ "FitType", "classpsp__dist_1_1_fit_type.html", "classpsp__dist_1_1_fit_type" ],
    [ "PlotType", "classpsp__dist_1_1_plot_type.html", "classpsp__dist_1_1_plot_type" ],
    [ "func1", "psp__dist_8py.html#a2cfdcff4263652b3e2cb398de6f58964", null ],
    [ "func2", "psp__dist_8py.html#aad2dcfc2e30413cae5837fb8e80c1be4", null ],
    [ "main", "psp__dist_8py.html#af1a003cdb779835892c0411a0e66d15e", null ],
    [ "plot_data", "psp__dist_8py.html#a8cd27c0cc8493a8609a4c8a02ce48a4a", null ],
    [ "a0", "psp__dist_8py.html#ab99d5d84c78f34d025b536a8ad95ef06", null ],
    [ "amu", "psp__dist_8py.html#a02333d4ebefcceda9022f3dda79a35ab", null ],
    [ "b0", "psp__dist_8py.html#ab64f6586c4f563f37026eb7e1694c378", null ],
    [ "eV", "psp__dist_8py.html#a07057097f0d7012ed4e966341d122e4f", null ],
    [ "Lunit", "psp__dist_8py.html#a22b99514ccaaea68312778cca4f81397", null ],
    [ "m_H", "psp__dist_8py.html#a7eaf0831d9c3dd94c7666253ef9f7626", null ],
    [ "m_He", "psp__dist_8py.html#aae5d43d04e26f37518aa19620ba3670c", null ],
    [ "Mu_e", "psp__dist_8py.html#a04735c767fe935d141680427ea6c8159", null ],
    [ "Mu_i", "psp__dist_8py.html#a1fa77945973c3299e720ecd5e9b3b7cc", null ],
    [ "Munit", "psp__dist_8py.html#a9aad5f32634c0d632078f796b1e193d4", null ],
    [ "ndatr", "psp__dist_8py.html#a400fe7bb51dbe0ac088e8084c9786dd8", null ],
    [ "Tunit", "psp__dist_8py.html#ac568d3f3ceb941043e26a562d18e3817", null ],
    [ "Vunit", "psp__dist_8py.html#a66aead81f1de7a60a6392057facfb06d", null ],
    [ "X_H", "psp__dist_8py.html#af017a6028f1cb05cc2dc459c98195085", null ],
    [ "X_He", "psp__dist_8py.html#a8ae14a25db8e50e8d57c120f90e21e34", null ]
];