var model3d_8h =
[
    [ "Models3d", "model3d_8h.html#ab6fca3418c4b6179d83a8991a8b2f63c", [
      [ "file", "model3d_8h.html#ab6fca3418c4b6179d83a8991a8b2f63cab4e367fba14b5eae38519a2823aca793", null ],
      [ "isothermal", "model3d_8h.html#ab6fca3418c4b6179d83a8991a8b2f63ca29106aac59fe15bc1faaeb9f36b98369", null ],
      [ "sing_isothermal", "model3d_8h.html#ab6fca3418c4b6179d83a8991a8b2f63ca1e3e02a247b0827f53b3857d2391af03", null ],
      [ "low_sing_isothermal", "model3d_8h.html#ab6fca3418c4b6179d83a8991a8b2f63ca2eaa21639347aa7170b81bd6b045e3f5", null ],
      [ "hernquist_model", "model3d_8h.html#ab6fca3418c4b6179d83a8991a8b2f63cac0970110dfcc13031371c12b384e5988", null ],
      [ "gen_polytrope", "model3d_8h.html#ab6fca3418c4b6179d83a8991a8b2f63ca3d2302023fdfd7af77908fa4c0580176", null ],
      [ "plummer", "model3d_8h.html#ab6fca3418c4b6179d83a8991a8b2f63caa2dc54c26360cd45bf83cb64b7db58b0", null ]
    ] ],
    [ "Model3dNames", "model3d_8h.html#a985611c3d3f696b3b72e057ccc5e6ca0", null ]
];