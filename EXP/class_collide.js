var class_collide =
[
    [ "Labels", "class_collide_1_1_labels.html", "class_collide_1_1_labels" ],
    [ "CrsVelMap", "class_collide.html#ac64126251450d0ee5f372ab746b144e3", null ],
    [ "dvMap", "class_collide.html#a5dd6f8f7e98de0db5f2f2061e695ce16", null ],
    [ "effortL", "class_collide.html#a26883ec5b40c597dab534f56ba8ac840", null ],
    [ "NTCqqHist", "class_collide.html#a2cd3ca25e1dc43421aca3017de50dd14", null ],
    [ "NTCuuHist", "class_collide.html#ad72884e244ed1456d95045c8454ca840", null ],
    [ "Precord", "class_collide.html#ae80e0efea8c18762e94d8937849b74fe", null ],
    [ "qqMap", "class_collide.html#a46b15fa736a3c145b28f651e434442a5", null ],
    [ "UU", "class_collide.html#a69cf25d6de8ab00d1d644b10bf8d8deb", null ],
    [ "Collide", "class_collide.html#a763cc2841e1462faf49d8323faf224bd", null ],
    [ "~Collide", "class_collide.html#a6ff230d36e9ae0d70603ac864e212640", null ],
    [ "ageout", "class_collide.html#a59afac8cff10fb4d2050a0ebef149618", null ],
    [ "atomic_weights_init", "class_collide.html#a7a645827e9dc3ddb8ec2d9cf69625b96", null ],
    [ "auxGather", "class_collide.html#a19d5641ab3d99d961fb43ccdbdf544cf", null ],
    [ "auxPrint", "class_collide.html#a9ec140f33884eaf4ac0d86c9d7016718", null ],
    [ "cellN", "class_collide.html#ab01819724ea0a9f244e74e2b876ca4d1", null ],
    [ "colldeHelper", "class_collide.html#a0916de7b7154ed9e13cdbdcdd935b379", null ],
    [ "colldeTime", "class_collide.html#a77f308603eccdc8d0e41ee3d19f026a8", null ],
    [ "CollectTiming", "class_collide.html#ae6de96bd7f855c3eab48f23493253c5d", null ],
    [ "collide", "class_collide.html#ab76c8fcb93f96e08ae46289bf2158e67", null ],
    [ "collide_thread", "class_collide.html#ac61703cd3f6425062350342ba37086ba", null ],
    [ "collide_thread_fork", "class_collide.html#a633e37a79ea42be8a54ceb618ae38fd9", null ],
    [ "collQuantile", "class_collide.html#ab9a66fba481acb2aa7cf00305ddb15a2", null ],
    [ "compute_timestep", "class_collide.html#a437c578dd474ac1b89cd1423a5695f30", null ],
    [ "CPUHog", "class_collide.html#a8d28fb204f064136d3d8c52e6a7f3247", null ],
    [ "CPUHogGather", "class_collide.html#abb20103837992650f1f08ec82f45f776", null ],
    [ "crossSection", "class_collide.html#adb1de58ed956ced80170109a3b24a1c3", null ],
    [ "debug_list", "class_collide.html#a7132c51ee95041469c9a2d6d0f217987", null ],
    [ "dgnoscTime", "class_collide.html#a74ec91b78b229ce1af9fc902a63c1698", null ],
    [ "dispersion", "class_collide.html#aaf9e506634930ab223af5527e64f27a8", null ],
    [ "Elost", "class_collide.html#a0e11a60c6655b4eb884acc4573cc072f", null ],
    [ "energyExcess", "class_collide.html#a2a6c4bff78616d508f1b673d6b2a501a", null ],
    [ "EPSM", "class_collide.html#a3a72f1140a27cf8dc3ecbd2115c98705", null ],
    [ "EPSMcells", "class_collide.html#aa8c81675da338f410de591190353cab0", null ],
    [ "EPSMtiming", "class_collide.html#a162db9a1e7356743e1f31b9bd70c1324", null ],
    [ "EPSMtimingGather", "class_collide.html#af7acaaf4dcc098aef8d97555a23fd084", null ],
    [ "EPSMtotal", "class_collide.html#a95f13638d13b0a5065c3907f1b379fd3", null ],
    [ "errors", "class_collide.html#a505e702f6a897a99a2677163d52c2ce6", null ],
    [ "Etotal", "class_collide.html#a7bc43089021939dcf9bee3f2ebed65a2", null ],
    [ "finalize_cell", "class_collide.html#a4b79725d95c9faa808085e3d394f11e3", null ],
    [ "finish", "class_collide.html#acdf54bb15d8453755655af73069c4420", null ],
    [ "gatherSpecies", "class_collide.html#ad6008fc65691c8f7ff14d464b3e30915", null ],
    [ "generateSelection", "class_collide.html#a541eb1543e7be1dd836a615644d395d1", null ],
    [ "getCoolingRate", "class_collide.html#a5461630bf1841f5a4490dc35e26359b8", null ],
    [ "hasHeatCool", "class_collide.html#a6b7750999f7de77abdc588ef07e9dc2c", null ],
    [ "hsDiameter", "class_collide.html#a1d2d78a0e4dc0f7000f5aade17bb62c9", null ],
    [ "inelastic", "class_collide.html#a98c1bd544102ddab1280eef094e1d069", null ],
    [ "initialize_cell", "class_collide.html#a3e01b8f712453dda8ee9c26fd0fcc05e", null ],
    [ "initialize_cell_dsmc", "class_collide.html#a3e95012005d570cd1c2ae9460949041f", null ],
    [ "initialize_cell_epsm", "class_collide.html#acead525ecf9bc75226f3168e526f0ebb", null ],
    [ "joinedTime", "class_collide.html#aad6d2561347df2d6d67e4f9bc51c4d9b", null ],
    [ "joinngTime", "class_collide.html#a43144255f8816c700fe5c132a20db9b8", null ],
    [ "KEloss", "class_collide.html#a1db3a7efbd7b4936f786c1fca2cc5149", null ],
    [ "KElossGather", "class_collide.html#afb1e6b3b1bd4f299c651549175420309", null ],
    [ "list_sizes", "class_collide.html#a88f32b681505341063a76e2d2c24390b", null ],
    [ "list_sizes_proc", "class_collide.html#a8f60ed35f2a3d32b516e13e3e2d102a8", null ],
    [ "medianColl", "class_collide.html#af910db866d00beb31a175cc3adb606e3", null ],
    [ "medianNumber", "class_collide.html#ab2efe22bd698732850756e33d440fadf", null ],
    [ "mfpCLGather", "class_collide.html#a14462afebeb4188284c1e34976110ab8", null ],
    [ "mfpCLPrint", "class_collide.html#a6f76af5a0bb50325badf02885875a339", null ],
    [ "mfpsizeQuantile", "class_collide.html#a004a4b7662902676f148f281c75f5b58", null ],
    [ "molWeight", "class_collide.html#a57866a160bb63a8c3cd92dbce3b7865c", null ],
    [ "Mtotal", "class_collide.html#aa5c666cdbfe93be185110cc39d923a6e", null ],
    [ "NTCgather", "class_collide.html#a726b884dc96905f8a12654bb7553d6ee", null ],
    [ "NTCstanza", "class_collide.html#adbf2ed4982e8212dde30625dd50f5373", null ],
    [ "NTCstats", "class_collide.html#a67a33fb072f4ad1f2724044f11bb2f9a", null ],
    [ "pairInfo", "class_collide.html#ab3373f8a011dd011ee31dbead4316ccc", null ],
    [ "Particles", "class_collide.html#afdf597806b60e4cd0a36ad7ad5b91c9f", null ],
    [ "post_cell_loop", "class_collide.html#a89d7755b01bc7d5ca8493ebb323c9740", null ],
    [ "post_collide_diag", "class_collide.html#aec697c14111469744e68cee212cf12cc", null ],
    [ "pre_cell_loop", "class_collide.html#a39992ba70e8aee7f19b05ed9c5ece1a5", null ],
    [ "pre_collide_diag", "class_collide.html#a56a97628bf24c8e9770586a3e03680ef", null ],
    [ "printCollGather", "class_collide.html#a1501ab344bbaec07b4f220e655b32c6c", null ],
    [ "printCollSummary", "class_collide.html#a8545913b1982a171e7e8f4f9d30c309d", null ],
    [ "printSpecies", "class_collide.html#a7a7e205ac6cd706441883f8be7e8144f", null ],
    [ "Qi", "class_collide.html#a65528a1199c2fbb8144f6d79856016eb", null ],
    [ "resetColls", "class_collide.html#a371013e050f9759a9d38444bdcc6802b", null ],
    [ "select", "class_collide.html#a5c971468bfb920a42818e80e99715fd3", null ],
    [ "set_Eint", "class_collide.html#a56f934b2d6b3c2fced715f480b7035b6", null ],
    [ "set_excess", "class_collide.html#a6ebda7aa63f0cac9388d20aebf227064", null ],
    [ "set_key", "class_collide.html#ad05f6b54e18fe36c2908572d431eaea6", null ],
    [ "set_Kn", "class_collide.html#a17be262203836d28b93219059eed76a6", null ],
    [ "set_MFPTS", "class_collide.html#ab68d1bd6e94e4d925479ab2baa488e25", null ],
    [ "set_St", "class_collide.html#a593f497d1b9251585e109501a28ef340", null ],
    [ "set_temp_dens", "class_collide.html#ad190b65fbee59c3a0d330990d7e61008", null ],
    [ "set_timestep", "class_collide.html#ac226038dc387e926700885713bf14249", null ],
    [ "thread_timing_beg", "class_collide.html#afb27f49c297f9ea48d9537597c47ada6", null ],
    [ "thread_timing_end", "class_collide.html#ab10eace20e7d88b6cf124a91258cac34", null ],
    [ "threadTime", "class_collide.html#ad87588c982549da51054d68727be49bb", null ],
    [ "timestep_thread", "class_collide.html#a762a3b21155a14459a7fa9bf884f5e08", null ],
    [ "total", "class_collide.html#a2288508db65b580178fd3590cb31248d", null ],
    [ "totalCrossSections", "class_collide.html#a81d7e51cce9daf5e0a396196a92f9be9", null ],
    [ "tsdiag", "class_collide.html#a0ff37b20983f55176a3acb7f361316b4", null ],
    [ "unit_vector", "class_collide.html#a50930243dcd1d19bb06c34cf6ccf6d45", null ],
    [ "velocityUpdate", "class_collide.html#a23e4d3a7422c036f6c94a24fbb031429", null ],
    [ "voldiag", "class_collide.html#ac49e5603cf4a2a5e5af5e0d321411388", null ],
    [ "waitngTime", "class_collide.html#a8ee34cbe0e65394009b3ab8d7c2df4c0", null ],
    [ "accAcc", "class_collide.html#a3868afae047207b89be7c5ca050ad66c", null ],
    [ "accOvr", "class_collide.html#a729da6f175f92d30c2ff5c12ae37b46d", null ],
    [ "accTot", "class_collide.html#aadcfb86e2f7a2860e44cc7cbf621dc8d", null ],
    [ "atomic_weights", "class_collide.html#a8de5942695b17256c4815b55c11683d4", null ],
    [ "bodycount", "class_collide.html#af747b3a0a51c3666d54ce1787871af4d", null ],
    [ "bufCap", "class_collide.html#aa09862eb05d1faad88ea71f1700d3b05", null ],
    [ "c0", "class_collide.html#a02245b20b63b86bfd19d881d97782f31", null ],
    [ "caller", "class_collide.html#ab3e221f8d7ab721a479755714278beca", null ],
    [ "cellist", "class_collide.html#a92551be9817ce986f8daf4a90e6fdf2e", null ],
    [ "cellSoFar", "class_collide.html#a6f9b86e33f9d1825a8e8fc3ff99a22f1", null ],
    [ "cellSum", "class_collide.html#a1c35784c94673df094208eda72969b37", null ],
    [ "cellTime", "class_collide.html#ab6c302817621a2a7d31a6085902d4da3", null ],
    [ "col1T", "class_collide.html#a9e16f8c8af0a9d68b6f5af955967b92a", null ],
    [ "colcnt", "class_collide.html#a723fb851451aaf175a8a81de4909c841", null ],
    [ "colcntT", "class_collide.html#acd77446fc6b11edd1b2bdd235bd5615c", null ],
    [ "collCnt", "class_collide.html#a1dc1d940312f8a68d7131905a4ac7fb9", null ],
    [ "collSoFar", "class_collide.html#a1e5b768b9ce5bd284046302a1816f1ba", null ],
    [ "collSum", "class_collide.html#a0d833e38a53e14934886d780e9f11578", null ],
    [ "collTime", "class_collide.html#a1772aa3388e4accefbd073ed62cab323", null ],
    [ "collTnum", "class_collide.html#a19f54387c28871b9b636478fb2059503", null ],
    [ "coltot", "class_collide.html#a6b9b8fa393db2a586e65baf44cf56435", null ],
    [ "coolSoFar", "class_collide.html#ab51684362c7ae86fdb20018af153a39a", null ],
    [ "coolSum", "class_collide.html#af19a1f5583dc0c5bdf9cedd42d0ffeaf", null ],
    [ "coolTime", "class_collide.html#ad6f920bc89db99c9849307c07489ba0d", null ],
    [ "Cover", "class_collide.html#a159f21e3c1530a8513d426c9af2ed4fe", null ],
    [ "Cover0", "class_collide.html#acd3bbb2b1767b5d351337e36484db00a", null ],
    [ "Cover1", "class_collide.html#a3b7274256b52cf40e32d2471db75b1b4", null ],
    [ "CoverT", "class_collide.html#ac292b7bc92e32cbda167c996b0ebc289", null ],
    [ "CPUH", "class_collide.html#a24fef65dd72abf2fc108cdc31c37ac97", null ],
    [ "crossfac", "class_collide.html#a27d606dfa665e4a1c556438557617784", null ],
    [ "curcSoFar", "class_collide.html#a6a633f1408174e4fffb0fdd40b6050f4", null ],
    [ "curcTime", "class_collide.html#ab0cb302d640ff77fb0127b025a3d726d", null ],
    [ "DEBUG_NTC", "class_collide.html#a584933830811236878c49d997dce1aec", null ],
    [ "decelT", "class_collide.html#a06b4e2dac8020de598f5c10342f7c788", null ],
    [ "decolT", "class_collide.html#a02e6bd0e1a8f2619aa3af1eef5e0330e", null ],
    [ "derat", "class_collide.html#aee50872ec74189c5c9c34461868559eb", null ],
    [ "deratT", "class_collide.html#a087b9027ac3bb5e19cad38dd6a002603", null ],
    [ "diagSoFar", "class_collide.html#aac84c6c4438c946b785640f8f1541199", null ],
    [ "diagSum", "class_collide.html#a8cdecfcc6ee01a0d2609bf35431b74d4", null ],
    [ "diagTime", "class_collide.html#a087902c9ad956fdc8e1cb94464946114", null ],
    [ "disptot", "class_collide.html#a6dbb10f89fbefd12e88658cf6d724edc", null ],
    [ "DRYRUN", "class_collide.html#aba36ff1774b84020690d17094be1531c", null ],
    [ "EFFORT", "class_collide.html#acf644c4f9083acd08ee4efd0341f51a3", null ],
    [ "effortAccum", "class_collide.html#ab8e7f7335586f0d55ce4e64ffa80f2f3", null ],
    [ "effortNumber", "class_collide.html#ab5c6fc13edfac75535f39c512db6c558", null ],
    [ "elasSoFar", "class_collide.html#a146108ccc356f84d1e4a0cc10bd78f84", null ],
    [ "elasSum", "class_collide.html#a26d297db7645f947d8cff0aab2f0cea9", null ],
    [ "elasTime", "class_collide.html#af8b8025cfa06934f0f045a62432ac0b9", null ],
    [ "electronDef", "class_collide.html#abe46c831dfc23ae26e93781d01775460", null ],
    [ "electronKey", "class_collide.html#ac1e1d6078494960369ced85445bdc6a0", null ],
    [ "electronTyp", "class_collide.html#ad25992286c955c37a8afdeb9d633b214", null ],
    [ "ENHANCE", "class_collide.html#a4e11c030e363668cef96bc4057773e99", null ],
    [ "ENSEXES", "class_collide.html#a3d70e4bb7a62aef0f71cfeacd55bed1c", null ],
    [ "Eover", "class_collide.html#a6ce11fab78f733f2324ffcd7fe0de228", null ],
    [ "Eover0", "class_collide.html#a7c9dae549602d8b45123f8d1d48699c1", null ],
    [ "Eover1", "class_collide.html#ae54ad768584471a034715553ca62c791", null ],
    [ "EoverT", "class_collide.html#a3aaa98aa845fc1fe1ec0e2a5bc47d060", null ],
    [ "epsm1T", "class_collide.html#acfd53a5bd62becf45ae1484728f1bc18", null ],
    [ "epsmcells", "class_collide.html#a6bd32c50bf2351a53470f009a336bc0c", null ],
    [ "EPSMmin", "class_collide.html#a7f3b2d5a7e3fc16ff988879b00f9d6ee", null ],
    [ "EPSMratio", "class_collide.html#a90dd69f21767b0ea629728168b771606", null ],
    [ "epsmSoFar", "class_collide.html#aebf6ae0899ce9838332108314a5165ea", null ],
    [ "epsmSum", "class_collide.html#a54ef7f3cc72cf35d539835274909ef8e", null ],
    [ "EPSMT", "class_collide.html#aac3e82319b6170ad663972c71fee257a", null ],
    [ "epsmTime", "class_collide.html#a121eec53054a2c583fbec3ab779943da", null ],
    [ "EPSMtime", "class_collide.html#a33a7794b58fd6e3d775133cbc1f8b4b1", null ],
    [ "epsmtot", "class_collide.html#a770e4e9315008cba1dbf5a3771d4e535", null ],
    [ "EPSMTSoFar", "class_collide.html#aac052bb65d652e5ba37e1f2effa4db30", null ],
    [ "error1T", "class_collide.html#afc8abfa2482b2db0591dcaf86a825fba", null ],
    [ "errtot", "class_collide.html#ab71f17702fa5b91b07ccfaa7aff91180", null ],
    [ "ESOL", "class_collide.html#a6552b7493e91edf0e2338ff56b429630", null ],
    [ "excessCtot", "class_collide.html#a9c6d828f88816b368dfb3ee77f2ce737", null ],
    [ "excessEtot", "class_collide.html#aba51ade82c2a42999ca053ea4df90106", null ],
    [ "exesCT", "class_collide.html#a4df48dc1dc1647859a62298334d96a2c", null ],
    [ "exesET", "class_collide.html#ad4a998208a684f7f1dc848de0e1a9c1f", null ],
    [ "EXTRA", "class_collide.html#a8221cf534ddd3095665f3c62d569a732", null ],
    [ "forkSoFar", "class_collide.html#a1e7cbd1d5cb68aab85f020c5c2b82852", null ],
    [ "forkSum", "class_collide.html#a9f9eeb29b988cc73827a40c099c29159", null ],
    [ "forkTime", "class_collide.html#a6d62bb96aacf183e3a89f777c18e158c", null ],
    [ "gen", "class_collide.html#ada1ca5ebe2f25c6f5f9b9e023307ef7d", null ],
    [ "hsdiam", "class_collide.html#a8f78f6efccb40458d649817d796ed74e", null ],
    [ "init_dbg", "class_collide.html#abe51f4f0b89115112e893a8ec65eb272", null ],
    [ "init_dbg_time", "class_collide.html#a3497751da80e88e5f67bb0b44f839264", null ],
    [ "init_dbg_Z", "class_collide.html#a8380eed34fdf39b4e36e303efef2a31b", null ],
    [ "initSoFar", "class_collide.html#a4b405fdfb6de8d5c454d7384c833d960", null ],
    [ "initSum", "class_collide.html#aed74deef060bf8b62772d7b61438458c", null ],
    [ "initTime", "class_collide.html#a6d5a3bdd3b38d09af614241e928c3c1e", null ],
    [ "joinSoFar", "class_collide.html#a622b0665187c98fb2ae445ce3cf79eb6", null ],
    [ "joinSum", "class_collide.html#a5363e4b300c60a0440ad500b269c8496", null ],
    [ "joinTime", "class_collide.html#aff44d06d95a3751a08e3ea4276abf09b", null ],
    [ "KElostT", "class_collide.html#a0346809cf9a967299cd8e7a20af04aa6", null ],
    [ "kerat", "class_collide.html#ae30f72d215e829ed7d20e9c8740d63bb", null ],
    [ "keratT", "class_collide.html#a43c1ba56cfd173774e3b0f0e28aecbc1", null ],
    [ "KEtotT", "class_collide.html#ad5d1369395b107efaa7c0e21116045f3", null ],
    [ "labels", "class_collide.html#a1faf72a2eaa9a68382510a12c7955d42", null ],
    [ "listSoFar", "class_collide.html#a5a52593683b04c35b42ed1f9f845f6e3", null ],
    [ "listSum", "class_collide.html#ab26c78d84c05e7928b45af30c754f73d", null ],
    [ "listTime", "class_collide.html#ac89d06d761d692bf1e2c69bbe07e82ab", null ],
    [ "lostSoFar_EPSM", "class_collide.html#ab89c2e9fd5f48c1feec07139d8f608d8", null ],
    [ "masstot", "class_collide.html#a06ef22fa0b0a094a529ca2f049eb09d0", null ],
    [ "maxCollP", "class_collide.html#ad4988343cd6bb04c0a642d8bd63e76b8", null ],
    [ "maxPart", "class_collide.html#a8b955552a56fac6ab32be2bbf6e6824b", null ],
    [ "maxUsage", "class_collide.html#a80ef46f6c499dd7e70a47f488770a52c", null ],
    [ "MFPCL", "class_collide.html#aa7ea22f63a8b645d54b88cb73bf2be41", null ],
    [ "mfpCLdata", "class_collide.html#a2edd3036741002e6eed1627b9468ef7e", null ],
    [ "mfpclHist", "class_collide.html#ade58b84f1957232727ea565db626aa80", null ],
    [ "MFPDIAG", "class_collide.html#a9541cf318b467cb597f9957b7b66c480", null ],
    [ "mfpratT", "class_collide.html#aa9e2074619d46d2fbbdf44e0a018d592", null ],
    [ "MFPTS", "class_collide.html#a5b33405b4dab5eee78942b316c2b5e6f", null ],
    [ "minCollP", "class_collide.html#a70010def247ebb46da948766b4d9499a", null ],
    [ "minPart", "class_collide.html#aae337e5c79ea766b11c653c656dcd4c4", null ],
    [ "minUsage", "class_collide.html#aa5b6fabd92fc2f39b74b7cd3192098bf", null ],
    [ "mlev", "class_collide.html#af6bdb6bc26752713ff816521effcd184", null ],
    [ "mol_weight", "class_collide.html#a26188f4dc0edbb380eccd8eb4cfce893", null ],
    [ "name_id", "class_collide.html#aa197a85af8549df74d2782bbe7484b85", null ],
    [ "ncells", "class_collide.html#ad653d7510246af45da47212fed2fc3cc", null ],
    [ "Nepsm1T", "class_collide.html#a10c0b3ec8a0671dcb3b619435f56165f", null ],
    [ "nEPSMT", "class_collide.html#acffc8170a2a54c5366043803833d7d2f", null ],
    [ "Nmfp", "class_collide.html#a1d781555ac71c8c9f073d92298cc5f92", null ],
    [ "NOCOOL", "class_collide.html#ac2d3a0b3e05b4bf0a9a6db2520468a0d", null ],
    [ "norm", "class_collide.html#a09bdf84e88cddd07858033ca35216362", null ],
    [ "Nphase", "class_collide.html#a064094e491005e5afe4648b645b518fd", null ],
    [ "NTC", "class_collide.html#ae2e2be707f1cc394b3b053287cf75583", null ],
    [ "NTC_DIST", "class_collide.html#a4c6e2fa815945ab077e5db8a3c3af3a5", null ],
    [ "ntcAcc", "class_collide.html#a4985a42ca63b08bbd8eb55afb0127811", null ],
    [ "ntcdb", "class_collide.html#ac771fbf64395563a6c73af739047ef28", null ],
    [ "ntcFactor", "class_collide.html#af05a308b4a7535a290b44495b784e8fa", null ],
    [ "ntcHisto", "class_collide.html#ab531ad340788e27b5537c5526e9ecfe0", null ],
    [ "NTCnodb", "class_collide.html#ad7cfb917e6825bfd6c659f9ea76b8b11", null ],
    [ "ntcOvr", "class_collide.html#aebd2628f89bc02bd709a72dac8eb12d6", null ],
    [ "ntcRes", "class_collide.html#a24c0ccc7c3bc70558d915a3a0736036e", null ],
    [ "ntcSum", "class_collide.html#a467e4b1faea79f249afd67e237065afe", null ],
    [ "ntcThresh", "class_collide.html#a5c53f5b624e4e76d0417ceb350fd2c13", null ],
    [ "ntcThreshDef", "class_collide.html#ad083df68799eb621a3e893cee0086a38", null ],
    [ "ntcTot", "class_collide.html#a0c846f7c0077df11dfe5fb02da415668", null ],
    [ "ntcVal", "class_collide.html#a6acf2518129676becc776d99de3af427", null ],
    [ "nthrds", "class_collide.html#aa3dd0b1547fa1c5fadbc9a9518268223", null ],
    [ "numbSum", "class_collide.html#ac7a1d2ebfb531d212b56b928e10f273b", null ],
    [ "numcnt", "class_collide.html#ad3ba68c105ef09b55daf664f934c4b40", null ],
    [ "numcntT", "class_collide.html#a46b74652854aae5e9caffd3356968538", null ],
    [ "numdiag", "class_collide.html#a49fa270c93e97b041899a1728107ac8a", null ],
    [ "numSanityFreq", "class_collide.html#a7c5f1dce1d33b39bca7d2e4365c9bcd5", null ],
    [ "numSanityMax", "class_collide.html#a1543126f31e9b0ad646faee68f0b183d", null ],
    [ "numSanityMsg", "class_collide.html#ab0e0278ac3cce9efbf79bdf3f95f9173", null ],
    [ "numSanityStop", "class_collide.html#a371a67c3c39029e662edcad741180057", null ],
    [ "numSanityVal", "class_collide.html#aa2d92bfb4bf6cfa567dd60bed2190492", null ],
    [ "numtot", "class_collide.html#a16eee06ea47f061a3ee81e663a1849aa", null ],
    [ "nvold", "class_collide.html#a804b88d0f701fd501c8ab1231aa8d867", null ],
    [ "PHASE", "class_collide.html#a614bc6cc10a8a4dbec8d2ef5be52942a", null ],
    [ "prec", "class_collide.html#abd11aea2dde2b3158d4138e8553a3e9b", null ],
    [ "printDivider", "class_collide.html#a33bc98d1fc59b13369781e415ffa7977", null ],
    [ "PULLIN", "class_collide.html#a14e554c824be37c8c42d3565b93ed0dc", null ],
    [ "qq", "class_collide.html#a2f677744d16684fde25a6f1a14dd800f", null ],
    [ "qqHisto", "class_collide.html#a0b4b89ba0387f8d33b51bebe6258670b", null ],
    [ "qv", "class_collide.html#acb93336d6f2a37d12d6392570f6c3544", null ],
    [ "ret", "class_collide.html#a0482178322dfb986142b9f5d10460335", null ],
    [ "retCrs", "class_collide.html#a6d668f958d82fdaa87d5855354834f1a", null ],
    [ "SampleCRM", "class_collide.html#a881173173dc09be5deb0df9c93047033", null ],
    [ "seed", "class_collide.html#ad4728972b646d2ce6767cc518ace2bea", null ],
    [ "sel1T", "class_collide.html#ae14e0bf1cb7e3724902c91dc05058f75", null ],
    [ "selcnt", "class_collide.html#ab3df3e14cf92ecc066108f0aa9c5dc18", null ],
    [ "selMFP", "class_collide.html#a4e24590db4a82a37420ce04a5dc97f94", null ],
    [ "seltot", "class_collide.html#a991890aae1652b1609cf4798e1c9328e", null ],
    [ "snglSoFar", "class_collide.html#a7c7c25f3f8ebd08cff4bc818e10e97f0", null ],
    [ "snglSum", "class_collide.html#a9bfe878624eac1913c1080342a287c65", null ],
    [ "snglTime", "class_collide.html#af38122d1c27ddaf0763f67226fbcce5f", null ],
    [ "SORTED", "class_collide.html#a0c412041613f399fb844510f2dbb9d80", null ],
    [ "species_file_debug", "class_collide.html#a02190932f853f0f30549920a3a0aad77", null ],
    [ "stat1SoFar", "class_collide.html#aa2b31e8186eeabde5f0df63445332e40", null ],
    [ "stat1Sum", "class_collide.html#ab6f3adf007083b3cf002bef9bcd3b768", null ],
    [ "stat1Time", "class_collide.html#ac40c332a239b329b28a6786f5799c9a0", null ],
    [ "stat2SoFar", "class_collide.html#ad65d71a352213efe3eea8d290dceac6e", null ],
    [ "stat2Sum", "class_collide.html#ada8f24540bab90ae9a5c6baab2584931", null ],
    [ "stat2Time", "class_collide.html#a5c21b147f76b533129d76a95c2306ca5", null ],
    [ "stat3SoFar", "class_collide.html#aa5842b9bcfc72f0bcd243a301fb948ba", null ],
    [ "stat3Sum", "class_collide.html#acd414a736ddaee05d481d879633a1416", null ],
    [ "stat3Time", "class_collide.html#ac1e0a15474b1ab4a21e3cb6a596fd372", null ],
    [ "stepcount", "class_collide.html#a43ebdc316790a2d89cb4279890fce9d1", null ],
    [ "t", "class_collide.html#adca6ebfec5f45b1887d1559eeadc219d", null ],
    [ "tcool", "class_collide.html#a1d34d296abd34500515b48a13c061b03", null ],
    [ "tcool0", "class_collide.html#ac5adf89c6ff94393134140e3a41df64e", null ],
    [ "tcool1", "class_collide.html#a07434fda15f521932b900e37fe3b2187", null ],
    [ "tcoolT", "class_collide.html#a207f19bc3bd30f219bd99a331eca2a93", null ],
    [ "td", "class_collide.html#ac3ab61ed34859da81c4298b06feb3a31", null ],
    [ "tdelt", "class_collide.html#a69d2bb4b6d0b050ae56446d4b853f00a", null ],
    [ "tdeltT", "class_collide.html#af4baf3e9a597d4a71aec9c28c8665dae", null ],
    [ "tdens", "class_collide.html#af9660c8043f41cbf493b919e371d3319", null ],
    [ "tdensT", "class_collide.html#a6c8b7b37ded3237e2c5f4b894af3a537", null ],
    [ "tdiag", "class_collide.html#a05d9c09abc3f946ac61466751800ef92", null ],
    [ "tdiag0", "class_collide.html#a8571c058b18105e779ac6b486328deab", null ],
    [ "tdiag1", "class_collide.html#aecb2c998d296774a1964f52283c21c8e", null ],
    [ "tdiagT", "class_collide.html#a565998973637e0bb90d76e8f87d3c9fd", null ],
    [ "tdispT", "class_collide.html#ac345cd9ad46289311dbe20782a289264", null ],
    [ "tdT", "class_collide.html#ae62e218c9884abca2839d349a81b5c1b", null ],
    [ "TestSpreadCount", "class_collide.html#ad4e373664f952ce664b2f4ccadf783c6", null ],
    [ "TFLOOR", "class_collide.html#aecc8051aa6f6a7292c49a546baac761a", null ],
    [ "timer_list", "class_collide.html#a98639feed9809d7fdcdad62bc1e10b98", null ],
    [ "TIMING", "class_collide.html#a42205479e4b308c2a5ad7df73d42ca5f", null ],
    [ "tlock", "class_collide.html#a4893eaff6cb68baa8dd47c11b5c3a245", null ],
    [ "tmassT", "class_collide.html#a9e770062f5aa8844c514eb8cc5bbd17c", null ],
    [ "tmfpst", "class_collide.html#a9baf4207d6dd944eb37ccd51aa9c95c8", null ],
    [ "tmfpstT", "class_collide.html#a2d359377ae649b06a242e55ff88d9c16", null ],
    [ "tphase", "class_collide.html#afba7745e8d67918adcdfbe0dae66b6a0", null ],
    [ "tphaseT", "class_collide.html#a8dcff5627f9c74bf34a0945d62e2a7d9", null ],
    [ "tree", "class_collide.html#a8d00c3db74fb19ab6fe24e3dc9991039", null ],
    [ "TSDIAG", "class_collide.html#a9e54a355c89283c47b04e9117e4f0d41", null ],
    [ "tseln", "class_collide.html#a8427235c81a85ef8a3053e01c209f8f7", null ],
    [ "tselnT", "class_collide.html#ab5978f127acf56d28e57d7be70916910", null ],
    [ "TSPOW", "class_collide.html#a2ae5c67cfe4e279acf44e7338def441a", null ],
    [ "tsrat", "class_collide.html#ada9f3cfe7f0b01336920c14d04be8bd0", null ],
    [ "tsratT", "class_collide.html#abdb5522aa62ff64ac63c065bb336f8f4", null ],
    [ "ttemp", "class_collide.html#a8330c4ecdcab2d36af25a590bb7a79f6", null ],
    [ "ttempT", "class_collide.html#ae46a3b5dfcfd7bc975574626b04619a5", null ],
    [ "tvolc", "class_collide.html#a2250d79e19db8cc9e3033e0d36725637", null ],
    [ "tvolcT", "class_collide.html#a6f3253694849b76df60ca2f25593636a", null ],
    [ "unit", "class_collide.html#aadb4d1b9a1c5c42f7cb1e49a6dbb9d17", null ],
    [ "use_delt", "class_collide.html#a949e13b7e8425314a99628c727f40fde", null ],
    [ "use_dens", "class_collide.html#ad5e75753df0a46ebb83039fc97cfcc34", null ],
    [ "use_Eint", "class_collide.html#a3348198ff24a2fcd230e42e253e32330", null ],
    [ "use_epsm", "class_collide.html#a0f45d5e2a39637d07cdf253e1792cdff", null ],
    [ "use_exes", "class_collide.html#af3fbd45eeda434e688b0caf944b6da40", null ],
    [ "use_key", "class_collide.html#a09b184a3d79a4b16ecd4fb21be99d075", null ],
    [ "use_Kn", "class_collide.html#ad750f0dbb847fba5a38a2d048ae67bde", null ],
    [ "use_ntcdb", "class_collide.html#a41cafa85bfe22ba7a399d7b0dc981461", null ],
    [ "use_St", "class_collide.html#a5c76dbb35d2861b15350d26be094964f", null ],
    [ "use_temp", "class_collide.html#af14823305cf7d7a64ef7668bf4cc8ee2", null ],
    [ "Vcnt", "class_collide.html#a9a69660b1b3f6233c7a7cffdc613ef6d", null ],
    [ "Vcnt0", "class_collide.html#ac5de526ad5253ae9d116d1454a259466", null ],
    [ "Vcnt1", "class_collide.html#aa17451af2761673653b5e5b78a527ae0", null ],
    [ "VcntT", "class_collide.html#aa8f13f297bb4dcb11701c9680318add3", null ],
    [ "Vdbl", "class_collide.html#a4aca0ce7c291f385f8b35748e289af98", null ],
    [ "Vdbl0", "class_collide.html#a8825e3d1fe2b10efa802602b77618d71", null ],
    [ "Vdbl1", "class_collide.html#a142eff6c3ec18feade0d60563ef55fa3", null ],
    [ "VdblT", "class_collide.html#ac3f55a18566e50a0545377c399e44748", null ],
    [ "version_id", "class_collide.html#a550d1382f24690f52df7f74366292b97", null ],
    [ "VOLDIAG", "class_collide.html#a38c9a5fe78b4901ea5ca73eef5bdacd3", null ],
    [ "waitSoFar", "class_collide.html#ae7c68fe2a3558430f4660de8551a45ac", null ],
    [ "waitSum", "class_collide.html#a4a7f8acebc63766e1613d8fdb0cd2ec9", null ],
    [ "waitTime", "class_collide.html#a03ea4e6832bc175c8482185483918a8f", null ],
    [ "wgtHisto", "class_collide.html#ae1e28f65cf8515aabd4f3fed0d81c27b", null ],
    [ "wgtSum", "class_collide.html#ad50ee2d003dd7a413c6fbe7af0f39d00", null ],
    [ "wgtVal", "class_collide.html#a84f2d5354f13b6d89e3c70843bb89758", null ]
];