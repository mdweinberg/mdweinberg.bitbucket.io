var class_circular_orbit =
[
    [ "CircularOrbit", "class_circular_orbit.html#a5a185c7dd37bc1e5ce21283b0fe64fc8", null ],
    [ "compute_coefficients", "class_circular_orbit.html#aab96eb08a0d7e7e86e262dc486ebe8a2", null ],
    [ "compute_omega", "class_circular_orbit.html#a13c50738d06c37c6807dedabbd2b9a98", null ],
    [ "eval", "class_circular_orbit.html#a287bbe876550f5970f27768537304db4", null ],
    [ "AMPLITUDE", "class_circular_orbit.html#a3f7c331c59e1b83631e0fc07ae97c8ca", null ],
    [ "mass", "class_circular_orbit.html#a000e6d6d91b6ec2d9ffd79b6a10484af", null ],
    [ "radius", "class_circular_orbit.html#a6f0eaca1fa8b9812941ec804a515fc81", null ]
];