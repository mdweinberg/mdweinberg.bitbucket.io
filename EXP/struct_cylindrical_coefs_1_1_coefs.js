var struct_cylindrical_coefs_1_1_coefs =
[
    [ "read", "struct_cylindrical_coefs_1_1_coefs.html#af4f9984c6eede3ebdc74a1d8fad5ded5", null ],
    [ "cos_c", "struct_cylindrical_coefs_1_1_coefs.html#a93da41eb4b3ae12637fe32ac43ed5f06", null ],
    [ "mmax", "struct_cylindrical_coefs_1_1_coefs.html#ae7d94be3e8e4df9fd62e348dccf610f6", null ],
    [ "nmax", "struct_cylindrical_coefs_1_1_coefs.html#aef3aecb23ebb19b06ff755e784adefee", null ],
    [ "sin_c", "struct_cylindrical_coefs_1_1_coefs.html#acedccba5c4181a25a3b7e6e1932b69e6", null ],
    [ "time", "struct_cylindrical_coefs_1_1_coefs.html#a62abdd510fe8917a6d8e85c63aa0b7d1", null ]
];