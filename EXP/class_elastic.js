var class_elastic =
[
    [ "InterpPair", "class_elastic.html#a732f2c58c485729b7c00926aa2f66f13", null ],
    [ "xSection", "class_elastic.html#a6e98f1c53ccd7e54ba414f1a9445f9d0", null ],
    [ "CPtype", "class_elastic.html#aac99557a0d39d66ff9e815dba7fdb4ac", [
      [ "electron", "class_elastic.html#aac99557a0d39d66ff9e815dba7fdb4aca12b87883843149f5d1311ce5da747f3c", null ],
      [ "proton", "class_elastic.html#aac99557a0d39d66ff9e815dba7fdb4aca8d57099a88e1e870121461c2a8958d23", null ]
    ] ],
    [ "Elastic", "class_elastic.html#a885fc3df563f254cd21763e9933ebb75", null ],
    [ "interpolate", "class_elastic.html#ad20321a0bd812d87d8ac6a2d92028312", null ],
    [ "operator()", "class_elastic.html#a08a7a4b3ad297404d253db85ad8f3e26", null ],
    [ "atomicdata", "class_elastic.html#a866065ef87cdc1a3133cab0bfa567902", null ],
    [ "extrapolate", "class_elastic.html#af696e8450a75dd59d686766e51cfba97", null ],
    [ "iondata", "class_elastic.html#a8050295a0c44ef6e97073d97de95a8f6", null ],
    [ "pin", "class_elastic.html#acf11cd5b6c05d88b7b8b315c847acafa", null ]
];