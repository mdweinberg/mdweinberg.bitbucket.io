var coef__power_8py =
[
    [ "data", "coef__power_8py.html#ad65da744cfac21fb489b6bf0e9ad3cdf", null ],
    [ "file", "coef__power_8py.html#ad64735de2fb951d529e48e0a095ed558", null ],
    [ "label", "coef__power_8py.html#af067568c72a10d570464c16da266c9ad", null ],
    [ "m", "coef__power_8py.html#a903cb8a44ac3041a7a49d196de90d7cf", null ],
    [ "power", "coef__power_8py.html#aebd594b31ddfabcf97bef84422f2c04d", null ],
    [ "sum", "coef__power_8py.html#ac8046ea330d7bb7e4086a09f25aa6097", null ],
    [ "sums", "coef__power_8py.html#a0d1db762bbad3980e3c4997baeab7386", null ],
    [ "t", "coef__power_8py.html#a57f33deb4a07127cdfb57047c3bfb844", null ],
    [ "time", "coef__power_8py.html#ad317fde0aeeddd0905bf1cf710628d26", null ],
    [ "title", "coef__power_8py.html#ad0d183dab87dc790f885dd63188d5f46", null ],
    [ "toks", "coef__power_8py.html#a92d4db03c46ba728b35dcf2e9da9eef8", null ],
    [ "v", "coef__power_8py.html#a52857d19920288c5e653de7054d3541b", null ],
    [ "z", "coef__power_8py.html#aafa7a023d3a8edd54570cac68355df9f", null ]
];