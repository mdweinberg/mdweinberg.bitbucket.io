var structdark__particle =
[
    [ "eps", "structdark__particle.html#acf616c8d2a03b5c00a1e32c259e47381", null ],
    [ "mass", "structdark__particle.html#ac92bfe5d859987ff69aefcb931a91292", null ],
    [ "phi", "structdark__particle.html#a842f32d0aad6d801e54089553c00c5cd", null ],
    [ "pos", "structdark__particle.html#af86e9870ea96dbc5963326015d8ede71", null ],
    [ "vel", "structdark__particle.html#a1b8a9ab2e8676f493a35a62eed79c4fe", null ]
];