var class_ultra =
[
    [ "Ultra", "class_ultra.html#a3cc10ab65fb04480f8a0fc65c8668939", null ],
    [ "coef1", "class_ultra.html#a300930c02d3c86cea3684bcd953b528b", null ],
    [ "coef2", "class_ultra.html#a91b9ca746463e7bda00cf84aa6df5421", null ],
    [ "coef3", "class_ultra.html#a3da223bbee626d16cc568d6d69e16e76", null ],
    [ "coef4", "class_ultra.html#a4f973fceaabb2faa5a21e05e268b7d42", null ],
    [ "f0", "class_ultra.html#a5315994bd8122511d13408f7d8115b92", null ],
    [ "f1", "class_ultra.html#aade53399b6b7634abd5a58d062d4413d", null ],
    [ "h", "class_ultra.html#a1a071cc2dd0f100eb6682405bbcfe648", null ],
    [ "w", "class_ultra.html#a91a7c07fff9ef042f830a3b699dfc2fe", null ],
    [ "alpha", "class_ultra.html#a2a30fb0a4fdf5fc41d9cb06a16275d50", null ]
];