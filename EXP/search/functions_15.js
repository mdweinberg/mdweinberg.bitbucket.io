var searchData=
[
  ['v_5fcirc_7992',['v_circ',['../class_disk_halo.html#a07a4917e58daf565082f1dfd49bdbaa5',1,'DiskHalo::v_circ(double xp, double yp, double zp)'],['../class_disk_halo.html#a07a4917e58daf565082f1dfd49bdbaa5',1,'DiskHalo::v_circ(double xp, double yp, double zp)'],['../phase_8h.html#aa7e32e3c67df0b9093a055eccd20062a',1,'v_circ(double):&#160;phase.h'],['../rotcurv_8h.html#aa7e32e3c67df0b9093a055eccd20062a',1,'v_circ(double):&#160;rotcurv.h']]],
  ['value_7993',['value',['../class_cubic___table.html#af5480b829c618a08696f5f3a580bf179',1,'Cubic_Table']]],
  ['vbkts_7994',['vbkts',['../classvbkts.html#a5e9e1064a8abb8ffd6bb140815cae7b3',1,'vbkts']]],
  ['vector_7995',['Vector',['../class_vector.html#adc7052514a833f0fbb5ee306a5550ea0',1,'Vector::Vector(void)'],['../class_vector.html#a57a33816590480f25f900dbbdbb6913b',1,'Vector::Vector(int, int)'],['../class_vector.html#abced3f1767b12ea82e6072b57456865e',1,'Vector::Vector(int, int, double *)'],['../class_vector.html#a130470658bcf6440baa7280f4b94449b',1,'Vector::Vector(const Vector &amp;)']]],
  ['vectorsynchronize_7996',['VectorSynchronize',['../_vector_8h.html#a0c944521aa72cc64d0ac4f27aa66c979',1,'Vector.h']]],
  ['veebeta_7997',['VeeBeta',['../_res_pot_8_h.html#a693c653ef20d94a727115e4cb415f071',1,'VeeBeta(int l, int l2, int m, double beta):&#160;ResPot.H'],['../_res_pot_orb_8_h.html#a693c653ef20d94a727115e4cb415f071',1,'VeeBeta(int l, int l2, int m, double beta):&#160;ResPotOrb.H']]],
  ['vel_7998',['vel',['../class_s_particle.html#a41dfa34d6ec61e198033136a872c0c00',1,'SParticle::vel(int i)'],['../class_s_particle.html#a41dfa34d6ec61e198033136a872c0c00',1,'SParticle::vel(int i)'],['../class_s_particle.html#a41dfa34d6ec61e198033136a872c0c00',1,'SParticle::vel(int i)'],['../class_s_particle.html#a41dfa34d6ec61e198033136a872c0c00',1,'SParticle::vel(int i)'],['../class_component.html#a73f9ed983dd632e1714a694a31303214',1,'Component::Vel(int i, int j, unsigned flags=Inertial)'],['../class_component.html#adf9d24409865914ebbd72e60a91eba07',1,'Component::Vel(double *vel, int i, unsigned flags=Inertial)'],['../classp_cell.html#a9e255be662032f18293db6d4bdba16e3',1,'pCell::Vel()'],['../namespaceplot_o_u_t_l_o_g.html#afb7dc55af8d8631ba397ed7bf9dd4b0c',1,'plotOUTLOG.vel()']]],
  ['velocity_7999',['Velocity',['../class_phase.html#a1a260326c028b979e37631fd370e44a1',1,'Phase::Velocity(void)'],['../class_phase.html#aec38d075d7c9e8ab20aede9987ead41f',1,'Phase::Velocity(int i)']]],
  ['velocityupdate_8000',['velocityUpdate',['../class_collide.html#a23e4d3a7422c036f6c94a24fbb031429',1,'Collide::velocityUpdate()'],['../class_collide_ion.html#adbf4c77be307e46fc14a942f7214eb0a',1,'CollideIon::velocityUpdate()']]],
  ['verbose_5fdf_8001',['verbose_df',['../class_embedded_disk_model.html#a94c9dc67a4fe9ce9e6781c94038ac912',1,'EmbeddedDiskModel']]],
  ['vernerdata_8002',['VernerData',['../class_verner_data.html#a79abf99d193081e611f2a587d45061be',1,'VernerData']]],
  ['vertical_5fkappa_8003',['vertical_kappa',['../phase_8h.html#a7a47c6bd8133e3b423cca596337e31de',1,'vertical_kappa(double):&#160;phase.h'],['../rotcurv_8h.html#a7a47c6bd8133e3b423cca596337e31de',1,'vertical_kappa(double):&#160;rotcurv.h']]],
  ['virial_8004',['Virial',['../class_ensemble.html#ae39d07aa5db5fe9ccffa1ad490191d6c',1,'Ensemble']]],
  ['virial_5fratio_8005',['virial_ratio',['../class_disk_halo.html#a62c375ae24d2db9b0286bc05c845e8f3',1,'DiskHalo::virial_ratio(vector&lt; Particle &gt; &amp;hpart, vector&lt; Particle &gt; &amp;dpart)'],['../class_disk_halo.html#ad88d6fe1c7f1a8d6bc4d5a379b3cec73',1,'DiskHalo::virial_ratio(const char *, const char *)'],['../class_disk_halo.html#a62c375ae24d2db9b0286bc05c845e8f3',1,'DiskHalo::virial_ratio(vector&lt; Particle &gt; &amp;hpart, vector&lt; Particle &gt; &amp;dpart)'],['../class_disk_halo.html#ad88d6fe1c7f1a8d6bc4d5a379b3cec73',1,'DiskHalo::virial_ratio(const char *, const char *)'],['../class_disk_halo.html#a62c375ae24d2db9b0286bc05c845e8f3',1,'DiskHalo::virial_ratio(vector&lt; Particle &gt; &amp;hpart, vector&lt; Particle &gt; &amp;dpart)'],['../class_disk_halo.html#ad88d6fe1c7f1a8d6bc4d5a379b3cec73',1,'DiskHalo::virial_ratio(const char *, const char *)']]],
  ['visited_8006',['visited',['../classkdtree.html#a2c19e2011f2f139b34c7feacccf8cb2a',1,'kdtree']]],
  ['vlocate_8007',['Vlocate',['../interp_8h.html#a20a0c2de030a80d7cad32a2523c3afd2',1,'Vlocate(double x, const Vector &amp;xtab):&#160;interp.h'],['../interp_8h.html#aef43f105ecadc0dd19c4be25d0398614',1,'Vlocate(double x, const V &amp;xtab):&#160;interp.h']]],
  ['vlocate_5fwith_5fguard_8008',['Vlocate_with_guard',['../interp_8h.html#adcc3b181410da127da013c55346dc9f8',1,'interp.h']]],
  ['voldiag_8009',['voldiag',['../class_collide.html#ac49e5603cf4a2a5e5af5e0d321411388',1,'Collide']]],
  ['volume_8010',['Volume',['../classs_cell.html#a9f42d40b0ee636200bdd0cfda94ff374',1,'sCell::Volume()'],['../classp_cell.html#a165fbed4e8658bc32631ad4ecafcbc8d',1,'pCell::Volume()'],['../classp_h_o_t.html#a7eb7867a021445e3cf77977e004154f6',1,'pHOT::Volume()'],['../classp_h_o_t__iterator.html#a76a3b377dc05c595f93454afabc9cfc2',1,'pHOT_iterator::Volume()']]],
  ['vp_5fdisp2_8011',['vp_disp2',['../class_disk_halo.html#afcd53163c0b27571599370db80c02987',1,'DiskHalo']]],
  ['vphi_8012',['vphi',['../class_disk_halo.html#aee4c3f1bad9424dbebea985189211fcb',1,'DiskHalo']]],
  ['vplgndr_8013',['vplgndr',['../class_hunter_disk.html#a5d9dc6c1a508e857e3b262fc96cb9878',1,'HunterDisk']]],
  ['vr_5fdisp_8014',['vr_disp',['../class_disk_halo.html#ab39fd9cf4346fb4362655173cd5a697f',1,'DiskHalo']]],
  ['vr_5fdisp2_8015',['vr_disp2',['../class_disk_halo.html#a9e368c3397604f8b491fb536f48b9e80',1,'DiskHalo']]],
  ['vtkgrid_8016',['VtkGrid',['../class_vtk_grid.html#a708cc3f68c13e7e532f89cec61dbbf38',1,'VtkGrid']]],
  ['vtkpca_8017',['VtkPCA',['../class_vtk_p_c_a.html#ac28a0d18a8fe5e4e5c04085b7e049d04',1,'VtkPCA']]],
  ['vz_5fdisp2_8018',['vz_disp2',['../class_disk_halo.html#ab7e59c8984171fd64f832ce6af568c6c',1,'DiskHalo']]]
];
