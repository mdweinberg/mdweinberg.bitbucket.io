var searchData=
[
  ['u_10980',['u',['../class_low_sing_isothermal_sphere.html#a08dd1e739309bb4ed31a621ae5530952',1,'LowSingIsothermalSphere::u()'],['../class_swap.html#ab2e8ddd2d09305aee6912f8420738640',1,'Swap::u()'],['../class_c_brock_disk.html#a390ce3b6a03d7da2dbb006fd02f5ec6e',1,'CBrockDisk::u()'],['../class_spherical_basis.html#a6ea9a5c1ea07f2205cab2e1e0af63516',1,'SphericalBasis::u()'],['../class_satellite_orbit.html#a54545fb099bc3633f85afd1818885a41',1,'SatelliteOrbit::u()'],['../class_user_sat_wake.html#a9affec35c98bb325215d869b6e5b2da5',1,'UserSatWake::u()']]],
  ['u0_10981',['U0',['../class_user_slab_halo.html#a5be85b230a3415793b44a987720e573d',1,'UserSlabHalo::U0()'],['../class_satellite_orbit.html#aa15ba88d86888354b59452c807a4d2b7',1,'SatelliteOrbit::u0()']]],
  ['u1_10982',['u1',['../class_n_t_c_1_1_quantile_bag.html#a0a300a9910c0793e1cf3b552c7e6e512',1,'NTC::QuantileBag']]],
  ['u2_10983',['u2',['../class_n_t_c_1_1_quantile_bag.html#a3fe91a4afc8cfd1751007acc543ec29c',1,'NTC::QuantileBag']]],
  ['u3_10984',['u3',['../class_n_t_c_1_1_quantile_bag.html#ae85cb312cdbee43510aa78bc506fe72c',1,'NTC::QuantileBag']]],
  ['u4_10985',['u4',['../class_n_t_c_1_1_quantile_bag.html#a3e1b7271102b90184116cc8d0190b149',1,'NTC::QuantileBag']]],
  ['umagic_10986',['umagic',['../class_component.html#a49ec95ffe08a0d63888d8287f6b00e80',1,'Component']]],
  ['uni_10987',['uni',['../class_interact_select.html#aaf0205799606ef7891a6ce03c74ef92d',1,'InteractSelect']]],
  ['uni_5fdist_10988',['uni_dist',['../class_interact_select.html#a09c1c75217aa29fa1988c3c85aa78166',1,'InteractSelect']]],
  ['unif_10989',['unif',['../class_scatter_m_f_p.html#a9b78e7f314b92533f27387d886c1608e',1,'ScatterMFP::unif()'],['../class_user_d_s_m_c.html#a329914996747bf7580f92ddde1a6273b',1,'UserDSMC::unif()']]],
  ['unit_10990',['Unit',['../class_axi_sym_model.html#a2ab1c0f70c4bbba2c16fffdce7f91125',1,'AxiSymModel::Unit()'],['../class_collide.html#aadb4d1b9a1c5c42f7cb1e49a6dbb9d17',1,'Collide::unit()'],['../class_collide_ion.html#a1ef48c9a56bc0ef2a5122259333b117f',1,'CollideIon::unit()'],['../class_user_periodic.html#a5b228e4540b5f321c488b43a8054124a',1,'UserPeriodic::unit()'],['../class_user_reflect.html#a4c9466f928ad9fafdbe3adccf673ec6b',1,'UserReflect::unit()'],['../class_user_s_nheat.html#a46f454f3f49bfb22ec23031d44e42025',1,'UserSNheat::unit()']]],
  ['unpack_10991',['unpack',['../class_spherical_basis.html#a6cd5fd9214ea8df66e296eab10382a34',1,'SphericalBasis']]],
  ['updat3_10992',['updat3',['../classp_h_o_t.html#a88214187cc8142f60415f31fc895f656',1,'pHOT']]],
  ['update_5ffr_10993',['update_fr',['../class_shells.html#a9603694917d162ffd5803c40a30d4365',1,'Shells']]],
  ['update_5fii_10994',['update_ii',['../class_shells.html#ac211f5fdb90908fc57b3f4b44aca84f3',1,'Shells']]],
  ['update_5fto_10995',['update_to',['../class_shells.html#a56f84d8f690cb33cdcd54e16b9e6a313',1,'Shells']]],
  ['upde_10996',['updE',['../classcoll_diag.html#a75548cd6290ef15577cd822310d4a208',1,'collDiag::updE()'],['../class_collide_ion.html#aa1cda71f5aca7b1b5a7b79349ff1c8df',1,'CollideIon::updE()']]],
  ['upde_5fs_10997',['updE_s',['../classcoll_diag.html#a509727a4748f0df74bfb79bffe921197',1,'collDiag']]],
  ['urand_10998',['urand',['../class_two_body_diffuse.html#a607008568091d53701c0a2587d233e33',1,'TwoBodyDiffuse::urand()'],['../class_user_add_mass.html#a33cec142674ede725068f848f4c73983',1,'UserAddMass::urand()'],['../class_user_diffuse.html#af01522c2f0317b664d407b9c7c329ebd',1,'UserDiffuse::urand()']]],
  ['us_10999',['us',['../class_key_convert.html#a921d282b1ac566a6b75deff553085bd4',1,'KeyConvert']]],
  ['use_11000',['use',['../class_pot_accel.html#acd32c6464b705469911df718114187b9',1,'PotAccel::use()'],['../class_spherical_s_l.html#a50e26f96820e4e50de3a58fc64b1c121',1,'SphericalSL::use()']]],
  ['use0_11001',['use0',['../class_c_brock_disk.html#a887ac1dd5f27c671af0f53975d39f351',1,'CBrockDisk::use0()'],['../class_cube.html#a0fa11ac46e1eb6cdb946d26dd8617cca',1,'Cube::use0()']]],
  ['use1_11002',['use1',['../class_c_brock_disk.html#ae58e834b8446db7f0d0f54da9df71de9',1,'CBrockDisk::use1()'],['../class_cube.html#a81c93879fae311863874fc1cb419d43f',1,'Cube::use1()']]],
  ['use_5facc_11003',['use_acc',['../class_orb_trace.html#af4fcce1c4fd3ec56be003324de2ce5cf',1,'OrbTrace']]],
  ['use_5fcons_11004',['use_cons',['../class_collide_ion.html#a35b05dee95365995381f46071915e04b',1,'CollideIon']]],
  ['use_5fcuda_11005',['use_cuda',['../global_8_h.html#a0ad8ed6e0196a9477f123c6bf569233b',1,'global.H']]],
  ['use_5fcwd_11006',['use_cwd',['../global_8_h.html#a11647d1a8a725b7d9b36745e9f996daa',1,'global.H']]],
  ['use_5fdelt_11007',['use_delt',['../class_collide.html#a949e13b7e8425314a99628c727f40fde',1,'Collide::use_delt()'],['../class_tree_d_s_m_c.html#a243f128ed0f51a5c0cb9cec3c16e28db',1,'TreeDSMC::use_delt()']]],
  ['use_5fdens_11008',['use_dens',['../class_collide.html#ad5e75753df0a46ebb83039fc97cfcc34',1,'Collide::use_dens()'],['../class_tree_d_s_m_c.html#a20eda48da634bd4f64f9f550fce6e667',1,'TreeDSMC::use_dens()']]],
  ['use_5feffort_11009',['use_effort',['../class_tree_d_s_m_c.html#ae50af391cefaca69e187d73d5a2312f5',1,'TreeDSMC']]],
  ['use_5feint_11010',['use_Eint',['../class_collide.html#a3348198ff24a2fcd230e42e253e32330',1,'Collide::use_Eint()'],['../class_tree_d_s_m_c.html#a3226b8dc19718ea1f1a77fb0f045b8f3',1,'TreeDSMC::use_Eint()']]],
  ['use_5felec_11011',['use_elec',['../class_collide_ion.html#a0f32a571d6e01a9dc1dbfa8e6d752cc4',1,'CollideIon']]],
  ['use_5fepsm_11012',['use_epsm',['../class_collide.html#a0f45d5e2a39637d07cdf253e1792cdff',1,'Collide']]],
  ['use_5fexes_11013',['use_exes',['../class_collide.html#af3fbd45eeda434e688b0caf944b6da40',1,'Collide::use_exes()'],['../class_tree_d_s_m_c.html#a7106ba4b4b9f8c8d98ead40c2b17359e',1,'TreeDSMC::use_exes()']]],
  ['use_5fexternal_11014',['use_external',['../class_pot_accel.html#a59a4a86ea8d2b3a9a5d420ef7e712697',1,'PotAccel']]],
  ['use_5ffile_11015',['use_file',['../class_user_diffuse.html#aa65305d550b2eda58e9ef935303b5446',1,'UserDiffuse']]],
  ['use_5fkey_11016',['use_key',['../class_collide.html#a09b184a3d79a4b16ecd4fb21be99d075',1,'Collide::use_key()'],['../class_tree_d_s_m_c.html#a996459e9d2e01c02a7f9a93b9b565935',1,'TreeDSMC::use_key()']]],
  ['use_5fkn_11017',['use_Kn',['../class_collide.html#ad750f0dbb847fba5a38a2d048ae67bde',1,'Collide::use_Kn()'],['../class_tree_d_s_m_c.html#a14ab6e8e27fb77af5e471402e761e452',1,'TreeDSMC::use_Kn()']]],
  ['use_5flev_11018',['use_lev',['../class_orb_trace.html#afd7bb20f6d3544e374c74ca048dccdaa',1,'OrbTrace']]],
  ['use_5fmpi_11019',['use_mpi',['../class_add_disk.html#abd2041a9deffc1d1d2971adb5e88fec0',1,'AddDisk']]],
  ['use_5fmulti_11020',['use_multi',['../class_tree_d_s_m_c.html#a62f5b1a7ca5dc047e0839c8e12506f65',1,'TreeDSMC']]],
  ['use_5fntcdb_11021',['use_ntcdb',['../class_collide.html#a41cafa85bfe22ba7a399d7b0dc981461',1,'Collide']]],
  ['use_5fphotoib_11022',['use_photoIB',['../class_collide_ion.html#a3a7c83a46e7d0c598b182f0621f70395',1,'CollideIon']]],
  ['use_5fphoton_11023',['use_photon',['../class_collide_ion.html#a936658fcce29e15d938e4a011a279055',1,'CollideIon']]],
  ['use_5fpot_11024',['use_pot',['../class_orb_trace.html#a366b9fbd3ef9d63a214b55478ad6d2cc',1,'OrbTrace']]],
  ['use_5fpullin_11025',['use_pullin',['../class_tree_d_s_m_c.html#aa7880cdc86ea3c92c225b73b78b682b5',1,'TreeDSMC']]],
  ['use_5fratio_11026',['use_ratio',['../class_collide_ion.html#acbd9d94c00a6cea5145c6af6e21e310e',1,'CollideIon']]],
  ['use_5frepair_11027',['use_repair',['../class_tree_d_s_m_c.html#a0586482e2715cd89e2d15afca7db44c9',1,'TreeDSMC']]],
  ['use_5fspectrum_11028',['use_spectrum',['../class_collide_ion.html#a958e2fd5c1d8de623681dc9303f3fa24',1,'CollideIon']]],
  ['use_5fspline_11029',['use_spline',['../class_disk_halo.html#a3f01d08f0f3793460399bbc5ffbcd097',1,'DiskHalo']]],
  ['use_5fst_11030',['use_St',['../class_collide.html#a5c76dbb35d2861b15350d26be094964f',1,'Collide::use_St()'],['../class_tree_d_s_m_c.html#a8af383384c40232842e82aad7cae1413',1,'TreeDSMC::use_St()']]],
  ['use_5ftemp_11031',['use_temp',['../class_collide.html#af14823305cf7d7a64ef7668bf4cc8ee2',1,'Collide::use_temp()'],['../class_tree_d_s_m_c.html#ae1a95dec4a041523fe154031a4b04a4c',1,'TreeDSMC::use_temp()']]],
  ['use_5fvfky_11032',['use_VFKY',['../class_ion.html#ae5e78c161ba64c6e72a9a63de4a7924d',1,'Ion']]],
  ['use_5fvol_11033',['use_vol',['../class_tree_d_s_m_c.html#a9fd7d686f07d1094d0137a3869f3caa6',1,'TreeDSMC']]],
  ['use_5fweight_11034',['use_weight',['../classp_h_o_t.html#aed3072178967f577490ffe3249b334fe',1,'pHOT']]],
  ['usebar_11035',['usebar',['../class_user_res_pot.html#a007e850e242114c3bd1f6356dd6006e4',1,'UserResPot::usebar()'],['../class_user_res_pot_n.html#a09fb7afa3e9c57962e0363f66b2d5e35',1,'UserResPotN::usebar()']]],
  ['usecache_11036',['UseCache',['../class_user_sat_wake.html#af360bca28f7fa926e0a02d6a2b531d4c',1,'UserSatWake::UseCache()'],['../class_heat_cool.html#a54a6ddaa83ff4b1aee72164f1a0ae00d',1,'HeatCool::useCache()']]],
  ['used_11037',['used',['../class_biorth_wake.html#a1b4e13bcd12d9150089c2254874eaece',1,'BiorthWake::used()'],['../class_orient.html#afdb91c183fd364fe2991af75abef823c',1,'Orient::used()'],['../class_out_log.html#ae74e4331db6c6d138f5dd39cbf7df5a6',1,'OutLog::used()'],['../class_pot_accel.html#a8fe5beef77e790f0d7bf6f455f5bf1d3',1,'PotAccel::used()'],['../class_spherical_s_l.html#a1d7bd83bd68c5ce84eb0a892246aa47a',1,'SphericalSL::used()'],['../class_sphere_s_l.html#a5aa222a303766f116c6c1863bebecbce',1,'SphereSL::used()']]],
  ['used1_11038',['used1',['../class_out_log.html#a43a02715b10102e5d0704db707b9dd64',1,'OutLog::used1()'],['../class_shells.html#abd9fff8ce1b52df6635272f1b1d5dbb4',1,'Shells::used1()']]],
  ['used_5flock_11039',['used_lock',['../class_c_brock_disk.html#a3f59419da8520d8ed64b0fe07cee83e1',1,'CBrockDisk::used_lock()'],['../class_cylinder.html#a7d85149a9b32eade518db4948d271fce',1,'Cylinder::used_lock()'],['../class_emp_cyl_s_l.html#ac5989dfc11361c8449b9aa8bfb5a4a3c',1,'EmpCylSL::used_lock()']]],
  ['usedt_11040',['usedT',['../class_shells.html#af603375e538442fffdc09788d0dbf567',1,'Shells']]],
  ['useexcitegrid_11041',['useExciteGrid',['../class_ion.html#ab0984a0bbae14105740b1d722b0d8f6d',1,'Ion']]],
  ['usefreefreegrid_11042',['useFreeFreeGrid',['../class_ion.html#a459aa3067973a2a5a1f6d7dddc5d98cd',1,'Ion']]],
  ['useionizegrid_11043',['useIonizeGrid',['../class_ion.html#a79364ebfa848eacfc92a768914074e66',1,'Ion']]],
  ['useorb_11044',['useorb',['../class_user_res_pot.html#aa74a1cbd028dd5bc150fb471305b953e',1,'UserResPot::useorb()'],['../class_user_res_pot_n.html#a0d24ea54d7e00999be9a6eef0b054bf8',1,'UserResPotN::useorb()']]],
  ['user_5fomega_11045',['user_omega',['../class_bar_forcing.html#aa4bd40aa4b4fdbe63e90898b64abaaf4',1,'BarForcing']]],
  ['useradrecombgrid_11046',['useRadRecombGrid',['../class_ion.html#a0442b8c87cc10bd8fead1cc259c6b0a9',1,'Ion']]],
  ['usesvd_11047',['USESVD',['../class_emp_cyl_s_l.html#a7ff5a8a2d67b8b5faaafa63d3d762f08',1,'EmpCylSL']]],
  ['usetag_11048',['usetag',['../class_user_res_pot.html#af7fe12ad97adfb5e6f9197029f476cd2',1,'UserResPot::usetag()'],['../class_user_res_pot_n.html#aca4d52efcdc4bb8d83c0f45036636848',1,'UserResPotN::usetag()'],['../class_user_res_pot_orb.html#a9fc0c87ed535d67fb08f20edd6c549aa',1,'UserResPotOrb::usetag()']]],
  ['uu_11049',['uu',['../class_c_brock.html#ab96410b9edce5fe69fec3d270f8cacdc',1,'CBrock::uu()'],['../class_user_e_bar_n.html#ae069deb6fdbaf207edfeee311df1890d',1,'UserEBarN::uu()'],['../class_ellipsoid_force.html#a414904096eebce2d3025f7886d7650dd',1,'EllipsoidForce::uu()']]]
];
