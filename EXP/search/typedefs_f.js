var searchData=
[
  ['raddata_11329',['radData',['../class_geometric.html#a70e0dbd6e2923e5bf77ecd3cc9eef838',1,'Geometric']]],
  ['real_11330',['Real',['../src_2tipsydefs_8h.html#ab685845059e8a2c92743427d9a698c70',1,'Real():&#160;tipsydefs.h'],['../utils_2_phase_space_2tipsydefs_8h.html#ab685845059e8a2c92743427d9a698c70',1,'Real():&#160;tipsydefs.h']]],
  ['recombratiomap_11331',['recombRatioMap',['../class_collide_ion.html#ad562767fafb22e1f4574b06c9d61d492',1,'CollideIon']]],
  ['recvelem_11332',['RecvElem',['../class_barrier_wrapper.html#a39928dc8d3ef7bc5aa6378fc6e28aa24',1,'BarrierWrapper']]],
  ['reqptr_11333',['ReqPtr',['../_barrier_wrapper_8_h.html#a3e07343f209cedf31b32bf7725c55b79',1,'BarrierWrapper.H']]],
  ['return_11334',['Return',['../class_stats_m_p_i.html#a1eb9c91baf779feb62dac1daf389388d',1,'StatsMPI']]],
  ['rpair_11335',['rpair',['../_scatter_m_f_p_8_h.html#ac13832c7133d31977830c35cd49f0f2a',1,'ScatterMFP.H']]],
  ['rr_5flab_11336',['RR_Lab',['../class_ion.html#a45bfc3e6c840b54dc22b0ef0e68271a2',1,'Ion']]],
  ['rr_5fmap_11337',['RR_Map',['../class_ion.html#ad639008856d2a40b73d97d9f473ea014',1,'Ion']]]
];
