var searchData=
[
  ['x_5freturn_5fmap_8053',['x_return_map',['../class_phase.html#ab4d5d4c4dda4fc0a1b91805f17ae55a8',1,'Phase']]],
  ['x_5fto_5fr_8054',['x_to_r',['../class_disk_eval.html#af527e5bb033d5f040b59b43146555752',1,'DiskEval']]],
  ['xchange_8055',['Xchange',['../classp_h_o_t.html#aa644a393b506af693314d6fcd0043286',1,'pHOT']]],
  ['xdr_5fdark_8056',['xdr_dark',['../utils_2_phase_space_2tipsydefs_8h.html#a18bac09320b8f4587a454d45c16c4ac6',1,'tipsydefs.h']]],
  ['xdr_5fgas_8057',['xdr_gas',['../utils_2_phase_space_2tipsydefs_8h.html#a182ca7f6caa29af42d040cc2983cd730',1,'tipsydefs.h']]],
  ['xdr_5finit_8058',['xdr_init',['../utils_2_phase_space_2tipsydefs_8h.html#ad5c01cb2c6a74faae19cf4a55e01b4cd',1,'tipsydefs.h']]],
  ['xdr_5fquit_8059',['xdr_quit',['../utils_2_phase_space_2tipsydefs_8h.html#a54920b3241ba7c64ec8762e4d992fe10',1,'tipsydefs.h']]],
  ['xdr_5fstar_8060',['xdr_star',['../utils_2_phase_space_2tipsydefs_8h.html#a3ca824c3364115b20f5df3d0dd7ef086',1,'tipsydefs.h']]],
  ['xi_5fto_5fr_8061',['xi_to_r',['../class_s_l_grid_cyl.html#aa8001f7f8909682b6d1a9a93e6724787',1,'SLGridCyl::xi_to_r()'],['../class_s_l_grid_sph.html#a6faeba3e71cfc3f8995fbebdedce438f',1,'SLGridSph::xi_to_r()'],['../class_emp_cyl_s_l.html#a40fe0cc482bea73f3393f5162d391e2c',1,'EmpCylSL::xi_to_r()']]],
  ['xi_5fto_5fz_8062',['xi_to_z',['../class_s_l_grid_slab.html#a1c73ea39ff0739ca2e37809553af8438',1,'SLGridSlab']]],
  ['xion_8063',['xion',['../class_heat_cool.html#a677e5e70fb02328177ec0b5654a7b56c',1,'HeatCool']]],
  ['xj_8064',['xJ',['../class_res_pot.html#a6b58eb585481b2511e418a8d96c5939b',1,'ResPot::xJ()'],['../class_res_pot_orb.html#a1cd5c9704695c8b3cac8bd8f3b38cadb',1,'ResPotOrb::xJ()']]],
  ['xmax_8065',['xmax',['../class_n_t_c_1_1_quantile.html#a26098784bdd1ec01558ae191bfade957',1,'NTC::Quantile::xmax()'],['../class_n_t_c_1_1_quantile_bag.html#af63ab5fa29c7be5d0510a831829c3256',1,'NTC::QuantileBag::xmax()']]],
  ['xmin_8066',['xmin',['../class_n_t_c_1_1_quantile.html#a991673669280ecd2dac4c261e3f43d26',1,'NTC::Quantile::xmin()'],['../class_n_t_c_1_1_quantile_bag.html#a0034badab46524933cee6174b7dcc8fb',1,'NTC::QuantileBag::xmin()']]],
  ['xstup_8067',['XStup',['../struct_collide_ion_1_1_x_stup.html#aeb617c3d3403be2ccb971a7ab5fb2be3',1,'CollideIon::XStup::XStup()'],['../struct_collide_ion_1_1_x_stup.html#a7faa7a48cdeb24087ca99672e190b91e',1,'CollideIon::XStup::XStup(const NTC::T &amp;t)']]]
];
