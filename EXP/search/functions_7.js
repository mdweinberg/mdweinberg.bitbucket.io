var searchData=
[
  ['h_7009',['h',['../class_ortho_poly.html#a012dedf1c757a8ac3c999f2c6c2c6b05',1,'OrthoPoly::h()'],['../class_gen_lagu.html#a9d7ecca4767fe280c50869b3a66b525b',1,'GenLagu::h()'],['../class_legendre.html#a3ff47311ca802bdf62979533b23bb947',1,'Legendre::h()'],['../class_ultra.html#a1a071cc2dd0f100eb6682405bbcfe648',1,'Ultra::h()'],['../class_cheb1.html#ab7dcbd54cb623cc67ba7cc366d73ca84',1,'Cheb1::h()'],['../class_cheb2.html#a9ddb27326d84b1e34210b8c1713c7b2c',1,'Cheb2::h()'],['../class_hermite.html#af5882ed3ef371ac816f721a4d5dbcced',1,'Hermite::h()']]],
  ['halobulge_7010',['HaloBulge',['../class_halo_bulge.html#adec71270f0f642021a0239121b175b8d',1,'HaloBulge']]],
  ['hasheatcool_7011',['hasHeatCool',['../class_collide.html#a6b7750999f7de77abdc588ef07e9dc2c',1,'Collide::hasHeatCool()'],['../class_collide_ion.html#af00002d140103f0fbaa34259c2a9012a',1,'CollideIon::hasHeatCool()'],['../class_collide_l_t_e.html#a7865ac3f219c239979a96069ff7ce73f',1,'CollideLTE::hasHeatCool()']]],
  ['havecoefdump_7012',['HaveCoefDump',['../class_pot_accel.html#a4a812594dff363f07bf57d69a60e4a13',1,'PotAccel']]],
  ['header_7013',['header',['../class_out_diag.html#a5965e4614910a5d89f2b2912ad06906f',1,'OutDiag']]],
  ['heatcool_7014',['HeatCool',['../class_heat_cool.html#a81068f5f43afdc2f194808a1ef9b8828',1,'HeatCool::HeatCool()'],['../class_heat_cool.html#a6e043041820750ad662840c9afa038e5',1,'HeatCool::HeatCool(double n, double T)'],['../class_heat_cool.html#ab5453392be7ab80d59e698907469f881',1,'HeatCool::HeatCool(double nmin, double nmax, double Tmin, double Tmax, unsigned nnum, unsigned Tnum, string cache=&quot;&quot;)']]],
  ['heatrate_7015',['HeatRate',['../class_heat_cool.html#a6de401e75e0267696a2b28e6b998dc6c',1,'HeatCool']]],
  ['heavy_5foperator_7016',['heavy_operator',['../class_barrier_wrapper.html#a502821e5fc06a438a54b8782a5dda179',1,'BarrierWrapper']]],
  ['hermite_7017',['Hermite',['../class_hermite.html#a65f7a86f19d866306d86c4dd26124dfa',1,'Hermite']]],
  ['hermquad_7018',['HermQuad',['../class_herm_quad.html#a57421fe7c96898260f3b384843797cd6',1,'HermQuad']]],
  ['hernquist_7019',['Hernquist',['../class_hernquist.html#aa611566d9629632cd8b09003c9d5bc96',1,'Hernquist']]],
  ['hernquistsphere_7020',['HernquistSphere',['../class_hernquist_sphere.html#a984f3ad39bf053316522666c3318af89',1,'HernquistSphere']]],
  ['histogram_7021',['histogram',['../class_n_t_c_1_1_quantile.html#a9ea8018b8fb1d6787bcec8659ab85a84',1,'NTC::Quantile']]],
  ['hotcreate_7022',['HOTcreate',['../class_component.html#acf1378e1b976305b9a2c07ed65984dc2',1,'Component']]],
  ['hotdelete_7023',['HOTdelete',['../class_component.html#ad903e166cbc0e20a3abce9fe547f290f',1,'Component']]],
  ['hqsphere_7024',['HQSphere',['../class_h_q_sphere.html#ae8b3c95585716adf85e48205b82160f5',1,'HQSphere']]],
  ['hsdiameter_7025',['hsDiameter',['../class_collide.html#a1d2d78a0e4dc0f7000f5aade17bb62c9',1,'Collide']]],
  ['hunterdisk_7026',['HunterDisk',['../class_hunter_disk.html#a1156f9f91f6db7bb82c76c392b92fe43',1,'HunterDisk']]],
  ['hunterdiskx_7027',['HunterDiskX',['../class_hunter_disk_x.html#abbb277972ddaaea04770fe01f150c319',1,'HunterDiskX']]]
];
