var structstar__particle =
[
    [ "eps", "structstar__particle.html#ac02db9fda2617382a8999bab6f0a0479", null ],
    [ "mass", "structstar__particle.html#a22c8264aa236c8e484994600f0482757", null ],
    [ "metals", "structstar__particle.html#ac6e5347d756bb19ab660df15fdc0d231", null ],
    [ "phi", "structstar__particle.html#a5cdb5f439b6c5cc8bc12a9a6084631fc", null ],
    [ "pos", "structstar__particle.html#a232e40d334a0fa00967a1ed92aa4493d", null ],
    [ "tform", "structstar__particle.html#a33fbc674592133b01b56472f10992ce6", null ],
    [ "vel", "structstar__particle.html#ad9bc18dabcea03f086984fb207e775ec", null ]
];