var class_sat_fix =
[
    [ "SatFix", "class_sat_fix.html#af518e50a07318cd927136d53675458de", null ],
    [ "~SatFix", "class_sat_fix.html#a8977ada85037c59f01f9b3b3b59ed050", null ],
    [ "check_body", "class_sat_fix.html#ae03f695785133db67a326e624abbc973", null ],
    [ "compute_list", "class_sat_fix.html#a2015a63a6de98c096cc6ba7bfcf9ac13", null ],
    [ "determine_acceleration_and_potential_thread", "class_sat_fix.html#a06974070e25994b495cd564ea9e83538", null ],
    [ "get_acceleration_and_potential", "class_sat_fix.html#a5fe08fad86e6c7d84ccea32fce77e1ae", null ],
    [ "initialize", "class_sat_fix.html#a46e33c58b8f56ee74314980bab8eff2c", null ],
    [ "print_body", "class_sat_fix.html#ac351d9041c33180734bcdf13b2a36198", null ],
    [ "userinfo", "class_sat_fix.html#a0c4e2d2f4846b16ebff5e2e7829f2c52", null ],
    [ "c0", "class_sat_fix.html#a837c456f5a746dbba8332edc575c990b", null ],
    [ "comp_name", "class_sat_fix.html#aa186b6e79e5b5ba5f4ae61c3435a3f2f", null ],
    [ "debug", "class_sat_fix.html#a7cea9baee3c521b762ee09aecd42372a", null ],
    [ "owner", "class_sat_fix.html#a9b9262136dd19de132b8edaca4d7fd42", null ],
    [ "status", "class_sat_fix.html#a016990d8528fb32ad5b1d4fa7c2c2b81", null ],
    [ "verbose", "class_sat_fix.html#ae37bdce8844adea07a2743d44902408e", null ]
];