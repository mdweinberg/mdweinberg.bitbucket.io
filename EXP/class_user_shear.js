var class_user_shear =
[
    [ "UserShear", "class_user_shear.html#afb78921b82a9f4c99b102063cd698e2f", null ],
    [ "~UserShear", "class_user_shear.html#aac4c04b37f4cb1f7a77f8b6ac799fa12", null ],
    [ "determine_acceleration_and_potential", "class_user_shear.html#a8ba93169648c49d9ec9e2d42e8a8e1fe", null ],
    [ "determine_acceleration_and_potential_thread", "class_user_shear.html#af1a18443950452677fa72056d63fe221", null ],
    [ "initialize", "class_user_shear.html#acd670060bbed3cd6e1765b8c2a14505d", null ],
    [ "userinfo", "class_user_shear.html#a9fee8e125aef7e30fc251a761ee3954f", null ],
    [ "A", "class_user_shear.html#aafd762a33b73411a4a0a80a3528a96f3", null ],
    [ "B", "class_user_shear.html#ae1e7777e139d559a8fb3a036afc4ed90", null ],
    [ "c0", "class_user_shear.html#a6c42c23cf5dd27fdbce541d2cc04e2cb", null ],
    [ "ctr_name", "class_user_shear.html#a85140edcbcd6049cfd8d833530c5bdbc", null ],
    [ "kappa", "class_user_shear.html#a8252d2beca84291cac5bab047c43aa48", null ],
    [ "omega", "class_user_shear.html#ac1284045fe7d23221f1c6128b39b2224", null ],
    [ "r0", "class_user_shear.html#a9e9ce1c1e7c455fec418b49aab33ae6d", null ],
    [ "s0", "class_user_shear.html#ad00b0954a14ae276a354614f6e880912", null ],
    [ "xoff", "class_user_shear.html#a4ee7f1d674f1899d6b44f0846ead0e7c", null ]
];