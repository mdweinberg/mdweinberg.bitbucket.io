var dir_2ee2cc264e21d8f7dd3d8c50544928e7 =
[
    [ "atomic_constants.H", "src_2_d_s_m_c_2atomic__constants_8_h.html", "src_2_d_s_m_c_2atomic__constants_8_h" ],
    [ "Collide.H", "_collide_8_h.html", [
      [ "Collide", "class_collide.html", "class_collide" ],
      [ "Labels", "class_collide_1_1_labels.html", "class_collide_1_1_labels" ],
      [ "thrd_pass_arguments", "structthrd__pass__arguments.html", "structthrd__pass__arguments" ],
      [ "thrd_pass_Collide", "structthrd__pass___collide.html", "structthrd__pass___collide" ],
      [ "thrd_pass_tstep", "structthrd__pass__tstep.html", "structthrd__pass__tstep" ],
      [ "tstep_pass_arguments", "structtstep__pass__arguments.html", "structtstep__pass__arguments" ]
    ] ],
    [ "CollideIon.H", "_collide_ion_8_h.html", "_collide_ion_8_h" ],
    [ "CollideLTE.H", "_collide_l_t_e_8_h.html", [
      [ "CollideLTE", "class_collide_l_t_e.html", "class_collide_l_t_e" ]
    ] ],
    [ "Elastic.H", "_elastic_8_h.html", [
      [ "Elastic", "class_elastic.html", "class_elastic" ],
      [ "Geometric", "class_geometric.html", "class_geometric" ]
    ] ],
    [ "HeatCool.H", "_heat_cool_8_h.html", [
      [ "HeatCool", "class_heat_cool.html", "class_heat_cool" ],
      [ "Rates", "struct_heat_cool_1_1_rates.html", "struct_heat_cool_1_1_rates" ]
    ] ],
    [ "interactSelect.H", "interact_select_8_h.html", "interact_select_8_h" ],
    [ "Ion.H", "_ion_8_h.html", "_ion_8_h" ],
    [ "NTC.H", "_n_t_c_8_h.html", "_n_t_c_8_h" ],
    [ "oxy.py", "oxy_8py.html", "oxy_8py" ],
    [ "Quantile.H", "_quantile_8_h.html", "_quantile_8_h" ],
    [ "QuantileBag.H", "_quantile_bag_8_h.html", [
      [ "QuantileBag", "class_n_t_c_1_1_quantile_bag.html", "class_n_t_c_1_1_quantile_bag" ]
    ] ],
    [ "recomb.py", "recomb_8py.html", "recomb_8py" ],
    [ "recomb_py.h", "recomb__py_8h.html", null ],
    [ "spline.H", "src_2_d_s_m_c_2_spline_8h.html", [
      [ "band_matrix", "classtk_1_1band__matrix.html", "classtk_1_1band__matrix" ],
      [ "spline", "classtk_1_1spline.html", "classtk_1_1spline" ]
    ] ],
    [ "TopBase.H", "_top_base_8_h.html", [
      [ "Error", "class_top_base_1_1_error.html", "class_top_base_1_1_error" ],
      [ "FileOpen", "class_top_base_1_1_file_open.html", "class_top_base_1_1_file_open" ],
      [ "NoIon", "class_top_base_1_1_no_ion.html", "class_top_base_1_1_no_ion" ],
      [ "NoLine", "class_top_base_1_1_no_line.html", "class_top_base_1_1_no_line" ],
      [ "NoSLP", "class_top_base_1_1_no_s_l_p.html", "class_top_base_1_1_no_s_l_p" ],
      [ "TBline", "struct_top_base_1_1_t_bline.html", "struct_top_base_1_1_t_bline" ],
      [ "TopBase", "class_top_base.html", "class_top_base" ]
    ] ],
    [ "TreeDSMC.H", "_tree_d_s_m_c_8_h.html", [
      [ "TreeDSMC", "class_tree_d_s_m_c.html", "class_tree_d_s_m_c" ]
    ] ],
    [ "Verner.H", "_verner_8_h.html", "_verner_8_h" ]
];