var _quantile_8_h =
[
    [ "Quantile", "class_n_t_c_1_1_quantile.html", "class_n_t_c_1_1_quantile" ],
    [ "Box", "_quantile_8_h.html#a1e3c9cc1b2d00f2ea398cf86699fd564", [
      [ "none", "_quantile_8_h.html#a1e3c9cc1b2d00f2ea398cf86699fd564a334c4a4c42fdb79d7ebc3e73b517e6f8", null ],
      [ "quantile", "_quantile_8_h.html#a1e3c9cc1b2d00f2ea398cf86699fd564a2494662b29e0b2100e97dbdcfc4a9670", null ],
      [ "histogram", "_quantile_8_h.html#a1e3c9cc1b2d00f2ea398cf86699fd564abdfb01f3ea1f6351d0d53d79625c347f", null ]
    ] ],
    [ "load", "_quantile_8_h.html#ad4cd638e7f193d415d49bc60cb7ff27b", null ],
    [ "save", "_quantile_8_h.html#abc9e8d88ec8056f6f28a8882591c0c2d", null ],
    [ "serialize", "_quantile_8_h.html#a22afe46c472bfe01fc9059ed9e2d033b", null ]
];