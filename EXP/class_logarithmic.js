var class_logarithmic =
[
    [ "force", "class_logarithmic.html#a046d9dd7e36dd8e23625b6bd0f1b8b77", null ],
    [ "get_r0", "class_logarithmic.html#a3cc9a01d9c29b720d8ba28b1f10677b7", null ],
    [ "get_vc", "class_logarithmic.html#a91b20e15fa8e05b1a8555db00129ad37", null ],
    [ "potential", "class_logarithmic.html#a95faf8589b971b2da624aacb790d70c2", null ],
    [ "set_Logarithmic", "class_logarithmic.html#a9b22631d654a47b66972a9b27564db0f", null ],
    [ "set_r0", "class_logarithmic.html#a1fd639eb35be83c03ca9c815fc7a60ec", null ],
    [ "set_vc", "class_logarithmic.html#a157de3f08ff0ad9104c2a6de3a143ce7", null ],
    [ "qy", "class_logarithmic.html#a1ab4020740e6c8ae2c817979393ffc0f", null ],
    [ "qz", "class_logarithmic.html#a4eb30bf19420aa36b4ae36b94cf9fd89", null ],
    [ "r0", "class_logarithmic.html#acea95b1a8f95a26022ca0d76d61077a9", null ],
    [ "vc", "class_logarithmic.html#ab799616b226dcce7655131cbd4c5e3dc", null ],
    [ "vcsq", "class_logarithmic.html#aeda40f0b3caf40e8a1d62ffbebeca15c", null ]
];