var dir_bc1ea1dd34437456447120be9e7e4b32 =
[
    [ "atomic_constants.H", "utils_2_phase_space_2atomic__constants_8_h.html", "utils_2_phase_space_2atomic__constants_8_h" ],
    [ "header.H", "utils_2_phase_space_2header_8_h.html", [
      [ "ComponentHeader", "class_component_header.html", "class_component_header" ],
      [ "MasterHeader", "class_master_header.html", "class_master_header" ]
    ] ],
    [ "KDtree.H", "_k_dtree_8_h.html", "_k_dtree_8_h" ],
    [ "PSP.H", "_phase_space_2_p_s_p_8_h.html", "_phase_space_2_p_s_p_8_h" ],
    [ "PSP2.H", "_phase_space_2_p_s_p2_8_h.html", "_phase_space_2_p_s_p2_8_h" ],
    [ "tipsydefs.h", "utils_2_phase_space_2tipsydefs_8h.html", "utils_2_phase_space_2tipsydefs_8h" ]
];