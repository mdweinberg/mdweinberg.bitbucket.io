var plot_coll_8py =
[
    [ "listAll", "plot_coll_8py.html#aae64c580172c367ea380a3fbebb18be6", null ],
    [ "listGlobal", "plot_coll_8py.html#ab5c65f5637991e4a7dcb3872a49726bc", null ],
    [ "listSpecies", "plot_coll_8py.html#ad10ece67bd397b5b11b13245b85fa1ce", null ],
    [ "main", "plot_coll_8py.html#a46027e7002ae82b76aa737a64afc8462", null ],
    [ "plotEnergy", "plot_coll_8py.html#a520e58d8c19f7deed0228efbfe81469a", null ],
    [ "readDB", "plot_coll_8py.html#aa90a8c76c99b898efeda50d5b3f91235", null ],
    [ "showAll", "plot_coll_8py.html#a9b686b6fdfaffdcd5ac08227b0d2d1fa", null ],
    [ "showSpecies", "plot_coll_8py.html#aea7b5b4d26a52a7c1534d7551ab14ea3", null ],
    [ "db", "plot_coll_8py.html#a272acb5dbf8d5f6a66caee81d6e154b9", null ],
    [ "labs", "plot_coll_8py.html#af645d4b191179469770de4ab27574c80", null ],
    [ "species", "plot_coll_8py.html#af8958fe55f7971d4e406a53f8df881b2", null ]
];