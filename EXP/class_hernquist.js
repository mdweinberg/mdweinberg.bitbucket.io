var class_hernquist =
[
    [ "Hernquist", "class_hernquist.html#aa611566d9629632cd8b09003c9d5bc96", null ],
    [ "d_r_to_rq", "class_hernquist.html#a8fc8710f2d1f7fe9f46a866d652ffff1", null ],
    [ "force", "class_hernquist.html#a6c0fe9e150caeec1888192f1408153e0", null ],
    [ "get_dens", "class_hernquist.html#afa7780e41cc919cd2ffcba87901a549b", null ],
    [ "get_dpotl", "class_hernquist.html#a48f92033b2207191b7f0f59d4d6d132d", null ],
    [ "get_potl", "class_hernquist.html#a2296416c4f72b86dfc869f0078e51025", null ],
    [ "get_potl_dens", "class_hernquist.html#a19fe026e41282948ca4b7fc5c51b0767", null ],
    [ "initialize", "class_hernquist.html#ad1f47f7a2150b79229624d103a81655f", null ],
    [ "knl", "class_hernquist.html#a0bb3b96c576dbfda7d3e4c7c4c8a7f64", null ],
    [ "norm", "class_hernquist.html#adc35805a7e230274e22658a088c8abfe", null ],
    [ "potential", "class_hernquist.html#affc71b82820652eb848714fa216e293a", null ],
    [ "r_to_rq", "class_hernquist.html#ad39317326268c2e0dcf7a4b964fa3e7a", null ],
    [ "rq_to_r", "class_hernquist.html#ad4909edd3536cac0ae9459a283e36995", null ],
    [ "set_Hernquist", "class_hernquist.html#a66800c4e57c5018740d7248905af89e7", null ],
    [ "G", "class_hernquist.html#a248e980ca253c00b56620c4bc74b9af9", null ],
    [ "GM", "class_hernquist.html#ae4bb107309f01c47fc8b63de5e0dcb36", null ],
    [ "M", "class_hernquist.html#a9efabc87e5d9ff1f26ab9bbe59f75c9c", null ],
    [ "r0", "class_hernquist.html#a8d8452cfaafd9798b83cd1c2b98f94b3", null ]
];