var class_p_v_quad =
[
    [ "PVQuad", "class_p_v_quad.html#a095fad8784772473c98761a1bd89298e", null ],
    [ "get_coefs", "class_p_v_quad.html#ae170d1c8ddbdfff345c82d47f9edd6be", null ],
    [ "get_eigenvalues", "class_p_v_quad.html#a1a48edd350ed644195f5ea4015ef0ae7", null ],
    [ "get_integral", "class_p_v_quad.html#a49787972b3582fa7a7381f5950f9e1de", null ],
    [ "get_orthopolies", "class_p_v_quad.html#a0c6a944809ea9310efbc2fbaf12ef44b", null ],
    [ "mu", "class_p_v_quad.html#a29dbb3c795e8c0b6fb5b6afbe7ad46a4", null ],
    [ "norm", "class_p_v_quad.html#a52945b58504890fed1b46c12e27c98c1", null ],
    [ "ovec", "class_p_v_quad.html#a74cbf305ccedf272dc970c203e548b09", null ],
    [ "q0", "class_p_v_quad.html#a07735c01d10110c3f960cc6af278f44a", null ],
    [ "return_coefs", "class_p_v_quad.html#adb21e8b069ab225c94548ba55e2cda6b", null ],
    [ "return_coefs", "class_p_v_quad.html#ad74c9cc07ebf1eba620ff6dc733f8039", null ],
    [ "return_pv", "class_p_v_quad.html#abc9fd5c6951826a64b5b37e69dd90326", null ],
    [ "return_pv", "class_p_v_quad.html#a6dc1e4f8a86b0761933cdd13f57645ad", null ],
    [ "return_pv", "class_p_v_quad.html#a9fe808485769aa0a436dd1a2873be83e", null ],
    [ "root", "class_p_v_quad.html#a90aa4b304efe79aea1ed458287b9001d", null ],
    [ "chrs", "class_p_v_quad.html#af7b4a4d4f06ee8e02141536e584a4491", null ],
    [ "coefs", "class_p_v_quad.html#a106d8e698819294961de07675949b846", null ],
    [ "eign", "class_p_v_quad.html#aad6da2d8e9c412b84b482f932dd072f4", null ],
    [ "n", "class_p_v_quad.html#a400c2aefe2bc62751650cdff09bfb504", null ],
    [ "nrml", "class_p_v_quad.html#a01988f4720a259ec5de1e9273013b0ca", null ],
    [ "poly", "class_p_v_quad.html#aee95aac1dff74f5550be1ca0f4f820d6", null ]
];