var struct_collide_ion_1_1_pord_1_1_es =
[
    [ "Es", "struct_collide_ion_1_1_pord_1_1_es.html#a4056f82d008954696008f487c7626eac", null ],
    [ "eta", "struct_collide_ion_1_1_pord_1_1_es.html#a0ecafe2083df158f41413a76af0b73c1", null ],
    [ "ke0", "struct_collide_ion_1_1_pord_1_1_es.html#a1f2331e70921203e0ea2f551016d0b20", null ],
    [ "ke1", "struct_collide_ion_1_1_pord_1_1_es.html#a3d0bc6a177b70daf5e904cd54387207d", null ],
    [ "KEe", "struct_collide_ion_1_1_pord_1_1_es.html#a3523e512c28ef42f6fe41a6fc699c66d", null ],
    [ "KEi", "struct_collide_ion_1_1_pord_1_1_es.html#ad7a32469fc9dab3ae5a39a6430d3841d", null ],
    [ "KEw", "struct_collide_ion_1_1_pord_1_1_es.html#a868bb215e8bfb61988ad503fa4ba58d9", null ]
];