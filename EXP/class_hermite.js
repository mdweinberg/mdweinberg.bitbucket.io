var class_hermite =
[
    [ "Hermite", "class_hermite.html#a65f7a86f19d866306d86c4dd26124dfa", null ],
    [ "coef1", "class_hermite.html#a20584862f32b4dc3cea3a7ad8ca5aad8", null ],
    [ "coef2", "class_hermite.html#a2d8ad699c5c538c2b8d86bc564db4767", null ],
    [ "coef3", "class_hermite.html#a811b64bf95278e44cedafe8ff7034842", null ],
    [ "coef4", "class_hermite.html#a91b9ce2aee7da32a99010969f91f17e0", null ],
    [ "f0", "class_hermite.html#aa1996089eb7171792d8d7083fd643e8e", null ],
    [ "f1", "class_hermite.html#a83fa92a38f026b34788778489d83ed72", null ],
    [ "h", "class_hermite.html#af5882ed3ef371ac816f721a4d5dbcced", null ],
    [ "w", "class_hermite.html#a6d33ef9a7642dae5a29263e2de346669", null ]
];