var biorth_8h =
[
    [ "AxiSymBiorth", "class_axi_sym_biorth.html", "class_axi_sym_biorth" ],
    [ "Biorth", "class_biorth.html", "class_biorth" ],
    [ "BiorthGrid", "class_biorth_grid.html", "class_biorth_grid" ],
    [ "BSSphere", "class_b_s_sphere.html", "class_b_s_sphere" ],
    [ "CBSphere", "class_c_b_sphere.html", "class_c_b_sphere" ],
    [ "HQSphere", "class_h_q_sphere.html", "class_h_q_sphere" ],
    [ "BiorthFcts3d", "biorth_8h.html#a021b8d5d0de968b2a533d342c41af77a", [
      [ "bessel", "biorth_8h.html#a021b8d5d0de968b2a533d342c41af77aa750b3d98ea0866a01b86f76b69bdada4", null ],
      [ "clutton_brock", "biorth_8h.html#a021b8d5d0de968b2a533d342c41af77aa40dfd50bb32e4cbe1bf99fd2d5c4178f", null ],
      [ "hernquist", "biorth_8h.html#a021b8d5d0de968b2a533d342c41af77aa2152559d6a29a11bf6a18674390defb7", null ],
      [ "sturm", "biorth_8h.html#a021b8d5d0de968b2a533d342c41af77aab9f6d1ce6accee067d448bc541b53607", null ]
    ] ],
    [ "ScalarType", "biorth_8h.html#afc6e90026099c08093d2dba767548f3c", [
      [ "density", "biorth_8h.html#afc6e90026099c08093d2dba767548f3ca22ebfd60994ac594c00a8e367372d092", null ],
      [ "potential", "biorth_8h.html#afc6e90026099c08093d2dba767548f3ca3411cb34f943955f072e60575ed46756", null ]
    ] ],
    [ "scalar_prod", "biorth_8h.html#a2d147d47d0a9ba891ea13ea70766ddd3", null ],
    [ "ScalarTypeName", "biorth_8h.html#a5b231a46c1f7fdcf238561816be223dd", null ],
    [ "BiorthFcts3dName", "biorth_8h.html#a4bf098c071f4e81fdbf5105aa72990da", null ],
    [ "rcsid_biorth", "biorth_8h.html#a1074be0704b06013867cacf515e18afc", null ]
];