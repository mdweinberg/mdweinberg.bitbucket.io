var psp__dist_l_8py =
[
    [ "FitType", "classpsp__dist_l_1_1_fit_type.html", "classpsp__dist_l_1_1_fit_type" ],
    [ "PlotType", "classpsp__dist_l_1_1_plot_type.html", "classpsp__dist_l_1_1_plot_type" ],
    [ "func1", "psp__dist_l_8py.html#a408d0419d68d437876f99bfc72079d8a", null ],
    [ "func2", "psp__dist_l_8py.html#aabdfe68777defe5398c29fd23036d4b3", null ],
    [ "main", "psp__dist_l_8py.html#a336c7a6d7a226782d3c1090329f6ebf8", null ],
    [ "plot_data", "psp__dist_l_8py.html#a9dcb899e86a4a9e1ad4a331ebb191625", null ],
    [ "a0", "psp__dist_l_8py.html#ac782a2f714394d77a8cf2c92cd84b609", null ],
    [ "amu", "psp__dist_l_8py.html#a878053d718e97d7b5c1bfb9cbd6210b1", null ],
    [ "b0", "psp__dist_l_8py.html#ab5d3df8d6df99d3f3fa12e21c327373d", null ],
    [ "eV", "psp__dist_l_8py.html#ac553490f5686eb1425e317e19d6b2455", null ],
    [ "Lunit", "psp__dist_l_8py.html#a7449a58a66021c663aa43d7adc78cd20", null ],
    [ "m_H", "psp__dist_l_8py.html#a3926dd7932f60f52331afc0d75e51972", null ],
    [ "m_He", "psp__dist_l_8py.html#acb81e81dd5d9f9a26fdeb73b729d3ae4", null ],
    [ "Mu_e", "psp__dist_l_8py.html#aa3a1004de4945a66fe382d537a46cf88", null ],
    [ "Mu_i", "psp__dist_l_8py.html#ab78d3c0b38d94422e1e5524aead5d510", null ],
    [ "Munit", "psp__dist_l_8py.html#a620ea26d00e342f4eca9b3c609479b6d", null ],
    [ "ndatr", "psp__dist_l_8py.html#aab709d1105927ce269f040dc630f50b7", null ],
    [ "Tunit", "psp__dist_l_8py.html#aab265810b6700305bd6bce922ac639f7", null ],
    [ "Vunit", "psp__dist_l_8py.html#acc658877fb20a65b6065123f0dcf08f0", null ],
    [ "X_H", "psp__dist_l_8py.html#ae6850c09873c69ce89189785ea277f4f", null ],
    [ "X_He", "psp__dist_l_8py.html#aabad00775ddb66ecbd5e85b179ecb9fb", null ]
];