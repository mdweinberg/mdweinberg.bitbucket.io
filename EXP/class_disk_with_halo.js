var class_disk_with_halo =
[
    [ "DiskWithHalo", "class_disk_with_halo.html#af2d9d93b9ff0164985f33db2f666ae0a", null ],
    [ "DiskWithHalo", "class_disk_with_halo.html#a6c137d07dc2d3f3cf866f66d35483e50", null ],
    [ "DiskWithHalo", "class_disk_with_halo.html#acd9e6c29b53f51e98397c52978c66241", null ],
    [ "d2fde2", "class_disk_with_halo.html#a2c18538f343898872f79288f58e3c425", null ],
    [ "dfde", "class_disk_with_halo.html#ac94b4ab6605a140f6a7de6ccffd90287", null ],
    [ "dfdl", "class_disk_with_halo.html#ac64b0ab81c8eea18fb29a20d88adb343", null ],
    [ "distf", "class_disk_with_halo.html#ac24a9b3008460c2d83aec008ab70619c", null ],
    [ "get_density", "class_disk_with_halo.html#a90dc033f414be9f3c41ee0c0fad633bc", null ],
    [ "get_dpot", "class_disk_with_halo.html#a298f24fcd27cf5f837c18e52eafdc106", null ],
    [ "get_dpot2", "class_disk_with_halo.html#a83af39a3f1f0eaaae16e9d6a54e57de3", null ],
    [ "get_mass", "class_disk_with_halo.html#a145b49523bfd5c4884a25a42002007a7", null ],
    [ "get_max_radius", "class_disk_with_halo.html#a825779fc157962845f20bf193610d6b3", null ],
    [ "get_min_radius", "class_disk_with_halo.html#ad1e546938d709da4c185adb617996734", null ],
    [ "get_pot", "class_disk_with_halo.html#abb298a3eff91de4660de03ec7e8c8a75", null ],
    [ "get_pot_dpot", "class_disk_with_halo.html#a522ab0472483200d97e5d9c18f437116", null ],
    [ "d", "class_disk_with_halo.html#a56ed719845b11f2c6f9bacca4ce1d5c3", null ],
    [ "h", "class_disk_with_halo.html#a1337d167b138c0e7ce0ffabf447a0b80", null ]
];