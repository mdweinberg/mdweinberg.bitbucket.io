var class_plummer =
[
    [ "force", "class_plummer.html#a099579a8bdf39305b9b961a3f4802af5", null ],
    [ "get_M", "class_plummer.html#a2876fc4e3134198461a375818cc817ed", null ],
    [ "get_r0", "class_plummer.html#a1ad6daa45ed07c9edc735bf174f8ef15", null ],
    [ "potential", "class_plummer.html#aa364273df33eb218c5603dbcf22e46fc", null ],
    [ "set_M", "class_plummer.html#a16ea90e9a9319a175b68a933d870ff81", null ],
    [ "set_Plummer", "class_plummer.html#a17dd3422260a716f89872bc7ab2bed99", null ],
    [ "set_r0", "class_plummer.html#aaddf34566682ddde31947be6bfc12837", null ],
    [ "G", "class_plummer.html#aa2a1b4a1c4cb0841a0ae1c64e744e4fe", null ],
    [ "GM", "class_plummer.html#abcb005aba1f32212cc20cca5a0c50902", null ],
    [ "M", "class_plummer.html#aa79c6cbb4b15e357554d3797c7a3bd5b", null ],
    [ "r0", "class_plummer.html#ac7df5e8840e6ba5a1bc8e5b3c0c58590", null ],
    [ "r2", "class_plummer.html#a52b35f16b516240494ffc0e08bb5da12", null ],
    [ "r3", "class_plummer.html#a13ea06b0837614fb5c607248294d9adb", null ]
];