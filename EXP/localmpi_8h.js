var localmpi_8h =
[
    [ "local_init_mpi", "localmpi_8h.html#a39ac8061c2faea3faf09bc24a60c55c0", null ],
    [ "mpi_report_location", "localmpi_8h.html#a44266dec80daeaa903107fd57cd174c3", null ],
    [ "MPI_COMM_SLAVE", "localmpi_8h.html#a5b4aeea2371e3088261bc89b7ed906e1", null ],
    [ "mpi_debug", "localmpi_8h.html#a4c2b696656dc213a7401e75968b9689c", null ],
    [ "myid", "localmpi_8h.html#a20705b4372a9940d34a164bae541be1f", null ],
    [ "numprocs", "localmpi_8h.html#a31bfe068640b411bea3df0d69177ded5", null ],
    [ "proc_namelen", "localmpi_8h.html#a604039d7c00e3589fc7894ede2ef7014", null ],
    [ "processor_name", "localmpi_8h.html#af3cbc7cca358c8795163f3349c842aaf", null ],
    [ "slave_group", "localmpi_8h.html#ab3412a3f53f2f1eedba7e7bf0205cbe6", null ],
    [ "slaves", "localmpi_8h.html#a8ddabe2fc193075e71ab0136c1c89177", null ],
    [ "world_group", "localmpi_8h.html#a6f96dd1ddda9dd5a0f108be09635122f", null ]
];