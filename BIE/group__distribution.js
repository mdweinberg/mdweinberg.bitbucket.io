var group__distribution =
[
    [ "Prior distributions", "group__prior.html", "group__prior" ],
    [ "BetaRDist", "class_b_i_e_1_1_beta_r_dist.html", [
      [ "BetaRDist", "class_b_i_e_1_1_beta_r_dist.html#a8ceefed10b6a8c8710638be433bf2437", null ],
      [ "BetaRDist", "class_b_i_e_1_1_beta_r_dist.html#a123e3e096d1faff929d51b05aea77a61", null ],
      [ "BetaRDist", "class_b_i_e_1_1_beta_r_dist.html#a47dee4987b766b6b743e82e0830d0f42", null ],
      [ "BetaRDist", "class_b_i_e_1_1_beta_r_dist.html#a17de1465761b38decf9fdc08ecb1b5c1", null ],
      [ "BetaRDist", "class_b_i_e_1_1_beta_r_dist.html#a991e0b1b3cefe27b3a8a83c3765c53a2", null ],
      [ "~BetaRDist", "class_b_i_e_1_1_beta_r_dist.html#acdd5052a9f07be93ebc7f789cd224cb8", null ],
      [ "CDF", "class_b_i_e_1_1_beta_r_dist.html#adec43c795749a32aeb18ea526e87e843", null ],
      [ "computeNorm", "class_b_i_e_1_1_beta_r_dist.html#ad716a170da221a06a40cbc29b484e019", null ],
      [ "logPDF", "class_b_i_e_1_1_beta_r_dist.html#a78d3562a239ac302f00e5fedb8356c57", null ],
      [ "lower", "class_b_i_e_1_1_beta_r_dist.html#a861ed829c1e6042ce661a4b7e8d889ae", null ],
      [ "Mean", "class_b_i_e_1_1_beta_r_dist.html#af2ddf71ce8d23e5fa203bc7caa347487", null ],
      [ "Moments", "class_b_i_e_1_1_beta_r_dist.html#a3c39bd3568c4a692d24a81f4b9b3ff8e", null ],
      [ "New", "class_b_i_e_1_1_beta_r_dist.html#ae85e9fd42aa71e26ce28a792a1dda29b", null ],
      [ "PDF", "class_b_i_e_1_1_beta_r_dist.html#a455a7f9c470788b3348f53989a85b6e9", null ],
      [ "Sample", "class_b_i_e_1_1_beta_r_dist.html#ab82b060c4c2beddb8c3040b094c9f4c4", null ],
      [ "serialize", "class_b_i_e_1_1_beta_r_dist.html#a906d8359740a505afd8d4be76be3ac83", null ],
      [ "StdDev", "class_b_i_e_1_1_beta_r_dist.html#a01d075b80cfd6f58966128bf3527ecff", null ],
      [ "upper", "class_b_i_e_1_1_beta_r_dist.html#a2f0337f2f4d424dc48363b038f13b24e", null ],
      [ "boost::serialization::access", "class_b_i_e_1_1_beta_r_dist.html#ac98d07dd8f7b70e16ccb9a01abf56b9c", null ],
      [ "center", "class_b_i_e_1_1_beta_r_dist.html#ad15887db90d0e3f90d93ad17a21e6f89", null ],
      [ "dim", "class_b_i_e_1_1_beta_r_dist.html#aa48ad55cf71150d071efb8d0808e1b7e", null ],
      [ "limits", "class_b_i_e_1_1_beta_r_dist.html#a4597b45b66e0f9925b9b89f81193953e", null ],
      [ "maxcdf", "class_b_i_e_1_1_beta_r_dist.html#a1ed79005707e9febb23472e74b954024", null ],
      [ "mincdf", "class_b_i_e_1_1_beta_r_dist.html#a7489830807ead406401f84844520f0a9", null ],
      [ "norm", "class_b_i_e_1_1_beta_r_dist.html#a44fa92658c02ae5f0a6150e9f40dfd8d", null ],
      [ "nrml", "class_b_i_e_1_1_beta_r_dist.html#aef86231081d350fe17e9207cc9d4aab4", null ],
      [ "scale", "class_b_i_e_1_1_beta_r_dist.html#a63500869beae5fa2298c44ca9e6ce059", null ],
      [ "shape", "class_b_i_e_1_1_beta_r_dist.html#ac4dddbec2f3b724e6b49edcde9cb8f74", null ],
      [ "unit", "class_b_i_e_1_1_beta_r_dist.html#a37cfa30ba4603b88cac0c4211dacc72a", null ]
    ] ],
    [ "BinnedDistribution", "class_b_i_e_1_1_binned_distribution.html", [
      [ "~BinnedDistribution", "class_b_i_e_1_1_binned_distribution.html#a22aaa86e6832ceed94b3e73d9b37f869", null ],
      [ "BinnedDistribution", "class_b_i_e_1_1_binned_distribution.html#a39bdffb6a5d4f1fdb40fac922a341049", null ],
      [ "CDF", "class_b_i_e_1_1_binned_distribution.html#a5a7122092e74f872a711487caee45b78", null ],
      [ "getHigh", "class_b_i_e_1_1_binned_distribution.html#ae3a90d19b60eb83433fca7a1b592bd43", null ],
      [ "getLow", "class_b_i_e_1_1_binned_distribution.html#a45cc2ae1cd2769bc076165170362071a", null ],
      [ "New", "class_b_i_e_1_1_binned_distribution.html#a22c6af7aafcf26343daf8339aa2b8a7f", null ],
      [ "serialize", "class_b_i_e_1_1_binned_distribution.html#a5e33e8e34da8abf1eb5e140533176c40", null ],
      [ "boost::serialization::access", "class_b_i_e_1_1_binned_distribution.html#ac98d07dd8f7b70e16ccb9a01abf56b9c", null ]
    ] ],
    [ "CauchyDist", "class_b_i_e_1_1_cauchy_dist.html", [
      [ "CauchyDist", "class_b_i_e_1_1_cauchy_dist.html#adedd7e774cb7b593491b6053cfd36ecc", null ],
      [ "CauchyDist", "class_b_i_e_1_1_cauchy_dist.html#a438377264908a4b4005e2c14a2682285", null ],
      [ "CauchyDist", "class_b_i_e_1_1_cauchy_dist.html#a30d5150307997f6586517a7bb5c68071", null ],
      [ "CauchyDist", "class_b_i_e_1_1_cauchy_dist.html#a3cbf2d8033219d5c20feca23bb6c00e6", null ],
      [ "CauchyDist", "class_b_i_e_1_1_cauchy_dist.html#ae2944b00c96e818b41a139c84491c07c", null ],
      [ "~CauchyDist", "class_b_i_e_1_1_cauchy_dist.html#a4ce08e21cdf3da85e6cdb11c5ddfa85c", null ],
      [ "CDF", "class_b_i_e_1_1_cauchy_dist.html#a04918932bdbd8edbeeeba4a9d6ed3c8c", null ],
      [ "logPDF", "class_b_i_e_1_1_cauchy_dist.html#a5525e8b33d256848cfc3f2296ce7e210", null ],
      [ "lower", "class_b_i_e_1_1_cauchy_dist.html#aafeff684386013465a586864e5418d1d", null ],
      [ "Mean", "class_b_i_e_1_1_cauchy_dist.html#aef76bb4aad01017b3762f88b2e3b7f06", null ],
      [ "Moments", "class_b_i_e_1_1_cauchy_dist.html#a62e6aa358cf575f94d0f26e420e3966c", null ],
      [ "New", "class_b_i_e_1_1_cauchy_dist.html#ae8f0cc7fa5726667c7b30dacce8fb21c", null ],
      [ "PDF", "class_b_i_e_1_1_cauchy_dist.html#a8fc7d1398080e99dad06c5065975d42f", null ],
      [ "Sample", "class_b_i_e_1_1_cauchy_dist.html#ab235c542a8831463c3d4028afe15c51e", null ],
      [ "serialize", "class_b_i_e_1_1_cauchy_dist.html#a1a6bef9a29aa6f784c76eb7182ef1d25", null ],
      [ "StdDev", "class_b_i_e_1_1_cauchy_dist.html#a5267ff5dfe619ca76430c4f5373235bf", null ],
      [ "upper", "class_b_i_e_1_1_cauchy_dist.html#a6b6567e9346648c35178e9a9a13ae779", null ],
      [ "boost::serialization::access", "class_b_i_e_1_1_cauchy_dist.html#ac98d07dd8f7b70e16ccb9a01abf56b9c", null ],
      [ "cauchy", "class_b_i_e_1_1_cauchy_dist.html#a1c98706b57d7b6fa07826558969b098a", null ],
      [ "center", "class_b_i_e_1_1_cauchy_dist.html#ab47a79adf96e4aafa2ea0cb161e7b7c0", null ],
      [ "maxcdf", "class_b_i_e_1_1_cauchy_dist.html#aefe423d2b6fbcc69ab2bed86b939252c", null ],
      [ "mincdf", "class_b_i_e_1_1_cauchy_dist.html#a270727efffddb5ee9958f10cc72f14c2", null ],
      [ "norm", "class_b_i_e_1_1_cauchy_dist.html#aeb292727f6e26551df6a89930a193f44", null ],
      [ "scale", "class_b_i_e_1_1_cauchy_dist.html#aa08e09d4b8e15625b35ab451ce4a471c", null ],
      [ "vmax", "class_b_i_e_1_1_cauchy_dist.html#a2ed945cff8e0f1d4ed47a9a465a40b44", null ],
      [ "vmin", "class_b_i_e_1_1_cauchy_dist.html#aad043149e44fd20f260aa26a0136b33f", null ]
    ] ],
    [ "Dirichlet", "class_b_i_e_1_1_dirichlet.html", [
      [ "Dirichlet", "class_b_i_e_1_1_dirichlet.html#a13dca34988adfaaa9e27400be1d25b80", null ],
      [ "Dirichlet", "class_b_i_e_1_1_dirichlet.html#a9702dabb5687072fe28fc447c987d84b", null ],
      [ "Dirichlet", "class_b_i_e_1_1_dirichlet.html#a02fbefdc07dab205254da0212f843a12", null ],
      [ "~Dirichlet", "class_b_i_e_1_1_dirichlet.html#a73eef67d13ae5ec8ad5e9318bd846733", null ],
      [ "logPDF", "class_b_i_e_1_1_dirichlet.html#a6f349a70a96f0793be364fa126e23677", null ],
      [ "lower", "class_b_i_e_1_1_dirichlet.html#ae3d20f3d5e030796888809ba68e04263", null ],
      [ "Mean", "class_b_i_e_1_1_dirichlet.html#a9cd49e41c0a6ed15d9e2018479b7bcd8", null ],
      [ "Moments", "class_b_i_e_1_1_dirichlet.html#a7e6c1b2e8cdffcdc3a06f04f4b53cb09", null ],
      [ "New", "class_b_i_e_1_1_dirichlet.html#a2e528459d95d2a83238d9df2bb52cee2", null ],
      [ "PDF", "class_b_i_e_1_1_dirichlet.html#a786f4b911d92b56a95941ad837faa444", null ],
      [ "Sample", "class_b_i_e_1_1_dirichlet.html#a3fc675c65612d183417ecd82a907bdbf", null ],
      [ "serialize", "class_b_i_e_1_1_dirichlet.html#a5b172aef9caa83fb5f691a4615436587", null ],
      [ "StdDev", "class_b_i_e_1_1_dirichlet.html#afe07477fe33bd561812dd863735e0c57", null ],
      [ "upper", "class_b_i_e_1_1_dirichlet.html#a6bd3cde626aca2af19ff1b0c27952135", null ],
      [ "boost::serialization::access", "class_b_i_e_1_1_dirichlet.html#ac98d07dd8f7b70e16ccb9a01abf56b9c", null ],
      [ "A", "class_b_i_e_1_1_dirichlet.html#a61060c838f06ae5310ae7b338f982123", null ],
      [ "gengam", "class_b_i_e_1_1_dirichlet.html#a1b3c3e951c919f000539c276e004eead", null ],
      [ "n", "class_b_i_e_1_1_dirichlet.html#a2b1019173447ee0aa12b1f620ecf8226", null ],
      [ "pdf_fac", "class_b_i_e_1_1_dirichlet.html#a150190b3946bc5a4100818fae3a3cb91", null ],
      [ "sumA", "class_b_i_e_1_1_dirichlet.html#a1fccef2a52afebd5782f746b79e3d47c", null ],
      [ "varA", "class_b_i_e_1_1_dirichlet.html#a9c0a89cfc7d587ea30e942339e8e7fc2", null ]
    ] ],
    [ "Distribution", "class_b_i_e_1_1_distribution.html", [
      [ "ProposalType", "class_b_i_e_1_1_distribution.html#a4dfca70c48e18e901cd60ff273dbe430", [
        [ "UniformAdd", "class_b_i_e_1_1_distribution.html#a4dfca70c48e18e901cd60ff273dbe430a1b82d65c75b7eb15f9fef8a7307e5acc", null ],
        [ "UniformMult", "class_b_i_e_1_1_distribution.html#a4dfca70c48e18e901cd60ff273dbe430aa720831ff628e98da99f10234b9bb38b", null ],
        [ "NormalAdd", "class_b_i_e_1_1_distribution.html#a4dfca70c48e18e901cd60ff273dbe430ab623867abc3694e61a876a46e810e971", null ],
        [ "NormalMult", "class_b_i_e_1_1_distribution.html#a4dfca70c48e18e901cd60ff273dbe430ac71ab3043d0e568bfbfd282927a4e36d", null ],
        [ "Undefined", "class_b_i_e_1_1_distribution.html#a4dfca70c48e18e901cd60ff273dbe430a5bc00e09a70446240fad65b7f00ea92d", null ]
      ] ],
      [ "Distribution", "class_b_i_e_1_1_distribution.html#ad77bf8cec24bee79d57985f79ddd7b06", null ],
      [ "~Distribution", "class_b_i_e_1_1_distribution.html#ae5d6a0d23210ab1df046ca9e104bc166", null ],
      [ "CDF", "class_b_i_e_1_1_distribution.html#a959d44248c0de1c1fce7d8198514167e", null ],
      [ "Dim", "class_b_i_e_1_1_distribution.html#a2797c79693c3430e2a9f08bef6064e08", null ],
      [ "logPDF", "class_b_i_e_1_1_distribution.html#aed25244e82e3b99de4b85bddf9f6f2a3", null ],
      [ "lower", "class_b_i_e_1_1_distribution.html#a7d63404a9aa7e20801297d32cbf3ee09", null ],
      [ "Mean", "class_b_i_e_1_1_distribution.html#aa95b0e4965fa0530e0866d88c4b3d5d9", null ],
      [ "Moments", "class_b_i_e_1_1_distribution.html#afff916a32053572a73f3736696698d23", null ],
      [ "New", "class_b_i_e_1_1_distribution.html#a3fd392534c46961d2f5d67b4f4549014", null ],
      [ "PDF", "class_b_i_e_1_1_distribution.html#aa65659a876e8a921b0a978ea46bd0c60", null ],
      [ "Sample", "class_b_i_e_1_1_distribution.html#aa62a818aae0678e2638f0681afa20ebb", null ],
      [ "serialize", "class_b_i_e_1_1_distribution.html#a1315f9f83db4b138642eeb1733f358e3", null ],
      [ "setWidth", "class_b_i_e_1_1_distribution.html#a6f217e7873e5977a00a51bd0dcfde8a3", null ],
      [ "StdDev", "class_b_i_e_1_1_distribution.html#a11e9cf93dbe6d15b1f767cf2ac4bff89", null ],
      [ "Type", "class_b_i_e_1_1_distribution.html#a098fdf4eea7dece81a5e5530aebae257", null ],
      [ "upper", "class_b_i_e_1_1_distribution.html#a6be85a5b2b8e20d5a55f27d94eb4c401", null ],
      [ "boost::serialization::access", "class_b_i_e_1_1_distribution.html#ac98d07dd8f7b70e16ccb9a01abf56b9c", null ]
    ] ],
    [ "ErfDist", "class_b_i_e_1_1_erf_dist.html", [
      [ "ErfDist", "class_b_i_e_1_1_erf_dist.html#a53205cb99186f6b51b10b4f054946181", null ],
      [ "ErfDist", "class_b_i_e_1_1_erf_dist.html#a8beeb8b1883b6f6af19c833582d1cceb", null ],
      [ "ErfDist", "class_b_i_e_1_1_erf_dist.html#a56a197bc2e34adb2afa752504e65a8a9", null ],
      [ "~ErfDist", "class_b_i_e_1_1_erf_dist.html#a4b330ce68770b3510565ab7de240853c", null ],
      [ "CDF", "class_b_i_e_1_1_erf_dist.html#ac616267d76b540ee74198ba651448981", null ],
      [ "generate", "class_b_i_e_1_1_erf_dist.html#a8f991e88e8770ae998756dfbb9d0a0d5", null ],
      [ "logPDF", "class_b_i_e_1_1_erf_dist.html#a11a2bfbfb22637f446c0010cb9280b57", null ],
      [ "lower", "class_b_i_e_1_1_erf_dist.html#a9cceb4a37169840f09cb2d90f94f3ee8", null ],
      [ "Mean", "class_b_i_e_1_1_erf_dist.html#aea9a961db62391485e4f9a8bf5108819", null ],
      [ "Moments", "class_b_i_e_1_1_erf_dist.html#ab765b555369b4aaab14a6b6f8bd4380a", null ],
      [ "New", "class_b_i_e_1_1_erf_dist.html#afda00909cc9c95d7605c5cf7f37d2b7b", null ],
      [ "PDF", "class_b_i_e_1_1_erf_dist.html#ab1b2399a7afd0637c6ff57378b2834b3", null ],
      [ "Sample", "class_b_i_e_1_1_erf_dist.html#a24b73d9d59455189b343456e360960e8", null ],
      [ "serialize", "class_b_i_e_1_1_erf_dist.html#a397d9c07d81d1195e474c43e227e112d", null ],
      [ "StdDev", "class_b_i_e_1_1_erf_dist.html#a1e99ad852429d484992ba6970495faff", null ],
      [ "upper", "class_b_i_e_1_1_erf_dist.html#ac5bfc94d86a60d4c97f508954abcf362", null ],
      [ "boost::serialization::access", "class_b_i_e_1_1_erf_dist.html#ac98d07dd8f7b70e16ccb9a01abf56b9c", null ],
      [ "a", "class_b_i_e_1_1_erf_dist.html#afe415cc903d46adef7f482e7495b4146", null ],
      [ "b", "class_b_i_e_1_1_erf_dist.html#aee082184f01b7db9b27625b0f9f3b757", null ],
      [ "c", "class_b_i_e_1_1_erf_dist.html#a110bc7f6f1a8fe5094a30193526cb6b0", null ],
      [ "d", "class_b_i_e_1_1_erf_dist.html#a02b063e63eac8fe9e1a49f3dbc22ff71", null ],
      [ "Nspc", "class_b_i_e_1_1_erf_dist.html#aee57489febba9fe7cbc2efab06e49311", null ],
      [ "Nwid", "class_b_i_e_1_1_erf_dist.html#a49e5c26e558fe11fc8881a16a349439b", null ],
      [ "unit", "class_b_i_e_1_1_erf_dist.html#ac7df7ccf61fa71182101e9c0e60c5aca", null ]
    ] ],
    [ "GammaDist", "class_b_i_e_1_1_gamma_dist.html", [
      [ "GammaDist", "class_b_i_e_1_1_gamma_dist.html#a5123f958016bccdce73df0dc835a8349", null ],
      [ "GammaDist", "class_b_i_e_1_1_gamma_dist.html#a741ce6245fa6c5acd83c82c15df70f3b", null ],
      [ "~GammaDist", "class_b_i_e_1_1_gamma_dist.html#aeb70c57abd61a2ead092f52242db33a4", null ],
      [ "CDF", "class_b_i_e_1_1_gamma_dist.html#a80c974ce38f3ae99caeed7ee560358d3", null ],
      [ "logPDF", "class_b_i_e_1_1_gamma_dist.html#aa77eb1aba7c05db0b6f8f83c235e5736", null ],
      [ "lower", "class_b_i_e_1_1_gamma_dist.html#aff76bc673b038827a83b51d2e689f573", null ],
      [ "Mean", "class_b_i_e_1_1_gamma_dist.html#a7463d78d1a025a7625618b96d785a584", null ],
      [ "Moments", "class_b_i_e_1_1_gamma_dist.html#a7e11fc289d952f50f42a3e2a03806350", null ],
      [ "New", "class_b_i_e_1_1_gamma_dist.html#aff0ba3b62072cbe74b0192f08c56d7d0", null ],
      [ "PDF", "class_b_i_e_1_1_gamma_dist.html#ae6f2a5a6b7954da3b5eb7125a3aa53f0", null ],
      [ "Sample", "class_b_i_e_1_1_gamma_dist.html#a9f327c24859501821d862e52ebed4de0", null ],
      [ "serialize", "class_b_i_e_1_1_gamma_dist.html#aa0a4358002ea36c45630be6ba28c0226", null ],
      [ "StdDev", "class_b_i_e_1_1_gamma_dist.html#aad8f1ccc8015aa6b9b4e58b909389ce0", null ],
      [ "upper", "class_b_i_e_1_1_gamma_dist.html#a00dd0895621631447a6f91b8880da69a", null ],
      [ "boost::serialization::access", "class_b_i_e_1_1_gamma_dist.html#ac98d07dd8f7b70e16ccb9a01abf56b9c", null ],
      [ "k", "class_b_i_e_1_1_gamma_dist.html#a9dec78d768eff2fe53ccfe34c6920e9a", null ],
      [ "maxcdf", "class_b_i_e_1_1_gamma_dist.html#a4f448e795cd6430ea18a4b59f1e5e4d1", null ],
      [ "mean", "class_b_i_e_1_1_gamma_dist.html#a2293d5358c7593cc48700943f1e80072", null ],
      [ "mincdf", "class_b_i_e_1_1_gamma_dist.html#a205ef13902f195a65acb71d93922bb2f", null ],
      [ "norm", "class_b_i_e_1_1_gamma_dist.html#ad7a2d0d1950d85d61430a23af28a282b", null ],
      [ "t", "class_b_i_e_1_1_gamma_dist.html#a46a417bcf5b735a671c11ab3968421bb", null ],
      [ "ugam", "class_b_i_e_1_1_gamma_dist.html#aea657707bf8724ce1ad30944eaaf6781", null ],
      [ "var", "class_b_i_e_1_1_gamma_dist.html#a1fe192b0ec5cd2f5edb080aa0a7258b3", null ],
      [ "vmax", "class_b_i_e_1_1_gamma_dist.html#a03de68d8ac2e739ba4a8a9a20fb96498", null ],
      [ "vmin", "class_b_i_e_1_1_gamma_dist.html#ada59e093ac4861e18173b4fc6238420a", null ]
    ] ],
    [ "InverseGammaDist", "class_b_i_e_1_1_inverse_gamma_dist.html", [
      [ "InverseGammaDist", "class_b_i_e_1_1_inverse_gamma_dist.html#a3a7687f27c838a82854a67cf865755d7", null ],
      [ "InverseGammaDist", "class_b_i_e_1_1_inverse_gamma_dist.html#ad47bc76f2fcd0239e2ac8b1a94dc4d33", null ],
      [ "~InverseGammaDist", "class_b_i_e_1_1_inverse_gamma_dist.html#ada90bf359f58a2b52430c81b6ac7a89b", null ],
      [ "CDF", "class_b_i_e_1_1_inverse_gamma_dist.html#a081e49aa52507e0bbc27db5e0db03418", null ],
      [ "logPDF", "class_b_i_e_1_1_inverse_gamma_dist.html#a297b24e84b831196089ff2ab0017daf3", null ],
      [ "lower", "class_b_i_e_1_1_inverse_gamma_dist.html#ae309c9a8379ac5e86e5ae29d2c1c1e2e", null ],
      [ "Mean", "class_b_i_e_1_1_inverse_gamma_dist.html#ad12802c4e5d968fc269b20353544e35c", null ],
      [ "Moments", "class_b_i_e_1_1_inverse_gamma_dist.html#a5a83d60a73b77d5fa134f91afa8e631c", null ],
      [ "New", "class_b_i_e_1_1_inverse_gamma_dist.html#aa0a2105a4bd4528044b082efddc17ff5", null ],
      [ "PDF", "class_b_i_e_1_1_inverse_gamma_dist.html#ae05e7ec53eb967d4f687228fe1b055a0", null ],
      [ "Sample", "class_b_i_e_1_1_inverse_gamma_dist.html#a46f01645b193dd31d29ed60382cc99e6", null ],
      [ "serialize", "class_b_i_e_1_1_inverse_gamma_dist.html#a5ab403dc943087ba851637f2492ab3fa", null ],
      [ "StdDev", "class_b_i_e_1_1_inverse_gamma_dist.html#a064e508266cbcc72ca2fe57d636a65c7", null ],
      [ "upper", "class_b_i_e_1_1_inverse_gamma_dist.html#a38b91f46a2a795059a0fb13b7645dfd4", null ],
      [ "boost::serialization::access", "class_b_i_e_1_1_inverse_gamma_dist.html#ac98d07dd8f7b70e16ccb9a01abf56b9c", null ],
      [ "alpha", "class_b_i_e_1_1_inverse_gamma_dist.html#a1718d5045ee9b90ed61a22abd67d9445", null ],
      [ "beta", "class_b_i_e_1_1_inverse_gamma_dist.html#abe1100055ccc516114f20029c82f0c01", null ],
      [ "maxcdf", "class_b_i_e_1_1_inverse_gamma_dist.html#a68f5103573f32db135d12b5b4beb8c2b", null ],
      [ "mean", "class_b_i_e_1_1_inverse_gamma_dist.html#a8bd462d8e0438a452b1bad2364d6922c", null ],
      [ "mincdf", "class_b_i_e_1_1_inverse_gamma_dist.html#a73c2dceeca013f8b68e718e9aeb34300", null ],
      [ "norm", "class_b_i_e_1_1_inverse_gamma_dist.html#a16f3ba4a29f709247a161c38d10af707", null ],
      [ "qmax", "class_b_i_e_1_1_inverse_gamma_dist.html#a18549b266109eea5b165f8e8a4013b5a", null ],
      [ "qmin", "class_b_i_e_1_1_inverse_gamma_dist.html#a46a54398bea06bdf620a43e6ba1e3f95", null ],
      [ "ugam", "class_b_i_e_1_1_inverse_gamma_dist.html#a4b0bbadddfdafcd3b26c02a2ced7cf28", null ],
      [ "var", "class_b_i_e_1_1_inverse_gamma_dist.html#abc4d8ee8da33170a8df06aaf4c2dbaed", null ],
      [ "vmax", "class_b_i_e_1_1_inverse_gamma_dist.html#aa96f7098ede3d4a71d85145ac80d7ad7", null ],
      [ "vmin", "class_b_i_e_1_1_inverse_gamma_dist.html#a4dd231a846b1ceca8f0315f7dfb2e1c6", null ]
    ] ],
    [ "MultiNWishartDist", "class_b_i_e_1_1_multi_n_wishart_dist.html", [
      [ "MultiNWishartDist", "group__distribution.html#gadaee5df224cf6dafbd740223ae928875", null ],
      [ "MultiNWishartDist", "group__distribution.html#ga2d3ade41b212bdf9e555ae5dbcd77ba1", null ],
      [ "~MultiNWishartDist", "class_b_i_e_1_1_multi_n_wishart_dist.html#ae0d832e22bd07d3b2cd1e538b032bf78", null ],
      [ "MultiNWishartDist", "class_b_i_e_1_1_multi_n_wishart_dist.html#a8b94d9c64b404284d6da1d65d53e98ca", null ],
      [ "CDF", "class_b_i_e_1_1_multi_n_wishart_dist.html#a849141c6570e7f24a6234f02cc9798c9", null ],
      [ "logPDF", "class_b_i_e_1_1_multi_n_wishart_dist.html#ad24eafaaff793b76fd81c4a2016d6ad6", null ],
      [ "lower", "class_b_i_e_1_1_multi_n_wishart_dist.html#abcaf8c12f1a92d3ac88e61846b6e0198", null ],
      [ "Mean", "class_b_i_e_1_1_multi_n_wishart_dist.html#a4fddffea1f5e59cc2c849a7dfd61a491", null ],
      [ "Moments", "class_b_i_e_1_1_multi_n_wishart_dist.html#a19dced92f0ebaa9321e65760c22eb91c", null ],
      [ "New", "class_b_i_e_1_1_multi_n_wishart_dist.html#ab172fd745f4dc4dea9bc70a8639df0ff", null ],
      [ "PDF", "class_b_i_e_1_1_multi_n_wishart_dist.html#a897a965c90fd5dfdccaa5a2b29c41d07", null ],
      [ "realize", "class_b_i_e_1_1_multi_n_wishart_dist.html#afdb975a63a7e4283706a7ad34a04da53", null ],
      [ "Sample", "class_b_i_e_1_1_multi_n_wishart_dist.html#a7b415cf3ac573ee4f0b154dc7b9b00d7", null ],
      [ "serialize", "class_b_i_e_1_1_multi_n_wishart_dist.html#a3f6444afc8ff428e422b0f5544d5b74b", null ],
      [ "StdDev", "class_b_i_e_1_1_multi_n_wishart_dist.html#a25e009a2fd09a925733efc60695c24f7", null ],
      [ "upper", "class_b_i_e_1_1_multi_n_wishart_dist.html#a455cd1a62e55e83d08da86bc2ba5e9ea", null ],
      [ "boost::serialization::access", "class_b_i_e_1_1_multi_n_wishart_dist.html#ac98d07dd8f7b70e16ccb9a01abf56b9c", null ],
      [ "A", "class_b_i_e_1_1_multi_n_wishart_dist.html#a7c496743c25535eb4075984f6d8f5a22", null ],
      [ "C", "class_b_i_e_1_1_multi_n_wishart_dist.html#abbb07fc4ff6e8e95420922c966f1e870", null ],
      [ "det", "class_b_i_e_1_1_multi_n_wishart_dist.html#ac6a6352e504d18fdab2767bf44706b45", null ],
      [ "diag", "class_b_i_e_1_1_multi_n_wishart_dist.html#a166e77423af52ad2c2fd3e0d735811a9", null ],
      [ "eigen", "class_b_i_e_1_1_multi_n_wishart_dist.html#a368c2fc9c848ac1c15954182bbe842a6", null ],
      [ "evals", "class_b_i_e_1_1_multi_n_wishart_dist.html#a11a670dacacc62c1681c5ceba43d4138", null ],
      [ "gamma", "class_b_i_e_1_1_multi_n_wishart_dist.html#a4bd881c69f7e1f226eb2475b22b1700a", null ],
      [ "M", "class_b_i_e_1_1_multi_n_wishart_dist.html#aba6ac25279de7ef234bcbbc04af0b35c", null ],
      [ "N", "class_b_i_e_1_1_multi_n_wishart_dist.html#a9326e71b8325fd16a2602dc0fa61bb35", null ],
      [ "normal", "class_b_i_e_1_1_multi_n_wishart_dist.html#a7a2f4af57b8ebbc4da8851e466dd0996", null ],
      [ "shape", "class_b_i_e_1_1_multi_n_wishart_dist.html#a130eaaf8c292e115a16f88059b81cc1b", null ],
      [ "W", "class_b_i_e_1_1_multi_n_wishart_dist.html#ac2070a112d5ad1cbd29b5052ed2bb8e9", null ]
    ] ],
    [ "NormalDist", "class_b_i_e_1_1_normal_dist.html", [
      [ "NormalDist", "class_b_i_e_1_1_normal_dist.html#a1b8eac1ea3ca844389337963fdf49ced", null ],
      [ "NormalDist", "class_b_i_e_1_1_normal_dist.html#a58a6c1ed0749db2051d6bdd393aca6ba", null ],
      [ "NormalDist", "class_b_i_e_1_1_normal_dist.html#af344de6bc28cd791ada1cab796717e96", null ],
      [ "NormalDist", "class_b_i_e_1_1_normal_dist.html#a86e92cc6a9626d88004e5a348df14507", null ],
      [ "NormalDist", "class_b_i_e_1_1_normal_dist.html#ac32f64e20773b079598b8760ca9a0636", null ],
      [ "~NormalDist", "class_b_i_e_1_1_normal_dist.html#a64db29f3df7ae15d281d25c2e1ec35d1", null ],
      [ "CDF", "class_b_i_e_1_1_normal_dist.html#a050ff9dd43a812bd91df690113ca43a5", null ],
      [ "logPDF", "class_b_i_e_1_1_normal_dist.html#ab71b0701f8d12706885c59c97fc09d6b", null ],
      [ "lower", "class_b_i_e_1_1_normal_dist.html#a62fa38be06914891c03a670fbd355f3d", null ],
      [ "Mean", "class_b_i_e_1_1_normal_dist.html#ab35431b998e1872f8b888ec5daea8fe8", null ],
      [ "Moments", "class_b_i_e_1_1_normal_dist.html#a7c0ec57d864776da5b138c90bd16de1c", null ],
      [ "New", "class_b_i_e_1_1_normal_dist.html#a28a5613feb202f62657de08581bd91ac", null ],
      [ "PDF", "class_b_i_e_1_1_normal_dist.html#a646b793c1cc27d1ab874b11f8cb94557", null ],
      [ "Sample", "class_b_i_e_1_1_normal_dist.html#a1e02bfa61d4f85867a0ad82794f720bd", null ],
      [ "serialize", "class_b_i_e_1_1_normal_dist.html#a48397083f08cbd726116f79cd69576c2", null ],
      [ "StdDev", "class_b_i_e_1_1_normal_dist.html#a3a7420e9f2d62c8334e4f007aa8e058e", null ],
      [ "upper", "class_b_i_e_1_1_normal_dist.html#a712cb1810280353448cd297fb585d10a", null ],
      [ "boost::serialization::access", "class_b_i_e_1_1_normal_dist.html#ac98d07dd8f7b70e16ccb9a01abf56b9c", null ],
      [ "maxcdf", "class_b_i_e_1_1_normal_dist.html#abd5e3b2375871e52936c18a089c44527", null ],
      [ "mean", "class_b_i_e_1_1_normal_dist.html#a2bf2f218c3a61269d6774d191ab2984a", null ],
      [ "mincdf", "class_b_i_e_1_1_normal_dist.html#ae2057bd108ae7d250b78aa3946cd8fce", null ],
      [ "norm", "class_b_i_e_1_1_normal_dist.html#a2b2a0719fcc71dc2e246313553bff65f", null ],
      [ "normal", "class_b_i_e_1_1_normal_dist.html#a43628e523e5a681124e0b5b7e2697ada", null ],
      [ "var", "class_b_i_e_1_1_normal_dist.html#a3ced458d15b18275c84cf6f377b2a582", null ],
      [ "vmax", "class_b_i_e_1_1_normal_dist.html#ae9796b242801a61031914e2be19abca0", null ],
      [ "vmin", "class_b_i_e_1_1_normal_dist.html#abf89b7821515bb9d40c8405c8711142b", null ]
    ] ],
    [ "NullDistribution", "class_b_i_e_1_1_null_distribution.html", [
      [ "NullDistribution", "class_b_i_e_1_1_null_distribution.html#ab40e99e3233d9922b70b66befd7d4960", null ],
      [ "~NullDistribution", "class_b_i_e_1_1_null_distribution.html#a1916903d8f194db845ea4325a9a0d081", null ],
      [ "New", "class_b_i_e_1_1_null_distribution.html#af76dca092cf73eecb01131fb9fbdd353", null ],
      [ "serialize", "class_b_i_e_1_1_null_distribution.html#a68f14d73edabfab380e767ebfe54c5a9", null ],
      [ "boost::serialization::access", "class_b_i_e_1_1_null_distribution.html#ac98d07dd8f7b70e16ccb9a01abf56b9c", null ]
    ] ],
    [ "PointDistribution", "class_b_i_e_1_1_point_distribution.html", [
      [ "PointDistribution", "class_b_i_e_1_1_point_distribution.html#aecfb3425be9c9518d354c17d548606dc", null ],
      [ "PointDistribution", "class_b_i_e_1_1_point_distribution.html#afe9d1d53215a52172c985ca79ea19ee4", null ],
      [ "~PointDistribution", "class_b_i_e_1_1_point_distribution.html#ac1e22e75ded10398475dbcf13c497dfb", null ],
      [ "PointDistribution", "class_b_i_e_1_1_point_distribution.html#ae1655acbf8e9da303f1af2f6391a5fc4", null ],
      [ "AccumData", "class_b_i_e_1_1_point_distribution.html#a564d77fd046b085375ba3f03c06f9300", null ],
      [ "AccumulateData", "class_b_i_e_1_1_point_distribution.html#a9c4924dbb0929b5a4336e72b60ebc8cf", null ],
      [ "AccumulateData", "class_b_i_e_1_1_point_distribution.html#aa3ed0ec2c445158f35848ade152d0f00", null ],
      [ "CDF", "class_b_i_e_1_1_point_distribution.html#a153ba2275389f53b86521056705c2378", null ],
      [ "getdim", "class_b_i_e_1_1_point_distribution.html#a8ee0f5221c9fa7c226fbf0bb5412da65", null ],
      [ "getRecordType", "class_b_i_e_1_1_point_distribution.html#a64196e210c810522061bed5c85bec835", null ],
      [ "getValue", "class_b_i_e_1_1_point_distribution.html#a7a35c1d29d5109bfe0116bf9ed1afa56", null ],
      [ "New", "class_b_i_e_1_1_point_distribution.html#add63f597324cc089df16469003aa5044", null ],
      [ "numberData", "class_b_i_e_1_1_point_distribution.html#a7d6fa4aacf2f9499641760e505184b56", null ],
      [ "Point", "class_b_i_e_1_1_point_distribution.html#a78613ba9ed175daaae524e51c2d3b6c6", null ],
      [ "serialize", "class_b_i_e_1_1_point_distribution.html#ad5a33863be9d962487bd8f3a71a574b8", null ],
      [ "boost::serialization::access", "class_b_i_e_1_1_point_distribution.html#ac98d07dd8f7b70e16ccb9a01abf56b9c", null ],
      [ "data", "class_b_i_e_1_1_point_distribution.html#af74b7669079679837cb802174e4b4989", null ],
      [ "full", "class_b_i_e_1_1_point_distribution.html#a9ec5aa2af5ca009b7f796c0125b5c418", null ]
    ] ],
    [ "SampleDistribution", "class_b_i_e_1_1_sample_distribution.html", [
      [ "~SampleDistribution", "class_b_i_e_1_1_sample_distribution.html#ab2377111bb720c278195dfed08e80aa3", null ],
      [ "SampleDistribution", "class_b_i_e_1_1_sample_distribution.html#ac6456eef8d22d2f6b0cb73ed6c95898e", null ],
      [ "AccumData", "class_b_i_e_1_1_sample_distribution.html#ae2885c8d3210c99134e91597ae2b8e19", null ],
      [ "AccumData", "class_b_i_e_1_1_sample_distribution.html#ab5c82bf942fc48552cc5dcfcf226da37", null ],
      [ "AccumData", "class_b_i_e_1_1_sample_distribution.html#a43d98a8cf83bc520707d2e7efe61210a", null ],
      [ "AccumulateData", "class_b_i_e_1_1_sample_distribution.html#abb13be0af2a623009fba6ff598f930b5", null ],
      [ "CDF", "class_b_i_e_1_1_sample_distribution.html#a5228afe7a61ca600681e7956b31d6818", null ],
      [ "ComputeDistribution", "class_b_i_e_1_1_sample_distribution.html#aff988ccf5217b345503d6abc248f5cac", null ],
      [ "getDataSetSize", "class_b_i_e_1_1_sample_distribution.html#afdfc4c8b3d719238f12214e29500a85c", null ],
      [ "getdim", "class_b_i_e_1_1_sample_distribution.html#aac74f1cd5ebf336874d4117385eb1839", null ],
      [ "getRecordType", "class_b_i_e_1_1_sample_distribution.html#af2acddcce93e154dfdb4557cb0b1a180", null ],
      [ "getValue", "class_b_i_e_1_1_sample_distribution.html#a7473cd6c68c0cfc5e2d6ad8383f47b8f", null ],
      [ "New", "class_b_i_e_1_1_sample_distribution.html#a5ecfcd1d6987bfdf1d88ef2e4077b8be", null ],
      [ "numberData", "class_b_i_e_1_1_sample_distribution.html#abbd59319e250eb093c4225a249661949", null ],
      [ "serialize", "class_b_i_e_1_1_sample_distribution.html#a28a125c31c3fdbe97e00872ab97c65b8", null ],
      [ "boost::serialization::access", "class_b_i_e_1_1_sample_distribution.html#ac98d07dd8f7b70e16ccb9a01abf56b9c", null ],
      [ "datasetsize", "class_b_i_e_1_1_sample_distribution.html#a648f46a1435f5cf86793721b72d18eb5", null ],
      [ "recordtype", "class_b_i_e_1_1_sample_distribution.html#ad86feecd054235925eab37c1ba56ba01", null ]
    ] ],
    [ "UniformDist", "class_b_i_e_1_1_uniform_dist.html", [
      [ "UniformDist", "class_b_i_e_1_1_uniform_dist.html#a6739a223db831923df7416971306d460", null ],
      [ "UniformDist", "class_b_i_e_1_1_uniform_dist.html#a5177481a0274d8a49b328a5b5b4d1a08", null ],
      [ "UniformDist", "class_b_i_e_1_1_uniform_dist.html#acb07012a65f9e7c27adb41dd3fa51379", null ],
      [ "~UniformDist", "class_b_i_e_1_1_uniform_dist.html#a1936081bc1ebd25a3dddca6e34a05d08", null ],
      [ "CDF", "class_b_i_e_1_1_uniform_dist.html#af4380156ea57e9136a9f2217bdb330c0", null ],
      [ "logPDF", "class_b_i_e_1_1_uniform_dist.html#ae70461da58cb864d20262a9af89c8f97", null ],
      [ "lower", "class_b_i_e_1_1_uniform_dist.html#ab60320b55e72df67e76f6dee307ce798", null ],
      [ "Mean", "class_b_i_e_1_1_uniform_dist.html#a20bcf4fad3dbabc140532098c98d68c4", null ],
      [ "Moments", "class_b_i_e_1_1_uniform_dist.html#adb83e6eb98f5ac65111c136568710864", null ],
      [ "New", "class_b_i_e_1_1_uniform_dist.html#a2e55a6f4d2ec109ab72f41e25fd75817", null ],
      [ "PDF", "class_b_i_e_1_1_uniform_dist.html#adbef02499d9c5c3a4b4e0e0440ae5690", null ],
      [ "Sample", "class_b_i_e_1_1_uniform_dist.html#a3a87c8910eaf25288417b983e02ee105", null ],
      [ "serialize", "class_b_i_e_1_1_uniform_dist.html#aaccfb21448dcc86d166c75acc6fb48de", null ],
      [ "StdDev", "class_b_i_e_1_1_uniform_dist.html#a2ceec86f8ce2c951361c70365d25ece8", null ],
      [ "upper", "class_b_i_e_1_1_uniform_dist.html#a5d6e238f47713385814b7cba84c69ebc", null ],
      [ "boost::serialization::access", "class_b_i_e_1_1_uniform_dist.html#ac98d07dd8f7b70e16ccb9a01abf56b9c", null ],
      [ "a", "class_b_i_e_1_1_uniform_dist.html#ad536a69ed4bfde717dbcc5d25ba2bf59", null ],
      [ "b", "class_b_i_e_1_1_uniform_dist.html#a7356fa1a5d002724f523095f97c5e076", null ],
      [ "mean", "class_b_i_e_1_1_uniform_dist.html#a485944abbdd74ad4515bf020e0f1ee96", null ],
      [ "unit", "class_b_i_e_1_1_uniform_dist.html#aa45e4c1d8b04fd60f18f75e9c40ec8f9", null ],
      [ "var", "class_b_i_e_1_1_uniform_dist.html#aa06907456f85189767ac73e6f91be4be", null ]
    ] ],
    [ "WeibullDist", "class_b_i_e_1_1_weibull_dist.html", [
      [ "WeibullDist", "class_b_i_e_1_1_weibull_dist.html#af125894b427158a4012109627140f573", null ],
      [ "WeibullDist", "class_b_i_e_1_1_weibull_dist.html#aff84834403157740a2a43b065a224187", null ],
      [ "~WeibullDist", "class_b_i_e_1_1_weibull_dist.html#ad51e4dd69f5d78c8b43438d1e5d35126", null ],
      [ "CDF", "class_b_i_e_1_1_weibull_dist.html#a62111db3a00253b4ea0ce2408470ea6a", null ],
      [ "logPDF", "class_b_i_e_1_1_weibull_dist.html#ada265ef5f90d6c563da7f74147d8cf56", null ],
      [ "lower", "class_b_i_e_1_1_weibull_dist.html#ad04d201da0fee27afc32de870e778134", null ],
      [ "Mean", "class_b_i_e_1_1_weibull_dist.html#a1351e39455ef2f3821bf621064a828ad", null ],
      [ "Moments", "class_b_i_e_1_1_weibull_dist.html#a836a0be4fae42e3c47eeee70003a2094", null ],
      [ "New", "class_b_i_e_1_1_weibull_dist.html#ab5a6e08b0c816879d632362917ee7743", null ],
      [ "PDF", "class_b_i_e_1_1_weibull_dist.html#ac8cb721a8d6e839b283ad23f68e64421", null ],
      [ "Sample", "class_b_i_e_1_1_weibull_dist.html#a4636ea456bf877ef1a7b2708555f0aac", null ],
      [ "serialize", "class_b_i_e_1_1_weibull_dist.html#a5575bd36fb0f8a8262538d8ed47845d6", null ],
      [ "StdDev", "class_b_i_e_1_1_weibull_dist.html#aa8212927c06ac6b4874798648179cd55", null ],
      [ "upper", "class_b_i_e_1_1_weibull_dist.html#a0e5c79d375ab9449aee6be4742e84702", null ],
      [ "boost::serialization::access", "class_b_i_e_1_1_weibull_dist.html#ac98d07dd8f7b70e16ccb9a01abf56b9c", null ],
      [ "alpha", "class_b_i_e_1_1_weibull_dist.html#aa95a1c644d9b948527647549a921a21e", null ],
      [ "beta", "class_b_i_e_1_1_weibull_dist.html#a7bf319c2ddb18a9c354ea619ebf2ab4c", null ],
      [ "maxcdf", "class_b_i_e_1_1_weibull_dist.html#a3b31d179a4532401f0ef6427da970e80", null ],
      [ "mean", "class_b_i_e_1_1_weibull_dist.html#a30e968aae2f1eca3c44cd94277f352d5", null ],
      [ "mincdf", "class_b_i_e_1_1_weibull_dist.html#ab70e7dd297a3135cd75a52045618e7f5", null ],
      [ "norm", "class_b_i_e_1_1_weibull_dist.html#aba5414e845a7e5f80008f45f84c86545", null ],
      [ "unit", "class_b_i_e_1_1_weibull_dist.html#aec81ece804b6af2473c2330d6c1532ab", null ],
      [ "var", "class_b_i_e_1_1_weibull_dist.html#a769cf3b4a5091c69765877b95b5f9b99", null ],
      [ "vmax", "class_b_i_e_1_1_weibull_dist.html#a33ae9cbc0d4f75e71cab2d10a946a457", null ],
      [ "vmin", "class_b_i_e_1_1_weibull_dist.html#aed33fb92f899239d5b63545934ff235a", null ]
    ] ],
    [ "MultiNWishartDist", "group__distribution.html#gadaee5df224cf6dafbd740223ae928875", null ],
    [ "MultiNWishartDist", "group__distribution.html#ga2d3ade41b212bdf9e555ae5dbcd77ba1", null ]
];