var class_user_state =
[
    [ "UserState", "class_user_state.html#a762eb91e493111290efc1973c17fd650", null ],
    [ "UserState", "class_user_state.html#a56a01f7fb068bad3a770b7589dd06de9", null ],
    [ "~UserState", "class_user_state.html#a76406af79eb123ffafa495bcd6c32d28", null ],
    [ "getHistory", "class_user_state.html#af65e2e00fcdd1ed58164332756f369cd", null ],
    [ "getMetaInfo", "class_user_state.html#acfd2de6c79076de5d795a12b9302a63d", null ],
    [ "getSession", "class_user_state.html#a771218adcf44042927aa86c8b3d2c6c5", null ],
    [ "getTransClosure", "class_user_state.html#ace9602c837fac4ad18990e66fc502434", null ],
    [ "getVersion", "class_user_state.html#ae8ef97aed7e44cf391f3ebdb3373eaca", null ],
    [ "newVersion", "class_user_state.html#a5b242830b56d5c273ea1f2f761071d75", null ],
    [ "serialize", "class_user_state.html#a05d7c9049d0b5c34ac3c0fe8d155c612", null ],
    [ "setSession", "class_user_state.html#a15ba419f95fdd362ddd63207e25f3408", null ],
    [ "setVersion", "class_user_state.html#a9ab16e88a024bb75b6b82f3457aec218", null ],
    [ "boost::serialization::access", "class_user_state.html#ac98d07dd8f7b70e16ccb9a01abf56b9c", null ],
    [ "session_name", "class_user_state.html#aad763d2c80fa279af2e00b420426093b", null ],
    [ "state_version", "class_user_state.html#a180f37d0e7042a892806daaf76c782ba", null ]
];