var class_b_i_e_1_1_tess_tool_receiver =
[
    [ "TessToolReceiver", "class_b_i_e_1_1_tess_tool_receiver.html#aad34b5bb2fd6d2c48d822d7d1d5d1a8c", null ],
    [ "TessToolReceiver", "class_b_i_e_1_1_tess_tool_receiver.html#aa38a6fdad07d84fac09d3d6f6c010e0b", null ],
    [ "~TessToolReceiver", "class_b_i_e_1_1_tess_tool_receiver.html#a9675955373d2640d36344ac7ca6d6878", null ],
    [ "Detach", "class_b_i_e_1_1_tess_tool_receiver.html#a2aac4617ba0952d74be568f60dc523fb", null ],
    [ "GetData", "class_b_i_e_1_1_tess_tool_receiver.html#a08ce74d90c38d036440d418c6571ba96", null ],
    [ "isConnectedToTessToolSender", "class_b_i_e_1_1_tess_tool_receiver.html#a3b436aca9727ca04a99b7dc6cc79302d", null ],
    [ "run", "class_b_i_e_1_1_tess_tool_receiver.html#a730a7864d55269f353b41a019136c49c", null ],
    [ "SampleNext", "class_b_i_e_1_1_tess_tool_receiver.html#aa1f3c97010b475dc88891580d08c5afa", null ],
    [ "Synchronize", "class_b_i_e_1_1_tess_tool_receiver.html#a94ab8af5375121e65b76fbadf6434f63", null ],
    [ "filter_capacityInRecords", "class_b_i_e_1_1_tess_tool_receiver.html#a0fcfac0eca4abf2f6c0dcffd4bb60358", null ],
    [ "filter_sessionId", "class_b_i_e_1_1_tess_tool_receiver.html#a353438a59283d77a589b632ba8eda79c", null ]
];