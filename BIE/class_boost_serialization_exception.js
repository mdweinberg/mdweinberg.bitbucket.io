var class_boost_serialization_exception =
[
    [ "BoostSerializationException", "class_boost_serialization_exception.html#acd54ece17dc6530ec9301206ca0a7c14", null ],
    [ "BoostSerializationException", "class_boost_serialization_exception.html#ac71a2245ca4e0253948af5d8a4041cb8", null ],
    [ "BoostSerializationException", "class_boost_serialization_exception.html#a16c467fae59d3afabc091dd6dbbbf40b", null ],
    [ "BoostSerializationException", "class_boost_serialization_exception.html#aa9f214a7606171e6285c107bb41a4487", null ],
    [ "~BoostSerializationException", "class_boost_serialization_exception.html#a637001fb22d23395147fff148ca2f787", null ],
    [ "print", "class_boost_serialization_exception.html#a722607790eee404982620b01635b749c", null ],
    [ "cause", "class_boost_serialization_exception.html#a8d975d9b0a0abd400eedc6f1b2c13277", null ],
    [ "extra_msg", "class_boost_serialization_exception.html#a499c80557a34c3ab91b7b911ca8a37e1", null ],
    [ "msg", "class_boost_serialization_exception.html#a60c6abee0a2136345bc8d49989d10af1", null ]
];