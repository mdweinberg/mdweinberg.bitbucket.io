var class_candidate_list =
[
    [ "CandidateList", "class_candidate_list.html#ac955ac8236828732544cea7d1d7f7bd4", null ],
    [ "CandidateList", "class_candidate_list.html#a93e59d3b7c8f9cfc11f85eda636cd96d", null ],
    [ "add", "class_candidate_list.html#abd5bd2419c5e52d405ad8ece953f4cff", null ],
    [ "Count", "class_candidate_list.html#a01ba34f63977ba579854645ce0115a01", null ],
    [ "get", "class_candidate_list.html#a39b9b4662517b6ba11fd3a0005ef9dc1", null ],
    [ "maxDist", "class_candidate_list.html#a33c30f6cc65ec83e69a24602aaae1c37", null ],
    [ "NN", "class_candidate_list.html#aefdc9638971362e57e7ccf029f255f47", null ],
    [ "serialize", "class_candidate_list.html#a750daefa3c415d7a0083e84c8efa9caa", null ],
    [ "V", "class_candidate_list.html#a20524ae763baa942e0ff1f0485e1b972", null ],
    [ "boost::serialization::access", "class_candidate_list.html#ac98d07dd8f7b70e16ccb9a01abf56b9c", null ],
    [ "cl", "class_candidate_list.html#a4ddc3a1e71653c9738fabc48467afd98", null ],
    [ "ct", "class_candidate_list.html#a66072f2065b578dd3e49e9da634bbcd8", null ],
    [ "my", "class_candidate_list.html#a78111a6741ef7cfb07a4add47506c6c0", null ],
    [ "nn", "class_candidate_list.html#aac67b25f9bc01e5b06d0d4ff39e11e80", null ],
    [ "pt", "class_candidate_list.html#aa4bbb681dcdb0230d0f51a02dd5085a1", null ]
];