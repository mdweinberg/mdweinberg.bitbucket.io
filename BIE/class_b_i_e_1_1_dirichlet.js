var class_b_i_e_1_1_dirichlet =
[
    [ "Dirichlet", "class_b_i_e_1_1_dirichlet.html#a13dca34988adfaaa9e27400be1d25b80", null ],
    [ "Dirichlet", "class_b_i_e_1_1_dirichlet.html#a9702dabb5687072fe28fc447c987d84b", null ],
    [ "Dirichlet", "class_b_i_e_1_1_dirichlet.html#a02fbefdc07dab205254da0212f843a12", null ],
    [ "~Dirichlet", "class_b_i_e_1_1_dirichlet.html#a73eef67d13ae5ec8ad5e9318bd846733", null ],
    [ "logPDF", "class_b_i_e_1_1_dirichlet.html#a6f349a70a96f0793be364fa126e23677", null ],
    [ "lower", "class_b_i_e_1_1_dirichlet.html#ae3d20f3d5e030796888809ba68e04263", null ],
    [ "Mean", "class_b_i_e_1_1_dirichlet.html#a9cd49e41c0a6ed15d9e2018479b7bcd8", null ],
    [ "Moments", "class_b_i_e_1_1_dirichlet.html#a7e6c1b2e8cdffcdc3a06f04f4b53cb09", null ],
    [ "New", "class_b_i_e_1_1_dirichlet.html#a2e528459d95d2a83238d9df2bb52cee2", null ],
    [ "PDF", "class_b_i_e_1_1_dirichlet.html#a786f4b911d92b56a95941ad837faa444", null ],
    [ "Sample", "class_b_i_e_1_1_dirichlet.html#a3fc675c65612d183417ecd82a907bdbf", null ],
    [ "serialize", "class_b_i_e_1_1_dirichlet.html#a5b172aef9caa83fb5f691a4615436587", null ],
    [ "StdDev", "class_b_i_e_1_1_dirichlet.html#afe07477fe33bd561812dd863735e0c57", null ],
    [ "upper", "class_b_i_e_1_1_dirichlet.html#a6bd3cde626aca2af19ff1b0c27952135", null ],
    [ "boost::serialization::access", "class_b_i_e_1_1_dirichlet.html#ac98d07dd8f7b70e16ccb9a01abf56b9c", null ],
    [ "A", "class_b_i_e_1_1_dirichlet.html#a61060c838f06ae5310ae7b338f982123", null ],
    [ "gengam", "class_b_i_e_1_1_dirichlet.html#a1b3c3e951c919f000539c276e004eead", null ],
    [ "n", "class_b_i_e_1_1_dirichlet.html#a2b1019173447ee0aa12b1f620ecf8226", null ],
    [ "pdf_fac", "class_b_i_e_1_1_dirichlet.html#a150190b3946bc5a4100818fae3a3cb91", null ],
    [ "sumA", "class_b_i_e_1_1_dirichlet.html#a1fccef2a52afebd5782f746b79e3d47c", null ],
    [ "varA", "class_b_i_e_1_1_dirichlet.html#a9c0a89cfc7d587ea30e942339e8e7fc2", null ]
];