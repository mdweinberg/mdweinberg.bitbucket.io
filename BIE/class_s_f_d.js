var class_s_f_d =
[
    [ "SFDmap", "class_s_f_d.html#aa77cbe3c3304148b5866442de978a4ab", null ],
    [ "SFD", "class_s_f_d.html#a5f67776ac350de334e62413f97ce36a3", null ],
    [ "SFD", "class_s_f_d.html#adc71e63235310e5fd1bf01eb8e08a00d", null ],
    [ "compute_extinction", "class_s_f_d.html#a596dcd3fac8b1c7b55a96fff93857ab4", null ],
    [ "compute_interp", "class_s_f_d.html#a23323119649f999b4cd5d1e8f96de229", null ],
    [ "getext", "class_s_f_d.html#adaf45755473dacc20c2172285613d20e", null ],
    [ "getext_closest_distance", "class_s_f_d.html#a6cc8fdaab82dbfc4f80a09ecead34249", null ],
    [ "getext_distance", "class_s_f_d.html#a4a894b7ece2ceb71c4534eeea51df1bb", null ],
    [ "getext_distance", "class_s_f_d.html#a94c57bf381a5b0dbe2b9ec7f93bfcf55", null ],
    [ "getext_distance", "class_s_f_d.html#addfca7f289c5224402ee453930aface3", null ],
    [ "getsfd", "class_s_f_d.html#afbc305512d0c8361a535c6a509c40525", null ],
    [ "getsfd_closest", "class_s_f_d.html#af4a7021a8d1ae5e50f48485886a06235", null ],
    [ "serialize", "class_s_f_d.html#adb488ea010f4970659257141565ff33d", null ],
    [ "boost::serialization::access", "class_s_f_d.html#ac98d07dd8f7b70e16ccb9a01abf56b9c", null ],
    [ "greater_lon", "class_s_f_d.html#ad98e1fdeace5090f9d7bb4e5aa351757", null ],
    [ "less_lon", "class_s_f_d.html#aadb44203892f20f2a3ee43ceb55c58f7", null ],
    [ "A1", "class_s_f_d.html#ac08ed21046f66fd34d232514f10cc592", null ],
    [ "CENTERAMP", "class_s_f_d.html#aaa9cae2890b175588775282f18e4f457", null ],
    [ "CENTERWIDTH", "class_s_f_d.html#ab5f11a49d2f527d4daafcd5d110af37a", null ],
    [ "dr", "class_s_f_d.html#a6ffb35a1cc53c2da9329ba9e7409813a", null ],
    [ "fconst", "class_s_f_d.html#afeaa0fa252f6dc75c6923f579d7957ac", null ],
    [ "greaterl", "class_s_f_d.html#a4a1b2ce15e432d70752875d9899fad08", null ],
    [ "lessl", "class_s_f_d.html#a2c60c3524c86a5a7f27580653b81ae44", null ],
    [ "NEARBYAMP", "class_s_f_d.html#a7b5353f17b57ec7df2333c749f0038b8", null ],
    [ "NEARBYWIDTH", "class_s_f_d.html#a40fc926fd216a3331c2de30a1ee9e489", null ],
    [ "NUM", "class_s_f_d.html#a5f4d67976596ff18af5824a6978b3db4", null ],
    [ "R0", "class_s_f_d.html#ae87f787e62046a7b8efaf6c6d033dbf1", null ],
    [ "radius", "class_s_f_d.html#a37cdaf9496b68c0a5e7bdb3296ac0d61", null ],
    [ "RING", "class_s_f_d.html#ac4b59bc0b1629abca4b695acdd20d376", null ],
    [ "RINGAMP", "class_s_f_d.html#ae0165bc1b708cd74f5c0736ec66f05ee", null ],
    [ "RINGLOC", "class_s_f_d.html#aedf04fcf87ae158974887c94e9758ea0", null ],
    [ "RINGWIDTH", "class_s_f_d.html#a7fc01eaed6f6b89f45ad508aa9074626", null ],
    [ "RLOG", "class_s_f_d.html#a5287d65b22f5929d6d1986dcbf695f50", null ],
    [ "RMAX", "class_s_f_d.html#a266b2b01596717326edfbd4accf18b41", null ],
    [ "RMIN", "class_s_f_d.html#a06b03a1195a0a10fa05217478cea2599", null ],
    [ "sfdlat", "class_s_f_d.html#a6880a89765a3632f750f81a0ece31eb3", null ],
    [ "sfdmap", "class_s_f_d.html#a1789d3320051d28c260b85e4a37488e8", null ],
    [ "space", "class_s_f_d.html#ae6f1e8e1fc1e2d67f75418309fa55e41", null ],
    [ "Z0", "class_s_f_d.html#a7b4dfb69be9d1d8e0b2132302af13349", null ],
    [ "Z1", "class_s_f_d.html#adc306bde1e87fbbd0d1431d28e25b1a0", null ]
];