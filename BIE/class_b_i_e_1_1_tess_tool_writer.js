var class_b_i_e_1_1_tess_tool_writer =
[
    [ "TessToolWriter", "class_b_i_e_1_1_tess_tool_writer.html#a1759f6c667f769e9bc818a7609bfb548", null ],
    [ "~TessToolWriter", "class_b_i_e_1_1_tess_tool_writer.html#a2666e371262ca4f151f8c6807e06925c", null ],
    [ "CacheData", "class_b_i_e_1_1_tess_tool_writer.html#af1c9557e4420ceb4f7c197bfe5abd8fa", null ],
    [ "filter_capacityInRecords", "class_b_i_e_1_1_tess_tool_writer.html#a1c863198f78cd3367662bba1fb75757c", null ],
    [ "filter_recordSize", "class_b_i_e_1_1_tess_tool_writer.html#ab990c43e9d22ed4885656faff04dded4", null ],
    [ "filter_sessionId", "class_b_i_e_1_1_tess_tool_writer.html#ae1cf9308159978042231075386419ab6", null ],
    [ "wwerr", "class_b_i_e_1_1_tess_tool_writer.html#a4ed0b69fcce16f97aa262bf137add4f4", null ]
];