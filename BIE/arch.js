var arch =
[
    [ "Software overview", "software.html", null ],
    [ "Code Standard", "standard.html", null ],
    [ "BIE regression test using DejaGNU", "regressiontest.html", null ],
    [ "Parallel debugging with MPI", "pdebug.html", [
      [ "BASICS", "pdebug.html#basics", null ],
      [ "Launching a debugger for each rank: background", "pdebug.html#debugger", null ]
    ] ]
];