var _engine_config_8h =
[
    [ "BackendType", "_engine_config_8h.html#acee299fbb7d897867808250049524594", [
      [ "BACKEND_FILE", "_engine_config_8h.html#acee299fbb7d897867808250049524594a1c4bc73bfe92dcf908b31eac0603c51c", null ]
    ] ],
    [ "EngineType", "_engine_config_8h.html#afccd19cf3b9753ee84ad2eca7bed182f", [
      [ "BOOST_TEXT", "_engine_config_8h.html#afccd19cf3b9753ee84ad2eca7bed182fab6e3c20af5b500895621cac6fddfeda8", null ],
      [ "BOOST_XML", "_engine_config_8h.html#afccd19cf3b9753ee84ad2eca7bed182fa0db8c5b1c489b1c2d7feb3107362490b", null ],
      [ "BOOST_BINARY", "_engine_config_8h.html#afccd19cf3b9753ee84ad2eca7bed182fa7dd3060d86db9af459458635c31984c3", null ]
    ] ],
    [ "engine_type_str", "_engine_config_8h.html#a6ab67e0fc161dca4e3e9726863f3d155", null ],
    [ "persistence_dir", "_engine_config_8h.html#a1599919f5bebb80f64f4117a1671deb5", null ]
];