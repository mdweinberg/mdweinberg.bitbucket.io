var _b_i_empi_8h =
[
    [ "mpi_init", "_b_i_empi_8h.html#a6acf463890ffa208073e1c1368792d86", null ],
    [ "mpi_quit", "_b_i_empi_8h.html#a66eeb99b134bacb22876b16097333266", null ],
    [ "CLI_COMM_WORLD", "_b_i_empi_8h.html#a17c6aa5500844b2b38630941fb05f834", null ],
    [ "MPI_COMM_SLAVE", "_b_i_empi_8h.html#a8dbd64463737045adb59a64a0d6db49d", null ],
    [ "proc_namelen", "_b_i_empi_8h.html#a6071697f55b85487f16ddac427f9b03b", null ],
    [ "processor_name", "_b_i_empi_8h.html#a77f63511479b63942d3132cc6bf58240", null ],
    [ "slaves", "_b_i_empi_8h.html#af92e2f707f9ffeb1a017dcdcf6045927", null ]
];