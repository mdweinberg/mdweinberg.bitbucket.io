var class_spline_kernel =
[
    [ "SplineKernel", "class_spline_kernel.html#a6b7382ec978958ed7855098683883b92", null ],
    [ "Combine", "class_spline_kernel.html#a999801755fd06e0b016209465b9f1781", null ],
    [ "maxDistKer", "class_spline_kernel.html#abb10c2291de87572cccecbdb397e6e6f", null ],
    [ "minDistKer", "class_spline_kernel.html#acd1b050e557e8ca4d4c2fe9b0f2e94f0", null ],
    [ "New", "class_spline_kernel.html#a7dc9fc93745883b27b4c8e422300783d", null ],
    [ "serialize", "class_spline_kernel.html#a974568538a4d89186e5b6bc2cc15073f", null ],
    [ "boost::serialization::access", "class_spline_kernel.html#ac98d07dd8f7b70e16ccb9a01abf56b9c", null ]
];