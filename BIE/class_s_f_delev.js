var class_s_f_delev =
[
    [ "SFDelev", "class_s_f_delev.html#abdb39f11926e80764ecae785d96d9018", null ],
    [ "SFDelev", "class_s_f_delev.html#a8cc4188ef3851170382716a243ee97bd", null ],
    [ "compute", "class_s_f_delev.html#a8233db82103c562d3787c7f66c5cb356", null ],
    [ "enter", "class_s_f_delev.html#a4cd94e7971515c6bd4c3bd4b211285a4", null ],
    [ "getval", "class_s_f_delev.html#aa4bd8128cb06e3cb0885160e94335d97", null ],
    [ "next", "class_s_f_delev.html#a2d61cd919005e0bec4b72db5da5f5d92", null ],
    [ "reset", "class_s_f_delev.html#a39b811e74ab9999e90f36c3a9ff2bfbe", null ],
    [ "serialize", "class_s_f_delev.html#a694cd0e690e2346c4c94deb1e9e69167", null ],
    [ "Set", "class_s_f_delev.html#aab9a6f28bd0b9cb58c33563b714469ff", null ],
    [ "boost::serialization::access", "class_s_f_delev.html#ac98d07dd8f7b70e16ccb9a01abf56b9c", null ],
    [ "computed", "class_s_f_delev.html#a569a0c7600174ae971a0e95404a242b4", null ],
    [ "dphi", "class_s_f_delev.html#a8b0f88f57b294e996e68aa606286622d", null ],
    [ "it", "class_s_f_delev.html#aa14558af43ad2ea82281ca4e14d61f2e", null ],
    [ "numbin", "class_s_f_delev.html#adbacb458806b2401f3cfc2939eea33e1", null ],
    [ "vec", "class_s_f_delev.html#ae374bdf71a007c0e3294253da321ae2a", null ]
];