var class_make_star =
[
    [ "MakeStar", "class_make_star.html#ab84b8b1d677c359c57be38056562517f", null ],
    [ "~MakeStar", "class_make_star.html#a3c46d2283ddf80e1aa58d97845866335", null ],
    [ "realize", "class_make_star.html#a1a79f518d3e19367b53c42b2a778db1f", null ],
    [ "A", "class_make_star.html#abe599b377a7e943ae092081fcb526f6e", null ],
    [ "gen", "class_make_star.html#adc48d75eb6edaeb276dc6e97daaa8a6f", null ],
    [ "H", "class_make_star.html#aff5514ade5859bbd0d765cfbf0fd8ad1", null ],
    [ "MAXITER", "class_make_star.html#a22b72667b3c16ed72462f66a88b8ac27", null ],
    [ "R0", "class_make_star.html#a6b956399a34cebc7b0c8c39d67303c05", null ],
    [ "TOL", "class_make_star.html#a6e969ce20a1b8c2ec71daacdf0a69567", null ],
    [ "unit", "class_make_star.html#a885166c5fc0faca71133f57602d86d51", null ],
    [ "Z0", "class_make_star.html#a42cc8efeeb559c5727fb90f44d7fc457", null ]
];