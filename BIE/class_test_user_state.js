var class_test_user_state =
[
    [ "TestUserState", "class_test_user_state.html#a34618894988ab4294492ef937681f755", null ],
    [ "TestUserState", "class_test_user_state.html#ac5c68d68f4453fe2fedb9351f258a45d", null ],
    [ "addObject", "class_test_user_state.html#a4f057d3f89c57aa5e62f9ce6594687bb", null ],
    [ "addObject", "class_test_user_state.html#ab1ebb963ca6ee5611e2898f88e147f01", null ],
    [ "addObject", "class_test_user_state.html#afe1ce9a948e3b667426bb6a9438fe359", null ],
    [ "addObject", "class_test_user_state.html#a89fe6991b6849f124b6c37cc3e3fdd13", null ],
    [ "addObject", "class_test_user_state.html#a232a1c7a1eb2171aebd8d24f96daf0f3", null ],
    [ "addObject", "class_test_user_state.html#abdfce77c4721a9b64fb780c1d3970cc0", null ],
    [ "getHistory", "class_test_user_state.html#a95750a4e32162a41fd9d480b2edf5f90", null ],
    [ "getMetaInfo", "class_test_user_state.html#ac082a2d24783aa46a7bda4da4cec4b06", null ],
    [ "getTransClosure", "class_test_user_state.html#a43d0fd3b7fff785090d5639e7021acf1", null ],
    [ "serialize", "class_test_user_state.html#a369aea4a15f98d252407f0af2079c8b4", null ],
    [ "boost::serialization::access", "class_test_user_state.html#ac98d07dd8f7b70e16ccb9a01abf56b9c", null ],
    [ "tus_history", "class_test_user_state.html#a96265b9e09cef2c7fe4bbcb8333c2784", null ],
    [ "tus_metainfo", "class_test_user_state.html#aa5f4d3c6a21b61991e739fccbc35f457", null ],
    [ "tus_transclosure", "class_test_user_state.html#ab7b3d5b8af2c3e6137ae185c76d52c67", null ]
];