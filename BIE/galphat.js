var galphat =
[
    [ "Galphat introduction", "galphat_intro.html", null ],
    [ "Galphat details", "galphat_details.html", [
      [ "Galphat algorithms and features", "galphat_details.html#galphat_features", [
        [ "Overview of Galphat implementation", "galphat_details.html#galphat_implement", null ],
        [ "Model generation", "galphat_details.html#galphat_model_gen", [
          [ "Cumulative table", "galphat_details.html#galphat_table", null ],
          [ "Rotation of model galaxy", "galphat_details.html#galphat_rotation", null ],
          [ "PSF convolution", "galphat_details.html#galphat_psf", null ],
          [ "Computation time for model generation", "galphat_details.html#galphat_time", null ]
        ] ],
        [ "Likelihood and Prior", "galphat_details.html#galphat_likelihood", null ]
      ] ]
    ] ],
    [ "Galphat code design", "galphat_code.html", [
      [ "Galphat architecture overview", "galphat_code.html#galphat_overview", null ],
      [ "Galphat configuration files", "galphat_code.html#galphat_config_file", [
        [ "The default value data base", "galphat_code.html#galphat_defaults", null ],
        [ "Current model definitions", "galphat_code.html#galphat_models", [
          [ "Single (7 parameters)", "galphat_code.html#galphat_model_single", null ],
          [ "Sky (3 parameters)", "galphat_code.html#galphat_model_sky", null ],
          [ "(12 parameter)", "galphat_code.html#galphat_model_bulgedisk", null ],
          [ "(3 parameter)", "galphat_code.html#galphat_model_psf", null ]
        ] ],
        [ "Extending Galphat's model types", "galphat_code.html#galphat_extending", [
          [ "Adding a new flux model", "galphat_code.html#galphat_new_flux", null ],
          [ "Adding a new model type", "galphat_code.html#galphat_new_modl", null ]
        ] ]
      ] ]
    ] ],
    [ "Galphat examples", "galphat_usage.html", [
      [ "Directory contents", "galphat_usage.html#usage_contents", null ],
      [ "FITS output: comparing models and data", "galphat_usage.html#usage_fits_comp", [
        [ "QUICK START", "galphat_usage.html#usage_qs", null ],
        [ "Details", "galphat_usage.html#details", null ]
      ] ],
      [ "The BulgeDisk half-light table", "galphat_usage.html#bulge_disk_table", null ]
    ] ]
];