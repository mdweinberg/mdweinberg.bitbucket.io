var modules =
[
    [ "BIE global variables", "group___variables.html", "group___variables" ],
    [ "Convergence algorithms", "group__converge.html", "group__converge" ],
    [ "Data trees and structures", "group__data.html", "group__data" ],
    [ "Ensembles of states", "group__ensembles.html", "group__ensembles" ],
    [ "I/O stream filters", "group__filters.html", "group__filters" ],
    [ "Likelihood computation", "group__likelihood.html", "group__likelihood" ],
    [ "Likelihood functions", "group__likefunc.html", "group__likefunc" ],
    [ "Markov chain algorithms", "group__mcalgorithm.html", "group__mcalgorithm" ],
    [ "Mcsimulation", "group__mcsimulation.html", "group__mcsimulation" ],
    [ "Probability distributions", "group__distribution.html", "group__distribution" ],
    [ "Simulation methods", "group__simulation.html", "group__simulation" ],
    [ "Tessellation types", "group__tess.html", "group__tess" ]
];