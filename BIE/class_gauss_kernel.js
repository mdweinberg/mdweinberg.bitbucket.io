var class_gauss_kernel =
[
    [ "GaussKernel", "class_gauss_kernel.html#a18b0765aacb3785cfea91f98fb8f9a24", null ],
    [ "Combine", "class_gauss_kernel.html#a99f3f0b0edde0473c9002ac3ead5c35f", null ],
    [ "maxDistKer", "class_gauss_kernel.html#adee73f60b9411ce88b780cacddfbd77f", null ],
    [ "minDistKer", "class_gauss_kernel.html#a225ae17783f4d86b9ecaf15a05557984", null ],
    [ "New", "class_gauss_kernel.html#a8ba72e9d660300e4c12e3f8fe553287b", null ],
    [ "serialize", "class_gauss_kernel.html#a81d7ebb1bd5ab1f8cfe51937b42a0516", null ],
    [ "boost::serialization::access", "class_gauss_kernel.html#ac98d07dd8f7b70e16ccb9a01abf56b9c", null ]
];