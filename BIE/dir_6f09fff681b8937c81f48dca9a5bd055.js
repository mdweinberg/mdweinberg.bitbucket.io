var dir_6f09fff681b8937c81f48dca9a5bd055 =
[
    [ "Backend.h", "_backend_8h.html", [
      [ "Backend", "class_backend.html", "class_backend" ]
    ] ],
    [ "BoostLoadEngine.h", "_boost_load_engine_8h.html", [
      [ "BoostLoadEngine", "class_boost_load_engine.html", "class_boost_load_engine" ]
    ] ],
    [ "BoostSaveEngine.h", "_boost_save_engine_8h.html", [
      [ "BoostSaveEngine", "class_boost_save_engine.html", "class_boost_save_engine" ]
    ] ],
    [ "CheckpointManager.h", "_checkpoint_manager_8h.html", [
      [ "CheckpointManager", "class_checkpoint_manager.html", "class_checkpoint_manager" ]
    ] ],
    [ "CLICheckpointManager.h", "_c_l_i_checkpoint_manager_8h.html", [
      [ "CLICheckpointManager", "class_c_l_i_checkpoint_manager.html", "class_c_l_i_checkpoint_manager" ]
    ] ],
    [ "CLILoadManager.h", "_c_l_i_load_manager_8h.html", [
      [ "CLILoadManager", "class_c_l_i_load_manager.html", "class_c_l_i_load_manager" ]
    ] ],
    [ "CLISaveManager.h", "_c_l_i_save_manager_8h.html", [
      [ "CLISaveManager", "class_c_l_i_save_manager.html", "class_c_l_i_save_manager" ]
    ] ],
    [ "CLIUserState.h", "_c_l_i_user_state_8h.html", [
      [ "CLIUserState", "class_c_l_i_user_state.html", "class_c_l_i_user_state" ]
    ] ],
    [ "EngineConfig.h", "_engine_config_8h.html", "_engine_config_8h" ],
    [ "FileBackend.h", "_file_backend_8h.html", [
      [ "FileBackend", "class_file_backend.html", "class_file_backend" ]
    ] ],
    [ "LoadEngine.h", "_load_engine_8h.html", [
      [ "LoadEngine", "class_load_engine.html", "class_load_engine" ]
    ] ],
    [ "LoadManager.h", "_load_manager_8h.html", [
      [ "LoadManager", "class_load_manager.html", "class_load_manager" ]
    ] ],
    [ "PersistenceControl.h", "_persistence_control_8h.html", [
      [ "PersistenceControl", "class_persistence_control.html", "class_persistence_control" ]
    ] ],
    [ "PersistenceException.h", "_persistence_exception_8h.html", [
      [ "ArchiveException", "class_archive_exception.html", "class_archive_exception" ],
      [ "ArchiveFileOpenException", "class_archive_file_open_exception.html", "class_archive_file_open_exception" ],
      [ "BoostSerializationException", "class_boost_serialization_exception.html", "class_boost_serialization_exception" ],
      [ "NoSuchArchiveTypeException", "class_no_such_archive_type_exception.html", "class_no_such_archive_type_exception" ],
      [ "NoSuchSessionException", "class_no_such_session_exception.html", "class_no_such_session_exception" ],
      [ "NoSuchUserStateException", "class_no_such_user_state_exception.html", "class_no_such_user_state_exception" ],
      [ "NoSuchUserStateVersionException", "class_no_such_user_state_version_exception.html", "class_no_such_user_state_version_exception" ],
      [ "NoSuchVersionException", "class_no_such_version_exception.html", "class_no_such_version_exception" ],
      [ "Persistence_NameInUseException", "class_persistence___name_in_use_exception.html", "class_persistence___name_in_use_exception" ],
      [ "PersistenceDirException", "class_persistence_dir_exception.html", "class_persistence_dir_exception" ]
    ] ],
    [ "persistent_classes_manual.h", "persistent__classes__manual_8h.html", "persistent__classes__manual_8h" ],
    [ "SaveEngine.h", "_save_engine_8h.html", [
      [ "SaveEngine", "class_save_engine.html", "class_save_engine" ]
    ] ],
    [ "SaveManager.h", "_save_manager_8h.html", [
      [ "SaveManager", "class_save_manager.html", "class_save_manager" ]
    ] ],
    [ "Serializable.h", "_serializable_8h.html", "_serializable_8h" ],
    [ "StateHistory.h", "_state_history_8h.html", [
      [ "StateHistory", "class_state_history.html", "class_state_history" ]
    ] ],
    [ "StateHistory_CLI.h", "_state_history___c_l_i_8h.html", [
      [ "StateHistory_CLI", "class_state_history___c_l_i.html", "class_state_history___c_l_i" ]
    ] ],
    [ "StateHistory_Test.h", "_state_history___test_8h.html", [
      [ "StateHistory_Test", "class_state_history___test.html", "class_state_history___test" ]
    ] ],
    [ "StateMetaInfo.h", "_state_meta_info_8h.html", [
      [ "StateMetaInfo", "class_state_meta_info.html", "class_state_meta_info" ]
    ] ],
    [ "StateMetaInfo_CLI.h", "_state_meta_info___c_l_i_8h.html", [
      [ "StateMetaInfo_CLI", "class_state_meta_info___c_l_i.html", "class_state_meta_info___c_l_i" ]
    ] ],
    [ "StateMetaInfo_Test.h", "_state_meta_info___test_8h.html", [
      [ "StateMetaInfo_Test", "class_state_meta_info___test.html", "class_state_meta_info___test" ]
    ] ],
    [ "StateTransClosure.h", "_state_trans_closure_8h.html", [
      [ "StateTransClosure", "class_state_trans_closure.html", "class_state_trans_closure" ]
    ] ],
    [ "StateTransClosure_CLI.h", "_state_trans_closure___c_l_i_8h.html", [
      [ "StateTransClosure_CLI", "class_state_trans_closure___c_l_i.html", "class_state_trans_closure___c_l_i" ]
    ] ],
    [ "StateTransClosure_Test.h", "_state_trans_closure___test_8h.html", [
      [ "StateTransClosure_Test", "class_state_trans_closure___test.html", "class_state_trans_closure___test" ]
    ] ],
    [ "TestCheckpointManager.h", "_test_checkpoint_manager_8h.html", [
      [ "TestCheckpointManager", "class_test_checkpoint_manager.html", "class_test_checkpoint_manager" ]
    ] ],
    [ "TestLoadManager.h", "_test_load_manager_8h.html", [
      [ "TestLoadManager", "class_test_load_manager.html", "class_test_load_manager" ]
    ] ],
    [ "TestSaveManager.h", "_test_save_manager_8h.html", [
      [ "TestSaveManager", "class_test_save_manager.html", "class_test_save_manager" ]
    ] ],
    [ "TestUserState.h", "_test_user_state_8h.html", [
      [ "TestUserState", "class_test_user_state.html", "class_test_user_state" ]
    ] ],
    [ "UserState.h", "_user_state_8h.html", [
      [ "UserState", "class_user_state.html", "class_user_state" ]
    ] ]
];