var class_b_i_e_1_1_tess_tool_reader =
[
    [ "TessToolReader", "class_b_i_e_1_1_tess_tool_reader.html#a82a8112ca7e677a96f031ccfd0bafeb3", null ],
    [ "~TessToolReader", "class_b_i_e_1_1_tess_tool_reader.html#a90d4d91eb17cd124e2bee200f4559ccf", null ],
    [ "Detach", "class_b_i_e_1_1_tess_tool_reader.html#a4733992f3df912889ecf118f66aaa8f2", null ],
    [ "finish", "class_b_i_e_1_1_tess_tool_reader.html#ae957bb79ac38e0b1bd934bab40f62bd9", null ],
    [ "GetData", "class_b_i_e_1_1_tess_tool_reader.html#a07ed929761ca5dc8889cceb4548adf48", null ],
    [ "readBuffer", "class_b_i_e_1_1_tess_tool_reader.html#ab12877cfe3d9539c03c62a840640bf70", null ],
    [ "run", "class_b_i_e_1_1_tess_tool_reader.html#a6e11d11fada901767554b3d70c6e32f5", null ],
    [ "SampleNext", "class_b_i_e_1_1_tess_tool_reader.html#aeac4c23d6ce583593c79a84ce8bf2705", null ],
    [ "setFile", "class_b_i_e_1_1_tess_tool_reader.html#a64930a7858b1469698cc9e2ceeb75b38", null ],
    [ "Synchronize", "class_b_i_e_1_1_tess_tool_reader.html#afac498a6e0bcb2000b2b6594d156745e", null ]
];