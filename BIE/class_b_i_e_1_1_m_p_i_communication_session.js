var class_b_i_e_1_1_m_p_i_communication_session =
[
    [ "MPICommunicationSession", "class_b_i_e_1_1_m_p_i_communication_session.html#a0ba19020d7dc33537e2449ed34a3853f", null ],
    [ "~MPICommunicationSession", "class_b_i_e_1_1_m_p_i_communication_session.html#a6ccbd8a973bd2aa57cd207d2f0543f48", null ],
    [ "convertRecordToDatatype", "class_b_i_e_1_1_m_p_i_communication_session.html#a6021747972f958ee5f6114b6ba55cf8b", null ],
    [ "finishSendTx", "class_b_i_e_1_1_m_p_i_communication_session.html#ab0bb7e35627c668e031816ac72a793a3", null ],
    [ "initializeSendTx", "class_b_i_e_1_1_m_p_i_communication_session.html#ae098a5af5e8e56bf4e5d89d2d0699f33", null ],
    [ "send", "class_b_i_e_1_1_m_p_i_communication_session.html#a6aa11cb1464768c9c7fb809270dffbfc", null ],
    [ "startNewSession", "class_b_i_e_1_1_m_p_i_communication_session.html#a145c1f0fa475fb178e4ecc87b735ba66", null ]
];