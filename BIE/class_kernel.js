var class_kernel =
[
    [ "Kernel", "class_kernel.html#a84dba549c0823454ecfc8089c20cf812", null ],
    [ "~Kernel", "class_kernel.html#ab0aee98e27b5f821688faad456185b43", null ],
    [ "Combine", "class_kernel.html#af7af78f716ebb59c047eeb14dc422b33", null ],
    [ "maxDistKer", "class_kernel.html#a9855d717a0228a50b19d3fd3c5fa2332", null ],
    [ "minDistKer", "class_kernel.html#ad5f51a007986953f5cd7779a3289e893", null ],
    [ "New", "class_kernel.html#a6b111fdbf64de679d8f841cbc8f7cbf2", null ],
    [ "MetricTreeDensity", "class_kernel.html#a81e1845c802cd113a317982be123c2e5", null ],
    [ "ID", "class_kernel.html#a40bdffae2c7802aaea69960260560bba", null ]
];