var docs =
[
    [ "Quick Start", "quick.html", null ],
    [ "Downloading the package", "download.html", null ],
    [ "Project Goals", "project_goals.html", "project_goals" ],
    [ "Implementation details", "reference.html", "reference" ],
    [ "Galphat", "galphat.html", "galphat" ],
    [ "Project Team", "project_team.html", [
      [ "Principle Investigators", "project_team.html#pis", null ],
      [ "Former group members:", "project_team.html#former", null ]
    ] ]
];