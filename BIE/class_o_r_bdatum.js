var class_o_r_bdatum =
[
    [ "ORBdatum", "class_o_r_bdatum.html#a086a4060146278f2420337883149fd57", null ],
    [ "ORBdatum", "class_o_r_bdatum.html#a34802b7956ff13fa4b9db93ec84cff45", null ],
    [ "ORBdatum", "class_o_r_bdatum.html#a80c299a95d40bb6adc98f7ad69cd3287", null ],
    [ "serialize", "class_o_r_bdatum.html#adf7e03ad58e2be2247571b17a62d7a47", null ],
    [ "boost::serialization::access", "class_o_r_bdatum.html#ac98d07dd8f7b70e16ccb9a01abf56b9c", null ],
    [ "beta", "class_o_r_bdatum.html#a3056e5759f54b0adaca5bf5d5a2e08f9", null ],
    [ "mult", "class_o_r_bdatum.html#aaeaac62cf9a8ccbb6feb6df33ebd312a", null ],
    [ "p", "class_o_r_bdatum.html#a07e18aad48a3a3b56e17a1fedd4fa6e1", null ],
    [ "v", "class_o_r_bdatum.html#ab44218c3d7edb0416b2e06d1e3adf81e", null ],
    [ "weight", "class_o_r_bdatum.html#ae91abd25966cc79fcf83657ed59216eb", null ]
];