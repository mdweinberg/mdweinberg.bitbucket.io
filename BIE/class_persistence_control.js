var class_persistence_control =
[
    [ "ckcleanup", "class_persistence_control.html#ad10ff39ae7a394992d0b5d9697d451aa", null ],
    [ "ckinterval", "class_persistence_control.html#aefd924681bee10263e69d2813a838880", null ],
    [ "ckmanual", "class_persistence_control.html#ad8b0db038f11104b3b2542939be06067", null ],
    [ "ckoff", "class_persistence_control.html#a10f52819dbba4e826e4df80bb3d4971e", null ],
    [ "ckon", "class_persistence_control.html#ab77151a4e17e89905fbdde1ffe900af8", null ],
    [ "cktimer", "class_persistence_control.html#af521bec792e6fec90a7a6f2665c59dea", null ],
    [ "cktoggle", "class_persistence_control.html#a3796b32696e2573df90aae9901e9821f", null ],
    [ "getLatest", "class_persistence_control.html#ab62ef238b5cf1d0457836c8e232aaef6", null ],
    [ "phistory", "class_persistence_control.html#ade87e67230703460f202fe3d7a65d0ca", null ],
    [ "plist", "class_persistence_control.html#a34675df73a68a77d6b8078c95b9b2632", null ],
    [ "pmetainfo", "class_persistence_control.html#ac15a44b4ed6e7a3afc5355f8c2b71872", null ],
    [ "pmetainfo", "class_persistence_control.html#a5e25bb79023f1ee369f356c936e349a0", null ],
    [ "pnewsession", "class_persistence_control.html#a0b25a205a7e0891ed98ea0522677c66b", null ],
    [ "pnewsession", "class_persistence_control.html#a282b375e19101f7f4cab8912ed21b7f6", null ],
    [ "prestore", "class_persistence_control.html#ae2f5a9bc3a709390a6b12cec842b3953", null ],
    [ "prestore", "class_persistence_control.html#a28c9d98cf13cba23d3d01cba956dc194", null ],
    [ "psave", "class_persistence_control.html#a1dc13fdcdc619645033c989487b5d665", null ],
    [ "psave", "class_persistence_control.html#ab02d982fc55b9c513b0c969872b13c3f", null ],
    [ "pversions", "class_persistence_control.html#ad9f051dd94e15d00ad22863776b9aba6", null ],
    [ "savemanager", "class_persistence_control.html#abbdb4fda07190073cd3b688f70575cc2", null ],
    [ "userstate", "class_persistence_control.html#a89695a2859097908f50e94a4dee346a7", null ]
];