var class_b_i_e_1_1_subtraction_filter =
[
    [ "SubtractionFilter", "class_b_i_e_1_1_subtraction_filter.html#a34ef6659d2e7a43dcdb231fbe321e080", null ],
    [ "SubtractionFilter", "class_b_i_e_1_1_subtraction_filter.html#a8ff9861acdde0d4bc7f068ae19b2c9eb", null ],
    [ "compute", "class_b_i_e_1_1_subtraction_filter.html#aef083a9f20ef9efdd9eece04d7a586e8", null ],
    [ "serialize", "class_b_i_e_1_1_subtraction_filter.html#a0d083bd8cb8a3df1d01c1895f37de08f", null ],
    [ "boost::serialization::access", "class_b_i_e_1_1_subtraction_filter.html#ac98d07dd8f7b70e16ccb9a01abf56b9c", null ],
    [ "difference_index", "class_b_i_e_1_1_subtraction_filter.html#abd6082f3b515296a80c421c419cc59ce", null ],
    [ "input", "class_b_i_e_1_1_subtraction_filter.html#ae5c8d89aadf054109ca0110ac7b6b795", null ],
    [ "output", "class_b_i_e_1_1_subtraction_filter.html#ae417741e2ec0078cdee9951b47f54188", null ],
    [ "x_index", "class_b_i_e_1_1_subtraction_filter.html#acd4d02a2cf7d1347fb077fdfe4787bfb", null ],
    [ "y_index", "class_b_i_e_1_1_subtraction_filter.html#a93e30b0356ac4089f8bd519124391b77", null ]
];