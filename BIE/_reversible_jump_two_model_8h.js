var _reversible_jump_two_model_8h =
[
    [ "RJMappingType", "_reversible_jump_two_model_8h.html#ga88b74b7b657743d03a2917e7822652a9", [
      [ "Unary", "_reversible_jump_two_model_8h.html#gga88b74b7b657743d03a2917e7822652a9ad4577103b14a9d36d8971ffdd1cac382", null ],
      [ "Split", "_reversible_jump_two_model_8h.html#gga88b74b7b657743d03a2917e7822652a9a3652858e591fa80760f448220e267b2b", null ],
      [ "Join", "_reversible_jump_two_model_8h.html#gga88b74b7b657743d03a2917e7822652a9ac4ee2627c93c294e5bff109120124887", null ],
      [ "Sample", "_reversible_jump_two_model_8h.html#gga88b74b7b657743d03a2917e7822652a9ac9def2de60faeb94877f77344982f3b2", null ],
      [ "SampleSplit", "_reversible_jump_two_model_8h.html#gga88b74b7b657743d03a2917e7822652a9a1b662c2a720decfcc13a207711857ec7", null ],
      [ "SampleJoin", "_reversible_jump_two_model_8h.html#gga88b74b7b657743d03a2917e7822652a9a60258e3576ed173cfec83114f6dc320e", null ]
    ] ],
    [ "RJRandomType", "_reversible_jump_two_model_8h.html#gaaf88708eed3f30abdb02c863c8e1e2d7", [
      [ "Add", "_reversible_jump_two_model_8h.html#ggaaf88708eed3f30abdb02c863c8e1e2d7ac932b63b188f53946c7cbe318caeaf18", null ],
      [ "Mult", "_reversible_jump_two_model_8h.html#ggaaf88708eed3f30abdb02c863c8e1e2d7aa27f07cc150b9d4e97d4a59c377fdd66", null ],
      [ "Cons", "_reversible_jump_two_model_8h.html#ggaaf88708eed3f30abdb02c863c8e1e2d7a0cba655ec38485a0b0b7215688a60f8b", null ]
    ] ]
];