var searchData=
[
  ['data_5fcompression_5ferr',['DATA_COMPRESSION_ERR',['../fitsio_8h.html#a7a8aca75e1d7c3c785d4901eea6b2d96',1,'fitsio.h']]],
  ['data_5fdecompression_5ferr',['DATA_DECOMPRESSION_ERR',['../fitsio_8h.html#aca0df5b7c00f690c8aa5dd07a6bfaf93',1,'fitsio.h']]],
  ['debug',['DEBUG',['../debug_8h.html#ad72dbcf6d0153db1b8d8a58001feed83',1,'debug.h']]],
  ['declare_5fget_5fset_5fis',['DECLARE_GET_SET_IS',['../_a_data_8h.html#a3dfefc2be875ea03ab2531612c6711d0',1,'AData.h']]],
  ['detach_5ftesstool_5fmask',['DETACH_TESSTOOL_MASK',['../bie_tags_8h.html#aa881ffc4ed4bd782fdc5fd6260630243',1,'bieTags.h']]],
  ['double_5fimg',['DOUBLE_IMG',['../fitsio_8h.html#ab982ffada2a38bbc1ac6b67d422870c6',1,'fitsio.h']]],
  ['doublenullvalue',['DOUBLENULLVALUE',['../fitsio_8h.html#a96aa9f8b3b2406d5e9fa88a3cf909ece',1,'fitsio.h']]],
  ['driver_5finit_5ffailed',['DRIVER_INIT_FAILED',['../fitsio_8h.html#a9c0e66fa4f64dfced15de1adaf4137e7',1,'fitsio.h']]]
];
