var searchData=
[
  ['parallel_20debugging_20with_20mpi',['Parallel debugging with MPI',['../pdebug.html',1,'arch']]],
  ['persistence',['Persistence',['../persistence.html',1,'project_goals']]],
  ['parallel_20chains',['Parallel chains',['../pmcmc.html',1,'']]],
  ['project_20goals',['Project Goals',['../project_goals.html',1,'docs']]],
  ['project_20team',['Project Team',['../project_team.html',1,'docs']]],
  ['peter_20green_27s_20reversible_20jump_20algorithm',['Peter Green&apos;s Reversible Jump Algorithm',['../rjpage.html',1,'algo']]]
];
