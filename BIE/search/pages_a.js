var searchData=
[
  ['marginal_20likelihood_20computation',['Marginal likelihood computation',['../marglike.html',1,'algo']]],
  ['markov_20chain_20monte_20carlo',['Markov Chain Monte Carlo',['../mcmc.html',1,'algo']]],
  ['metropolis_2dhastings_20transitions',['Metropolis-Hastings transitions',['../methast.html',1,'algo']]],
  ['multiresolution',['Multiresolution',['../multi.html',1,'algo']]]
];
