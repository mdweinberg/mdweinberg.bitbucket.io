var searchData=
[
  ['figure_201_3a_20statistical_20computation',['Figure 1: Statistical computation',['../figure1.html',1,'']]],
  ['figure_202_3a_20statistical_20computation',['Figure 2: Statistical Computation',['../figure2.html',1,'']]],
  ['figure_203_3a_20visualization_20tool',['Figure 3: Visualization tool',['../figure3.html',1,'']]],
  ['figure_204_3a_20visualization_20tool',['Figure 4: Visualization tool',['../figure4.html',1,'']]],
  ['figure_205_3a_20graphical_20user_20interface',['Figure 5: Graphical user interface',['../figure5.html',1,'']]],
  ['future_20features_20and_20todo_20list',['Future Features and TODO list',['../future.html',1,'']]],
  ['fixing_20kd_20tessellation_20aspect_20ratios',['Fixing KD Tessellation Aspect Ratios',['../kdtessellation.html',1,'']]]
];
