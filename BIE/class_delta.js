var class_delta =
[
    [ "Delta", "class_delta.html#a20c7bcc600ccfdf7a678872fa4d1a7a7", null ],
    [ "Delta", "class_delta.html#aa7c948787689cbb92914590dcbf4a6fd", null ],
    [ "Delta", "class_delta.html#afd2aae407f1ebdd177801a07d9d2cc95", null ],
    [ "key", "class_delta.html#a6088d8436bd15489a72c67192f256db1", null ],
    [ "Max", "class_delta.html#a7ad8b5c21e21637ae9cf9a67d2ce390e", null ],
    [ "maxDist", "class_delta.html#a9fb61e83e06f051e66248a74c8bef1f9", null ],
    [ "Min", "class_delta.html#a1eba0bf8317fe8936f90ea5cd2cb7e60", null ],
    [ "minDist", "class_delta.html#a7236176bd148d3a61efe9521c2df58a4", null ],
    [ "serialize", "class_delta.html#a706f0dd80352bf6d137ef40b0e31190a", null ],
    [ "boost::serialization::access", "class_delta.html#ac98d07dd8f7b70e16ccb9a01abf56b9c", null ],
    [ "Key", "class_delta.html#a23194a5c29bf2962fac3cb3cf04c6913", null ],
    [ "maximum", "class_delta.html#ab6e74a9b85b98ad070152e40900ba74c", null ],
    [ "minimum", "class_delta.html#a0d9088d6ab9649babb7641d640bab20d", null ]
];