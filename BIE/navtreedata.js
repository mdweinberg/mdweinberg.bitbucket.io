var NAVTREE =
[
  [ "Bayesian Inference Engine", "index.html", [
    [ "Baysian Inference Engine", "index.html", "index" ],
    [ "Modules", "modules.html", "modules" ],
    [ "Namespaces", null, [
      [ "Namespace List", "namespaces.html", "namespaces" ],
      [ "Namespace Members", "namespacemembers.html", [
        [ "All", "namespacemembers.html", null ],
        [ "Functions", "namespacemembers_func.html", null ],
        [ "Variables", "namespacemembers_vars.html", null ],
        [ "Typedefs", "namespacemembers_type.html", null ],
        [ "Enumerations", "namespacemembers_enum.html", null ],
        [ "Enumerator", "namespacemembers_eval.html", null ]
      ] ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Hierarchy", "hierarchy.html", "hierarchy" ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Functions", "functions_func.html", "functions_func" ],
        [ "Variables", "functions_vars.html", "functions_vars" ],
        [ "Typedefs", "functions_type.html", null ],
        [ "Enumerations", "functions_enum.html", null ],
        [ "Enumerator", "functions_eval.html", null ],
        [ "Related Functions", "functions_rela.html", "functions_rela" ]
      ] ]
    ] ],
    [ "Files", null, [
      [ "File List", "files.html", "files" ],
      [ "File Members", "globals.html", [
        [ "All", "globals.html", "globals_dup" ],
        [ "Functions", "globals_func.html", "globals_func" ],
        [ "Variables", "globals_vars.html", null ],
        [ "Typedefs", "globals_type.html", null ],
        [ "Enumerations", "globals_enum.html", null ],
        [ "Enumerator", "globals_eval.html", null ],
        [ "Macros", "globals_defs.html", "globals_defs" ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"_a_data_8h.html",
"_galaxy_model_one_d_cached_8h_source.html",
"_quantile_8h_source.html",
"_weibull_dist_8h_source.html",
"class_b_i_e_1_1_b_i_e_a_c_g.html#aecdca64daf185d1a5899651e266840b7",
"class_b_i_e_1_1_c_m_d.html#a8a4ca44e161ccaa501d61833f9998472",
"class_b_i_e_1_1_chain.html#aebdcd50d14c94304285c28f86ff66cad",
"class_b_i_e_1_1_differential_evolution.html#a9be59e476c9da6ba8e186387e312fad3",
"class_b_i_e_1_1_ensemble_disc.html#a1e20ae3ad05832cf33c2449de18f4e6c",
"class_b_i_e_1_1_evaluation_request.html#afca6754f490b62dcf0f97cf940fc5ac5",
"class_b_i_e_1_1_galaxy_model_one_d.html#a65654938b1c0257907efc87f117cc99a",
"class_b_i_e_1_1_galphat_1_1_bulge_disk.html#a9ac3de64fcc9c74e9e374f53b89cd12d",
"class_b_i_e_1_1_galphat_1_1_galphat_model.html#acce23c4f1c3299a8d3b6766386d6537b",
"class_b_i_e_1_1_galphat_1_1inpars.html#add474d45691b4747dcb75383befe1b43",
"class_b_i_e_1_1_gauss_test_likelihood_function.html#aa1d11efad9b196e2a8e3f6f23fd2af6c",
"class_b_i_e_1_1_histogram_cache.html#a111d1488ce4a6df823c1d4b7657b728f",
"class_b_i_e_1_1_inverse_mu_filter.html#a4e0e735bcdce11ecf7e7e8ba133f2e4f",
"class_b_i_e_1_1_likelihood_computation_m_p_i_t_p.html#adb456ea63b9847037993318700579659",
"class_b_i_e_1_1_max_filter.html#a7b70391de837d324e9df8209e47468cb",
"class_b_i_e_1_1_multi_n_wishart_dist.html#ab172fd745f4dc4dea9bc70a8639df0ff",
"class_b_i_e_1_1_p_f_simulation.html#a5518b1c34f095e16ec864ddb3384edd1",
"class_b_i_e_1_1_pop_cache_s_f.html#a5f745c1c58284fef2e05b1509eeeb2e1",
"class_b_i_e_1_1_pop_model_cache_s_f.html#aa8e34a484d1c4ed3237ec5a5550bb630",
"class_b_i_e_1_1_post_mixture_prior.html#ad44703357f01bad2e0ca2ca4039323cf",
"class_b_i_e_1_1_quantile.html#a7f61e9a153d94cfd972bcb1d134e1d2d",
"class_b_i_e_1_1_record_output_stream.html#a1db5a035293e5f7d2bbd46a00a9dfaa8",
"class_b_i_e_1_1_reversible_jump.html#ac98d07dd8f7b70e16ccb9a01abf56b9c",
"class_b_i_e_1_1_splat_model.html#a9c2599aed358097c1806fe1d4998115a",
"class_b_i_e_1_1_state_cache.html#aba27f1c394804b10ff953649fa497907",
"class_b_i_e_1_1_tempered_differential_evolution.html#a14386c2e96fe835be5b2edcb3bb92a79",
"class_b_i_e_1_1_tess_tool_v_t_k.html#a783de8a9d940cd07ede9cf07734c63c6",
"class_b_i_e_1_1_typed_buffer.html#aef3c2bef9650c37c6d1160d1d29429b0",
"class_b_i_e_1_1element.html#a592d616e0b61fdce2d2afea9c17dbf57",
"class_call_expr.html#a0715bac24bdf6ecab19c3ce7c8d95f41a2fd1893b2f13926aa521047fea2a855e",
"class_k_d_tree.html#abada47ace03cc3fe8c037dfd54166dd5",
"class_metric_tree.html#af89689051ec7d4cda7b3455231815f96",
"class_s_f_d.html#adc306bde1e87fbbd0d1431d28e25b1a0",
"class_t_n_tree.html#a809b0d16424087d7c9db941f1f842e43",
"class_user_state.html#ac98d07dd8f7b70e16ccb9a01abf56b9c",
"fitsio_8h.html#a18911142028c91647c015b5a0b87d015",
"fitsio_8h.html#a72d7831f337c5778fc6e104ca1c37945",
"fitsio_8h.html#ac22dac93a29f6640d6945b71f2ce5554",
"functions_vars_e.html",
"group__mcalgorithm.html#gga88b74b7b657743d03a2917e7822652a9a1b662c2a720decfcc13a207711857ec7",
"gvariable_8h.html#gacffd52bcced35016d81f348de2957720",
"longnam_8h.html#a76fb18ebd63a8f9f23b538703ab456a8",
"mcmcrj.html#mcmcrjintro"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';