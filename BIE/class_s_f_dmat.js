var class_s_f_dmat =
[
    [ "SFDmat", "class_s_f_dmat.html#afe1e9e4c5984ace2a08528a6412b96ad", null ],
    [ "SFDmat", "class_s_f_dmat.html#a4c847b2d41e1b4ad03db4b9ff9020673", null ],
    [ "enter", "class_s_f_dmat.html#aee943ea2f8f91d5f908516995a0828cd", null ],
    [ "getval", "class_s_f_dmat.html#a6ccac7b435414323a76256a8476db684", null ],
    [ "next", "class_s_f_dmat.html#aa538c5d5d62c9af89b52e0f09ade5039", null ],
    [ "reset", "class_s_f_dmat.html#a5183f37a8f633522dc38385c8f173ff0", null ],
    [ "serialize", "class_s_f_dmat.html#a0c0ed7a08c3684cb936d1954c372e397", null ],
    [ "Set", "class_s_f_dmat.html#ae9575078db1424d1daca802a254071c5", null ],
    [ "boost::serialization::access", "class_s_f_dmat.html#ac98d07dd8f7b70e16ccb9a01abf56b9c", null ],
    [ "dx", "class_s_f_dmat.html#a3d052ae46c766019523a565713f83447", null ],
    [ "it", "class_s_f_dmat.html#a009db63f8842fcadfa5ed5ce899ee728", null ],
    [ "numbin", "class_s_f_dmat.html#a37caecf70d5b70420264fb81ff269341", null ],
    [ "vec", "class_s_f_dmat.html#a63269b4cbd878032bb0b5b852bee64ad", null ]
];