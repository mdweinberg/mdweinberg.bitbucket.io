var class_c_l_i_save_manager =
[
    [ "CLISaveManager", "class_c_l_i_save_manager.html#a48363b3283423ffe186e6607252fe60b", null ],
    [ "~CLISaveManager", "class_c_l_i_save_manager.html#a8bc80d112c80d2f4c7918af166338f9c", null ],
    [ "CLISaveManager", "class_c_l_i_save_manager.html#a13aa80ad5cfa9f23d9ae1060e8519185", null ],
    [ "BOOST_SERIALIZATION_SPLIT_MEMBER", "class_c_l_i_save_manager.html#a9c4bc1fa8b8d860c11cf639e310e4062", null ],
    [ "getBackend", "class_c_l_i_save_manager.html#a52eb5b1505b1feb42a8666facb8c811d", null ],
    [ "load", "class_c_l_i_save_manager.html#af2ef43c6e688f845cdb861ede6b809c9", null ],
    [ "save", "class_c_l_i_save_manager.html#af3b57549148588f53496212d1c9a4c7a", null ],
    [ "save", "class_c_l_i_save_manager.html#a728cd177884ab05f44ba769540ddb09f", null ],
    [ "setComment", "class_c_l_i_save_manager.html#a611cc7eb1e20e601d8b07e18cbaaa81a", null ],
    [ "boost::serialization::access", "class_c_l_i_save_manager.html#ac98d07dd8f7b70e16ccb9a01abf56b9c", null ],
    [ "backend", "class_c_l_i_save_manager.html#a5590096bbd6f06d167527c5a390c1d46", null ],
    [ "bt", "class_c_l_i_save_manager.html#ae10310c4b2384a0ac0cd4ad31f7e05c2", null ],
    [ "et", "class_c_l_i_save_manager.html#ad7be9004df7adbb00cc506c4d1304c9a", null ],
    [ "saveEngine", "class_c_l_i_save_manager.html#a1f93a100ec8c98f9aae9bffd1e3bfc2a", null ],
    [ "userState", "class_c_l_i_save_manager.html#a956dbd3edd799abc09eed1fe34664be5", null ]
];