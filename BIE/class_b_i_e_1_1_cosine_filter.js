var class_b_i_e_1_1_cosine_filter =
[
    [ "CosineFilter", "class_b_i_e_1_1_cosine_filter.html#a6357914857271cb54f8b1fe4d7b1f14c", null ],
    [ "CosineFilter", "class_b_i_e_1_1_cosine_filter.html#a2188b36c5c3b741a86220ddfa65f3ffc", null ],
    [ "compute", "class_b_i_e_1_1_cosine_filter.html#a0ae74928a2c308c07bdf4eec75e6ad4c", null ],
    [ "serialize", "class_b_i_e_1_1_cosine_filter.html#a03b9c4a6c45639043c56205a481a4cfe", null ],
    [ "boost::serialization::access", "class_b_i_e_1_1_cosine_filter.html#ac98d07dd8f7b70e16ccb9a01abf56b9c", null ],
    [ "cosx_index", "class_b_i_e_1_1_cosine_filter.html#a826f02dbd2c22d22dace7064d3b2bfd5", null ],
    [ "input", "class_b_i_e_1_1_cosine_filter.html#a4961eaad88e6510f6dc1513cbf341a08", null ],
    [ "output", "class_b_i_e_1_1_cosine_filter.html#ab9781a1d909ecf5ac0cbdfa132bf33f1", null ],
    [ "x_index", "class_b_i_e_1_1_cosine_filter.html#aa2a3df372659b36d3f823970b8725c69", null ]
];