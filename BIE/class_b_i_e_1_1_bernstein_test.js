var class_b_i_e_1_1_bernstein_test =
[
    [ "Density", "class_b_i_e_1_1_bernstein_test.html#aeb480b656ab7d4970588ccb09583da7b", [
      [ "quartic", "class_b_i_e_1_1_bernstein_test.html#aeb480b656ab7d4970588ccb09583da7bac4d2c769fcb01bd2b52fec9d24aa5271", null ],
      [ "powerlaw", "class_b_i_e_1_1_bernstein_test.html#aeb480b656ab7d4970588ccb09583da7babffa812655087c3294e8353203b95564", null ],
      [ "twobump", "class_b_i_e_1_1_bernstein_test.html#aeb480b656ab7d4970588ccb09583da7ba496aa7b8ad90baac58baf1fac41033d2", null ]
    ] ],
    [ "BernsteinTest", "class_b_i_e_1_1_bernstein_test.html#a00cb3463944a580e1f6549df0fa9eb7b", null ],
    [ "BernsteinTest", "class_b_i_e_1_1_bernstein_test.html#aad0d54455fcee43712aa71f71bccaa3a", null ],
    [ "BernsteinTest", "class_b_i_e_1_1_bernstein_test.html#a81b0c71f59bb5a30c5e7bb9bf0d8da40", null ],
    [ "BernsteinTest", "class_b_i_e_1_1_bernstein_test.html#a1a404d655648caf04fae69e0c946d46d", null ],
    [ "~BernsteinTest", "class_b_i_e_1_1_bernstein_test.html#a96b4d0a45275c5e13d19099871f1f371", null ],
    [ "CDF", "class_b_i_e_1_1_bernstein_test.html#a324724f2a86150f58c60031ba0b6709e", null ],
    [ "CumuProb", "class_b_i_e_1_1_bernstein_test.html#ab68e2c6b6fc7c0b118d11b372d5713a9", null ],
    [ "initialize", "class_b_i_e_1_1_bernstein_test.html#a0d1d704f091d5d79069e7be7096ee75d", null ],
    [ "LikeProb", "class_b_i_e_1_1_bernstein_test.html#a14fcf246745cb47120cf4b5db7bc8edf", null ],
    [ "makeData", "class_b_i_e_1_1_bernstein_test.html#a834f50a4b7bd1f6c3f7dff07b9b1c0a4", null ],
    [ "ParameterDescription", "class_b_i_e_1_1_bernstein_test.html#a83b13ecae9968d332f9feec41715778f", null ],
    [ "PDF", "class_b_i_e_1_1_bernstein_test.html#ab82e5b4e179d3f64ac9693bcc573554a", null ],
    [ "serialize", "class_b_i_e_1_1_bernstein_test.html#a25cf9a8b4eb20db20d2ceda4ed50e0a1", null ],
    [ "boost::serialization::access", "class_b_i_e_1_1_bernstein_test.html#ac98d07dd8f7b70e16ccb9a01abf56b9c", null ],
    [ "bern", "class_b_i_e_1_1_bernstein_test.html#af0f69733ab3a5b48d730aeea0c2a3cd2", null ],
    [ "beta", "class_b_i_e_1_1_bernstein_test.html#a9c7b0d6ba0a5c69e4b0fa7dce19219c9", null ],
    [ "c", "class_b_i_e_1_1_bernstein_test.html#ac050113bcb84362f4c4bc04184647265", null ],
    [ "data", "class_b_i_e_1_1_bernstein_test.html#aa1f7147eaf08ad4e85768783e9eed2e5", null ],
    [ "first", "class_b_i_e_1_1_bernstein_test.html#a37c0e3951bab058f65411a5bdd7252fe", null ],
    [ "npts", "class_b_i_e_1_1_bernstein_test.html#aea46c31bfa3309d999075076fc544f60", null ],
    [ "order", "class_b_i_e_1_1_bernstein_test.html#ac0edcc7a43478e2574aa5bbac0763af5", null ],
    [ "type", "class_b_i_e_1_1_bernstein_test.html#ac1df60a32d156d53c525383f945bb5da", null ],
    [ "xmax", "class_b_i_e_1_1_bernstein_test.html#a4eef3670ee5ed8a69250c34980ff9c0b", null ],
    [ "xmin", "class_b_i_e_1_1_bernstein_test.html#aab6bc71db6f861ff191e062b88b6ccee", null ]
];