var class_backend =
[
    [ "Backend", "class_backend.html#a1a46af8e9525762a3b7bbf199b852bf6", null ],
    [ "~Backend", "class_backend.html#a93597d0e1cc4603227e5ff8fb8af678d", null ],
    [ "getInputStream", "class_backend.html#aa229f7edef208d4b436ab72893e44446", null ],
    [ "getInputStream", "class_backend.html#a4aced4d7fe1638e8b1a0a9835e19621d", null ],
    [ "getOutputStream", "class_backend.html#ae5aca754855e801402891d348bcdbc31", null ],
    [ "getOutputStream", "class_backend.html#aa494de5ad4d03115b53f170c564a0dff", null ],
    [ "listSessions", "class_backend.html#a46dce711e65fcda87b91947c23c9decd", null ],
    [ "listVersions", "class_backend.html#a4fe793db9f03633c5534e9cc9ee72ad0", null ]
];