var reference =
[
    [ "Usage and notes", "usage.html", [
      [ "Usage", "usage.html#Usage", null ],
      [ "Run", "usage.html#Sample", null ],
      [ "Results", "usage.html#sample", null ],
      [ "Notes", "usage.html#Notes", null ]
    ] ],
    [ "Description of specific algorithms", "algo.html", "algo" ],
    [ "Software architecture", "arch.html", "arch" ]
];