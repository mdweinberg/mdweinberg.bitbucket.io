var _kernel_8h =
[
    [ "EpanetchnikovKernel", "class_epanetchnikov_kernel.html", "class_epanetchnikov_kernel" ],
    [ "GaussKernel", "class_gauss_kernel.html", "class_gauss_kernel" ],
    [ "Kernel", "class_kernel.html", "class_kernel" ],
    [ "LaplaceKernel", "class_laplace_kernel.html", "class_laplace_kernel" ],
    [ "SplineKernel", "class_spline_kernel.html", "class_spline_kernel" ],
    [ "KernelPtr", "_kernel_8h.html#a3f41aa5430f0b228913f372b50481cc2", null ]
];