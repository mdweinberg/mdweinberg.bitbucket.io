var algo =
[
    [ "Metropolis-Hastings transitions", "methast.html", null ],
    [ "Peter Green's Reversible Jump Algorithm", "rjpage.html", null ],
    [ "Markov Chain Monte Carlo", "mcmc.html", null ],
    [ "The Reversible jump Metropolis-Hastings algorithm", "mcmcrj.html", [
      [ "Motivation for RJMCMC", "mcmcrj.html#mcmcrjintro", null ],
      [ "Implementing RJMCMC", "mcmcrj.html#mcmcrjim", null ],
      [ "The RJMCMC Algorithm", "mcmcrj.html#mcmcrjalgo", null ],
      [ "An example", "mcmcrj.html#mcmcrjex", [
        [ "References", "mcmcrj.html#refs", null ]
      ] ]
    ] ],
    [ "Tempered Transitions", "tempered.html", [
      [ "Concept", "tempered.html#Concept", [
        [ "Details", "tempered.html#Details", null ]
      ] ]
    ] ],
    [ "Multiresolution", "multi.html", null ],
    [ "Marginal likelihood computation", "marglike.html", [
      [ "Introduction", "marglike.html#intro", null ],
      [ "The algorithm", "marglike.html#algorithm", null ],
      [ "Using MCMC for importance sampling", "marglike.html#importance", [
        [ "Volume peeling", "marglike.html#peeling", null ],
        [ "Identification of the posterior mode", "marglike.html#mode_id", null ]
      ] ],
      [ "Using these in the BIE", "marglike.html#using", null ]
    ] ]
];